/**
 * This is the mediator for the SoSmart system
 * @requires json file with list of tours
 *
 * @author Jonathan Hussey <jonathan@winterwell.com>
 *
 * @requires Rank and Score
 *
 *
 **/

var SoSmartMediator = function() {

    $this = this;

    this.userRank = new Rank();

    this.userScore = new Score();

    var soSmartUrl = "/help-soSmart";

    var parentDir = "/static/code/widgets";

    var tutorialList = tutorialList || [];

    var userTutorials = userTutorials || {};

    var tagPrefix = "!tutorial_done_";

    //cached versions of the below methods which retrieve data
    var _cachedData = _cachedData || {};

    /**
     * Points to the sosmart url which allows functionality such as pulling completed tutorials
     * @return {String}
     */
    this.getDataUrl = function() {
        return soSmartUrl;
    };

    /**
     * The widget may be moved to a separate directory at some point
     * @return {String}
     */
    this.getParentDir = function() {
        return parentDir;
    };

    var userCompletedTutorials = DataLoader.get(this.getDataUrl() + ".json");

    var tutorials = $.getJSON(this.getParentDir() + "/SoSmartTutorialList.json");

    /**
     * Set the task list from the back end for users
     * @param {Task[]} tutorialList
     */
    var setTutorials = function(tutorialitem) {
        tutorialList = tutorialitem;
    };

    /**
     * Set the list of completed user tasks
     * @param {Array} usertutorialscompleted Normally returned from a deferred object
     */
    var setUserTutorialcompleted = function(usertutorialscompleted) {

        $.extend(userTutorials, {
            tasks: usertutorialscompleted
        });
    };

    /**
     * Remove prefix from tags
     * @param  {String} tag The name of the completed task
     * @return {String}
     */
    this.parseTutorialTag = function(tag) {
        return tag.split(tagPrefix)[1];
    };

    /**
     * Get the user tasks and represent as String[]
     * @param  {String[]} tasklist The array of completed tasks
     * @return {[type]}          [description]
     */
    this.generateCompletedUserTutorialsAsList = function() {

        var tutorial = soSmartMediator.getUserCompletedTutorials();
        var completed = _.filter(this.getTutorials(), function(value, key, list) {
            for (var i = 0; i < tutorial.length; i++) {
                if (tutorial[i].indexOf(value.slug) > -1) {
                    return value.slug;
                }
            }
        });
        return completed;
    };

    /**
     * [getUserCompletedTutorialsByLevel description]
     * @return {[type]} [description]
     */
    this.getUserCompletedTutorialsByLevel = function() {

        var userlist = this.generateCompletedUserTutorialsAsList();

        var userTutorialsByLevel = _.groupBy(userlist, function(value, key, list) {
            return value.level;
        });

        return userTutorialsByLevel;
    };

    /**
     * Retrieve the entire task list
     * @return {Object} An array of Task objects
     */
    this.getTutorials = function() {
        return tutorialList;
    };

    /**
     * Return completed tasks by the user
     * @return {Object} [description]
     */
    this.getUserCompletedTutorials = function() {
        return userTutorials.tasks.tutorials_done;
    };

    /**
     * Retrieve tasks based on the level
     * @param  {Number} level The level
     * @return {Tutorials[]}       A filtered array of Task objetcs based on level
     */
    this.getTutorialsByLevel = function(level) {
        var filteredList = _.filter(this.getTutorials(), {
            "level": level
        });
        return filteredList;
    };

    /**
     * Retrieve the list of allowable tasks based on a users role
     * @param  {Person} Person [description]
     * @return {Task[]}        Array of task objects
     */
    this.getTutorialsByCapability = function(Person) {
        // var userRole = Roles.getRole(Person);
        // var filteredList = _.filter(this.getTutorials(), {
        //     "capability": userRole
        // });
        // return filteredList;
    };

    /**
     * Verify if the user can complete this task
     * @param  {String} capability the capability of the task
     * @return {boolean}
     */
    this.userCanDoTutorial = function(capability) {
        return Roles.iCan(capability);
    };

    /**
     * Option to retrieve all tasks organised by level
     * @return {Tutorials{}} An object containing tasks grouped by level
     */
    this.getTutorialsOrganisedByLevel = function() {

        var orderedTutorialsByLevel = _.groupBy(this.getTutorials(), function(value, key, list) {
            return value.level;
        });

        return orderedTutorialsByLevel;
    };

    /**
     * Get a single task
     * @param  {String} slug the slug of the tag being stored i.e. everything after the prefix
     * @return {Task}    The task object based on the slug
     */
    this.getSingleTutorial = function(slug) {
        var task = _.find(this.getTutorials(), {
            "slug": slug
        });
        return task;
    };

    /**
     * Returns user tutorials completed deferred object
     * @return {Deferred} [description]
     */
    this.getDeferredCompletedTutorialsByUser = function() {
        return userCompletedTutorials;
    };

    /**
     * Returns a tutorial list deferred object
     * @return {Deferred} [description]
     */
    this.getDeferredAllTutorials = function() {
        return tutorials;
    };

    /**
     * Update the backend with completed tutorial
     * This will only persist the completed tutorial
     * if the tutorial exists in the json list
     * @param  {String} slug
     */
    this.persistCompletedTutorial = function(slug) {

        //lets check that the slug is part of the tutorial
        if (typeof this.getSingleTutorial(slug) !== "undefined") {
            //we could verify the action against a list of tasks
            DataLoader.post(soSmartUrl + "?action=done&slug=" + slug).done(function(data) {
                //an action to perform when the slug is completed e.g. a message of success
                Notifications.post({
                    type: "success",
                    text: "The tutorial is complete. Visit the " + "<a href='/help-soSmart'>SoSmart Academy</a> for more information."
                });
            });
        }
    };

    /**
     * Get tutorials grouped by slug
     * @return {Tasks{}} An object of tasks grouped by slug
     */
    this.getTutorialsOrderBySlug = function() {

        var list = this.getTutorials();

        var tutorialList = {};

        _.each(list, function(value, key, list) {
            tutorialList[value.slug] = value;
        });

        return tutorialList;
    };

    /**
     * Retrieve the list of user completed tasks as a an object
     * Normally completed by the underscores indexby method but not included in lodash version
     * @return {[type]} [description]
     */
    this.getUserCompletedTutorialsOrderBySlug = function() {

        var list = this.generateCompletedUserTutorialsAsList();

        var tutorialCompeted = {};

        _.each(list, function(value, key, list) {
            tutorialCompeted[value.slug] = value;
        });

        return tutorialCompeted;
    };

    /**
     * Retrieve the entire list of slugs for tutorials
     * @return {String[]}
     */
    this.getAllTutorialSlugs = function() {
        // var slugList = _.pluck(this.getTutorials(), 'slug');
        // return slugList;
    };

    /**
     * Remove a tutorial from the user list
     * @param  {String} slug The tutorial to be removed
     */
    this.removeCompletedTutorial = function(slug) {
        DataLoader.post(this.getDataUrl() + "?action=remove&slug=" + slug).done(function() {
            Notifications.post({
                type: "info",
                text: "The tutorial is removed. Visit the " + "<a href='/help-soSmart'>SoSmart Academy</a> for more information."
            });
        });
    };

    /**
     * Get the progress of the user
     * @return {Number} The completed number as a percentage
     */
    this.calculatePercentageComplete = function() {
        return Math.round(this.getUserCompletedTutorials().length / this.getTutorials().length * 100);
    };

    /**
     * Retrieves all the tutorials of the user and removes them
     */
    this.removeCompletedTutorialsBulk = function() {
        var completed = _.keys(this.getUserCompletedTutorialsOrderBySlug());
        var numOfCompleted = completed.length;

        while (numOfCompleted-- > 0) {
            this.removeCompletedTutorial(completed[numOfCompleted]);
        }
    };



    //when the object is created we want it to auto populate
    //the data structures for tasks and user completed tasks
    $.when(userCompletedTutorials, tutorials).done(function(uct, tut) {
        setUserTutorialcompleted(uct[0]);
        setTutorials(tut[0]);
    });

};


/**
 * Should only initiate when on SoSmart and help pages and welcome screen
 */
$(function() {
    //This could really do with som requirejs or AMD assistance
    soSmartMediator = new SoSmartMediator();
});
