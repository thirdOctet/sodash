/**
 *  The DataCentre is a dictionary like implementation data structure for data management
 *  It contains the fundamental javascript object as the data source and functions to interact with it
 *  @author jonathan@winterwell.com
 */
var DataCentre = function() {
    /**
     * The storage object for the dictionary like implementation
     * @type {Object}
     */
    this.dataCentre = {};

};


/**
 * Each data centre will have a unique way of storing data
 * When inheriting this object overwrite this method using the prototypical
 * model
 * @param  {String} key 
 * @return {String}     The converted key
 */
DataCentre.prototype.getUniqueID = function(key) {
    return key;
};

/**
 * The data represented as a map
 * @return {Object} The data
 */
DataCentre.prototype.getData = function() {
    return this.dataCentre;
};

/**
 * Get the value associated with the key from the data structure
 * will return undefined if key does not exist
 * @param  {String} key 
 * @return {Object}     
 */
DataCentre.prototype.get = function(key) {
    return this.dataCentre[key];
};

/**
 * Get the keys from the data structure
 * @return {String[]} 
 */
DataCentre.prototype.keys = function() {
    return Object.keys(this.dataCentre);
};

/**
 * Get the values associated with the keys from the data structure
 * @return {Object[]}
 */
DataCentre.prototype.values = function() {
    //todo - check underscorejs _.values        
};

/**
 * Check if the data structure is empty
 * @return {Boolean}
 */
DataCentre.prototype.isEmpty = function() {
    for (var prop in this.dataCentre) {
        return false;
    }
    return true;
};

/**
 * Retrieve the number of items in the data structure
 * @return {Number}
 */
DataCentre.prototype.size = function() {
    var count = 0;
    for (var prop in this.dataCentre) {
        count++;
    }
    return count;
};

/**
 * Check if the key exists in the data structure
 * @param  {String}  key
 * @return {Boolean}     
 */
DataCentre.prototype.hasKey = function(key) {
    return this.dataCentre[key] !== undefined;
};

/**
 * Empty the data structure
 * This function is chainable
 * @return {DataManager}
 */
DataCentre.prototype.clear = function() {
    this.dataCentre = {};
    return this;
};

/**
 * Remove an item from the data structure
 * This function is chainable
 * @param  {String} key 
 * @return {DataManager}     
 */
DataCentre.prototype.remove = function(key) {
    if (this.hasKey(key)) delete this.dataCentre[key];
    return this;
};

/**
 * Add an item to the data structure
 * This function is chainable
 * @param {String} key   
 * @param {DataManager} value 
 */
DataCentre.prototype.add = function(key, value) {
    this.dataCentre[key] = value;
    return this;
};