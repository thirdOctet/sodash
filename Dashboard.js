/**
 * The dashboard is a utility that allows users to receive an overview
 * of their workspace. Users can view data represented as a table,
 * chart (line, pie) or as custom html (We hope to extend this further)
 *
 * It implements the MVC paradigm so as to make it simpler
 *
 * Data:
 * Currently (17/08/2015)requests for data are sent to StreamServlet
 * StreamServlet will return a breakdown or timeseries depending on what
 * parameters are included within the url. For your requests to be effective
 * you must include the tagset you would like for the data to be rendered or
 * you will receive an exception message.
 *
 * Another source is based on DashboardServlet.java. These are included as part of the default
 * set of dashboard widgets available to the user. The default dashboard widgets are managed
 * in a file called DashboardDefaultWidgets.json.
 *
 * A request to DashboardServlet will look like:
 * http://workspace.soda.sh/dashboard/charts.json?name=(message-pie|message-timeseries|sentiment-pie) *
 *
 * The structure of the widgets contained within DashboardDefaultWidgets.json is closely tied to
 * WidgetCentre.js where DashboardWidget is defined. A complete functionality set is provided for
 * setting properties of the dashboard widget as well as exporting the widget contents for persistence
 * reasons.
 *
 * @example
 * 'http://workspace.soda.sh/stream.json?q=complaints&tagset=workflow&output=breakdown'
 *
 * For these dependencies to be met, please ensure they are added to simplepage.html
 * before this file is initialised. This file will normally be appended in the $scripts variable in simplepage
 *
 * @requires jQuery
 * @requires Underscore
 * @requires DataLoader
 * @requires Creole
 * @requires Templates
 * @requires DataCentre
 * @requires DashboardWidget
 * @requires Notifications
 * @requires GridsterWrapper
 * @requires DataRequestObject
 *
 * The initialisation process of the dashboard is as follows:
 *
 * - init() --> A new dashboard is created and initialised
 *     - Dashboard MVC initialised
 *     - controller.prepare
 *         - controller.prepareDashboard
 *             - controller.prepareWidgets
 *                 - controller.getWidgetsFromSource
 *                     - controller.widgetSourceToWidget
 *                 - controller.addWidgetsToModel
 *             - controller.prepareGrid
 *                 - controller.addGridsterPlaceholder
 *                 - controller.initGridsterInstance
 *             - controller.renderWidgetsToGrid
 *                 - controller.addDashboardWidgetsToGrid
 *                     - controller.prepareDashboardWidgetsForGrid
 *                         - controller.addDashboardWidgetToGrid
 *             - controller.ajaxifyWidgets
 *                 - controller.renderDataToWidget
 *                     - controller.parseDataToWidgetType
 *             - controller.ajaxifyRealTime
 *
 */
(function() {

    /**
     * The dashboard datacentre is based on the DataCentre implementation, which is essentially a dictionary
     * We have purposefully used inheritance as we may wish to handle the key,value pairing differently
     * in future implementations for reusability
     */
    function DashboardDataCentre() {}
    DashboardDataCentre.prototype = new DataCentre();

    //this allows us to call the inherited methods of the DataCentre
    //e.g. this.constructor.prototype.[function].call(params)
    DashboardDataCentre.prototype.constructor = DataCentre;

    /**
     * The dashboard implementation of getUniqueID focuses on using the
     * url as a way to identify a specific data request
     *
     * @param {DashboardWidget} dbw A dashboard widget object
     * @return {String} The key that will be used for the data centre
     */
    DashboardDataCentre.prototype.getUniqueID = function(dbw) {

        //in the case of the dashboard we are going to use the id of the widget as when a widget is created
        //or destroyed it will always have an id which is unique e.g. two streams with the same source will
        //have the same id, but the newest stream will contain a timestamp in teh format id-timestamp
        return dbw.getID();
    };

    /**
     * Add a widget to the dashboard data centre
     * @param {DashboardWidget} dbw
     */
    DashboardDataCentre.prototype.add = function(dbw) {

        //the url of the dashboard widget
        var key = this.getUniqueID(dbw);

        //Call the base class' add method
        this.constructor.prototype.add.call(this, key, dbw);
        return this;
    };


    /**
     * For more details refer to the information at the start of the document
     */
    function Dashboard() {

        /**
         * Can be referenced in the model, view or controller within this files lexical scope
         * @type {Dashboard}
         */
        var _dashboard = this;

        /**
         * Used as an indicator to determine if the dashboard has changed since it was loaded
         * It is closely tied to events 'Dashboard:Changed' and 'Dashboard:Saved'
         * 'Dashboard:Changed' is triggered in a number of sensible locations such as .. ctrl+f on your IDE and
         * search for $(document).trigger('Dashboard:Changed').
         * The event is handled in the controller since most handling is done by the controller
         * @type {Boolean}
         */
        var _dashboardChanged = false;

        /**
         * Determine if the current user can make amendments to the dashboard
         * This will influence the way in which the grid will render
         * If the user is on a mobile then they will not be able to render the dashboard
         * @type {Boolean}
         */
        this.userCanEdit = !window.isMobile && Roles.iCan('EditDashboard');

        /**
         * When initialised this will be an instance of gridster wrapper
         * it was originally stored against the controller however it has been
         * moved here for a much cleaner relationship
         * @type {GridsterWrapper}
         */
        this.gridster = undefined;

        /**
         * Uses this list to retrieve data for possible dashboard services
         * These are known dashboard sources
         * @return {Object}
         */
        this.knownServices = ['sogrow', 'saved', 'default']; //reports - 'streams' (dynamic sources)

        /**
         * The dashboard element
         * @type {jQuery}
         */
        this.$el = $('#dashboard');

        /**
         * Contains objects of data that the controller will be able to interact with
         * Typically the model will contain ways in which to retrieve data from a data object
         * This model houses multiple data objects (cache, saveDashboard, defaultWidgets, dataCentre)
         * The Controller will do most of the work to retrieve the data
         * @type {Object}
         */
        function Model() {

            /**
             * Cache any calculations made against the data store for future retrieval
             * @type {Object}
             */
            this.cache = {};

            /**
             * The dashboard data centre stores a DashboardWidget.html.id->DashboardWidget mapping within
             * a DataCentre object
             * @type {DashboardDataCentre}
             */
            this.dataCentre = new DashboardDataCentre();

            /**
             * The sources of the dashboard and their properties
             * These requests will always reply with an array of objects and their properties
             * Retrieves data on initialisation
             *
             * If you want to add more sources I have included examples below for future
             * products (mediawall|playlist) and perhaps some day reports which are based
             * on the latest report of that type.
             *
             * renderWidgetsToGrid will try and match the Roles.getRoles(Creole.user) against
             * this list. Any roles that are not matching the key in this.sources will be ignored
             * This will allow for users with multiple roles to be able to access multiple
             * dashboards
             *
             * One drawback is that it depends on matching the key of this.sources against the user role
             * If the name of the role changes then the dashboard for that service will break if not
             * updated here
             *
             *
             * @type {Object}
             */
            this.sources = {
                //dashboard widget sources for all workspaces
                'default': new DataRequestObject({
                    url: '/static/code/widgets/DashboardDefaultWidgets.json'
                }),
                //specific to separate products in the sodash ecosystem
                'sogrow': new DataRequestObject({
                    url: '/static/code/widgets/DashboardSogrowWidgets.json'
                }),
                //EXAMPLES for future reference
                // 'mediawall': new DataRequestObject({
                //     url: '/static/code/widgets/DashboardMediaWallWidgets.json'
                // }),
                // 'playlist': new DataRequestObject({
                //     url: '/static/code/widgets/DashboardPlayListWidgets.json'
                // })
                // 'reports' : {
                //     'traffic' : new DataRequestObject({
                //         url: 'reports.json?numres=1&detail=heavy&type=TrafficReport',
                //         type: 'DataLoader'
                //     }),
                //     'user' : new DataRequestObject({
                //         url: 'reports.json?numres=1&detail=heavy&type=UserReport',
                //         type: 'DataLoader'
                //     }),
                //     'activity' : new DataRequestObject({
                //         url: 'reports.json?numres=1&detail=heavy&type=ActivityReport',
                //         type: 'DataLoader'
                //     })
                // }
                //ALWAYS KEEP THIS OPTION LAST
                //it contains the configuration of existing widgets and merely extends their properties
                'saved': new DataRequestObject({
                    url: '/view/' + Creole.project + encodeURIComponent(':') + 'dashboard.json',
                    type: 'DataLoader'
                })
            };

        }

        /**
         * The controller controls the interactions between the model and view
         * @type {Object}
         */
        function Controller(model, view) {
            this.model = model;
            this.view = view;
        }

        /**
         * Contains the html required when rendering to a page
         */
        function View(model) {
            this.model = model;
        }

        /**
         * Contains the functions necessary for the controller to utilise when
         * rendering html elements to the DOM.
         */
        View.prototype = {
            /**
             * The dashboard placeholder
             * @return {String}
             */
            dashboardContainer: function() {
                return '<div id="dashboard-placeholder" class="creole-placeholder" title="Obtaining data for dashboard"></div>';
            },
            /**
             * This assumes that the global templates object is available
             * and that the dashboard templates have completed loading
             * @return {String} The html container for rendering a dashboard
             */
            dashboardPlaceholder: function() {
                return templates.Dashboard_Default();
            },

            /**
             * Indicator for the dashboard when resized
             * @return {String}
             */
            dashboardLoader: function() {
                return '<div class="dashboard-loading loading"></div>';
            },

            /**
             * Renders the container of a DashboardWidget using the properties
             * of the DashboardWidget
             * @param  {String} targetTemplate
             * @param  {Dashboardwidget} widget
             * @return {String} html template
             */
            renderWidgetContainerTemplate: function(targetTemplate, widget) {
                //useful for when template calls have not loaded and should be apart of the template loading chain
                assert(templates[targetTemplate], 'Dashboard.js - renderTemplate: ' + targetTemplate + ' is not loaded or is undefined');
                return templates[targetTemplate](widget);
            },

            /**
             * Renders the data from the return of a request
             * @param  {String} targetTemplate The template id within DashboardTemplates.html
             * @param  {Object} data           The logic for how the data is rendered is based on the type of DashboardWidget
             * @return {String}
             */
            renderWidgetTemplate: function(targetTemplate, data) {
                //useful for when template calls have not loaded and should be apart of the template loading chain
                assert(templates[targetTemplate], 'Dashboard.js - renderWidgetTemplate: ' + targetTemplate + ' is not loaded or is undefined');
                return templates[targetTemplate](data);
            },

            /**
             * Uses the properties of a DashboardWidget to create the container of
             * a DashboardWidget
             * @param  {Dashboardwidget} widget
             * @return {String}
             */
            renderWidgetContainer: function(widget) {
                return templates.Dashboard_Widget_Container(widget);
            },

            /**
             * Retrieves the html for the dashboard control buttons
             * @return {String} html
             */
            getDashboardButtons: function() {
                return templates.Dashboard_Buttons();
            },

            /**
             * Retrieves the Wizard template html
             * @return {String}
             */
            getDashboardWizard: function() {
                return templates.Dashboard_Widget_Wizard();
            },

            /**
             * Generates the settings modal for a DashboardWidget
             * @param  {DashboardWidget} widget
             * @return {String}        The dashboard widgets settings html
             */
            getDashboardWidgetSettings: function(widget) {
                return templates.Dashboard_Widget_Settings(widget);
            },

            /**
             * Generates a table containing the default widgets
             * @param  {DashboardWidget[]} widgets A list of default widgets
             * @return {String}
             */
            getDashboardWizardDefault: function(widgets) {
                return templates.Dashboard_Wizard_Add_Default_Widgets(widgets);
            },

            /**
             * Uses the properties tagSets and streams to populate the options
             * within the dropdown menu when adding streams
             * @param  {Object} properties {tagSets:[], streams: []}
             * @return {String}
             */
            getDashboardWizardStreams: function(properties) {
                return templates.Dashboard_Wizard_Add_Streams(properties);
            },

            /**
             * Renders a table based DashboardWidget
             * @param  {String} templateName
             * @param  {Object} breakdown    contains 'url' and 'breakdown' for the template to render
             * @return {String}              The rendered table
             */
            getDashboardTable: function(templateName, data) {
                var tmplt = templates[templateName];
                assert(tmplt, "Dashboard.js - no such template " + templateName);
                return tmplt(data);
            }
        };

        /**
         * The functions necessary to interact with the data objects within the model
         */
        Model.prototype = {

            /**
             * Get the sources object containing the sogrow items
             * @return {Object} String->DataRequestObject
             */
            getSources: function(){
                return this.sources;
            },

            /**
             * Get the list of sources
             * @return {String[]}
             */
            getSourcesKeys: function(){
                return _.keys(this.sources);
            },

            /**
             * Get the data request against the object
             * @param  {String} name
             * @return {DataRequestObject}
             */
            getSource: function(name){
                return this.sources[name];
            },

            /**
             * Add a new source dynamically
             * @param {String} name [description]
             * @param {String} url  [description]
             */
            addSource: function(name, url){
                this.sources[name] = new DataRequestObject({
                    url : url
                });
            },

            /**
             * A Recursive Deferred retriever on an object with nested DataRequestObjects
             * @param {Deferred[]}|undefined} deferredList
             * @param {DataRequestObject|undefined} sourceList
             * @return {Deferred[]}
             */
            getSourcesDeferreds: function(deferredList, sourceList){
                var deferreds = deferredList || [];
                var sources = sourceList || this.sources;

                _.each(sources, function(DRO, name){
                    if(DRO instanceof DataRequestObject){
                        deferreds.push(DRO.getDeferred());
                    } else {
                        deferreds.concat(this.getSourcesDeferreds(deferreds, DRO));
                    }
                }, this);

                return deferreds;
            },

            /**
             * Get a cached result from a previous calculation
             * @param  {String} key
             */
            getItem: function(key) {
                return this.cache[key];
            },

            /**
             * Cache a result from a calculation
             * @param  {String} key
             * @param  {AnyType} value
             */
            cacheItem: function(key, value) {
                this.cache[key] = value;
                return this;
            },

            /**
             * Parses a stringified saved dashboard and saves the result to the model.savedDashboard object
             * @param {String} data
             */
            convertSavedDashboardData: function(data) {
                var converted;
                try {
                    converted = JSON.parse(data.contents);
                } catch (e) {
                    console.log("Dashboard.js - convertSavedDashboardData: The format of the saved dashboard is incorrect");
                    converted = [];
                }
                return converted;
            },

            /**
             * Adds a DashboardWidget to the DataCentre
             * @param {DashboardWidget} widget
             */
            addWidget: function(widget) {
                this.dataCentre.add(widget);
                return this;
            },
            /**
             * Adds a list of DashboardWidgets to the DataCentre
             * @param {DashboardWidget[]} widgets
             */
            addWidgets: function(widgets) {
                _.each(widgets, function(Widget) {
                    this.addWidget(Widget);
                }, this);
                return this;
            },

            /**
             * Gets all the widgets from the data centre
             * @return {Object[]} {DashboardWidget.id -> DashboardWidget}[]
             */
            getAllWidgets: function() {
                return this.dataCentre.getData();
            },

            /**
             * Get DashboardWidgets by type
             * Allows only widgets of this type to be rendered to the Dashboard
             * @param  {String} type
             * @return {DashboardWidget[]}
             */
            getWidgetsByType: function(type) {
                var widgets = _.filter(this.dataCentre.getData(), function(dbw) {
                    return type === dbw.getType();
                });

                return widgets;
            },

            /**
             * Get DashboardWidgets by widget type
             * @param  {String} type A known type e.g. default, sogrow
             * @return {DashboardWidget[]}
             */
            getWidgetsByWidgetType: function(widgetType){
                var widgets = _.filter(this.dataCentre.getData(), function(dbw) {
                    return widgetType === dbw.getWidgetType();
                });

                return widgets || [];
            },

            /**
             * Get all dashboard widgets that are real time
             * @return {DashboardWidget[]}
             */
            getWidgetsByRealTime: function() {
                var widgets = _.filter(this.dataCentre.getData(), function(dbw) {
                    return dbw.isRealTime();
                });

                return widgets;
            }
        };

        /**
         * The controller is responsible for a lot of the interactivity between the
         * Dashboard and other components of the site
         */
        Controller.prototype = {

            /*********************************************************************************************************
             * VIEW-CONTROLLER INTEGRATION ***************************************************************************
             *********************************************************************************************************/

            /**
             * A jquery parsed html string
             * @param {String} html
             */
            parseHTML: function(html) {
                return $.parseHTML(html);
            },

            /**
             * The dashboard html container
             * @return {Controller} Chainable function
             */
            addDashboardPlaceholder: function() {
                $('#dashboard').append(this.parseHTML(this.view.dashboardContainer()));
                return this;
            },

            /**
             * Removes the dashboard placeholder from the dashboard when retrieving data
             * @return {Controller} Chainable function
             */
            removeDashboardPlaceholder: function() {
                $('#dashboard-placeholder').remove();
                return this;
            },

            /**
             * Adds the gridster placeholder to the dashboard
             * @return {Controller} Chainable function
             */
            addGridsterPlaceholder: function() {
                $('#dashboard').append(this.parseHTML(this.view.dashboardPlaceholder()));
                return this;
            },


            /*********************************************************************************************************
             * CONTROLLER-MODEL INTEGRATION **************************************************************************
             *********************************************************************************************************/

            /**
             * Checks against the model for saved widgets
             * @return {Boolean}
             */
            hasSavedWidgets: function() {
                return this.model.getSource('saved').getData().length > 0;
            },

            /**
             * Wrapper for adding widgets to the DashboardDataCentre within the model
             * @param {DashboardWidgets[]} widgets
             */
            addWidgetsToModel: function(widgets) {
                this.model.addWidgets(widgets);
                return this;
            },

            /**
             * Wrapper for adding a widget to the DashboardDataCentre within the model
             * @param {DashboardWidget} widget
             */
            addWidgetToModel: function(widget) {
                this.model.addWidget(widget);
                return this;
            },

            /*********************************************************************************************************
             * CONTROLLER-TEMPLATE INTEGRATION ***********************************************************************
             *********************************************************************************************************/

            /**
             * Retrieves the dashboard templates
             * @return {jQuery Deferred}
             */
            getDashboardTemplates: function() {
                return loadTemplates('/static/code/widgets/DashboardTemplates.html');
            },

            /**
             * Retrieves the reports templates
             * @return {jQuery Deferred}
             */
            getReportsTemplates: function() {
                return loadTemplates('/static/code/report/Reports.html');
            },

            /*********************************************************************************************************
             * CONTROLLER-UI INTEGRATION *****************************************************************************
             *********************************************************************************************************/

            /**
             * Checks if a dom element exists on the page
             * @param  {String} id The id or class of the element to be checked
             * @return {Boolean}
             */
            elementExists: function(id) {
                return $(id).length === 0 ? false : true;
            },

            /**
             * A utility for repeatedly checking against the presence of a dom element
             * before taking an action
             * @param  {String}   element                   The class or id of the element to be rendered
             * @param  {Function} callback  optional        The function to be executed based on presence of an element in the dom
             * @param  {Number}   milliseconds  optional    The time in milliseconds before checking for the element again
             */
            elementExistsTimer: function(id, callback, milliseconds, context) {

                if (!context) context = this;
                if (!milliseconds) milliseconds = 200;

                var timer = setInterval($.proxy(function() {
                    if ($(id).length > 0) {
                        clearInterval(timer);
                        context[callback]();
                    }
                }, context), milliseconds);

            },

            /*********************************************************************************************************
             * CONTROLLER-GRIDSTER INTEGRATION ***********************************************************************
             *********************************************************************************************************/

            /**
             * Initialises an instance of the GridsterWrapper
             * which prepares the gridster library for initialisation
             * @return {Controller}
             */
            initGridsterInstance: function() {

                //once the main elements of the dashboard have been prepared
                //we initialise a new grid from the library gridster
                _dashboard.gridster = new GridsterWrapper();

                _dashboard.gridster
                    //looks for '.gridster .gridster-container'
                    .prepareGridsterContainer()
                    //takes the width of the mainbody by default and sets the
                    //width of a widget based on a formula
                    .setWidgetWidth()
                    //takes the width of the mainbody by default and sets the
                    //width of gridster
                    .setGridsterWidth()
                    //set the drag location to be the header of the widget container
                    .extendGridsterOptions({
                        draggable: {
                            handle: '.dashboard-widget .panel-heading, .dashboard-widget .panel-heading h3'
                        }
                    })
                    //initilises the instance of gridster against the gridster container
                    //if usercanedit is false then there will be no option to resize the grid
                    //Other widget options such as close and settings are handled by updateDisplayBasedOnPermissions
                    .createInstance(_dashboard.getUserCanEdit())
                    //another check to ensure width is met, TODO revisit for testing
                    .setGridsterWidth();

                return this;

            },

            /*********************************************************************************************************
             * CONTROLLER-DASHBOARDWIDGET INTEGRATION ****************************************************************
             *********************************************************************************************************/


            /**
             * Convert a list of widget source objects to dashboard widgets
             * @param  {Object[]} widgets
             * @return {DashboardWidget[]}
             */
            getWidgetsFromSource: function(widgets) {
                var dbws = [];
                _.each(widgets, function(WidgetSource) {
                    //create the widgets
                    dbws.push(this.widgetSourceToWidget(WidgetSource));
                }, this);
                return dbws;
            },

            /**
             * This function will take an object and render the properties of an object
             * to a DashboardWidget. If the widget already exists then it will take the widget and extend
             * the properties. This is useful when saved widgets that are custom widgets are on the dashboard.
             *
             * Refer to WidgetCentre.js DashboardWidget.prototype.stringify/export
             * for properties that are exported
             * @param  {Object} widget
             * @return {DashboardWidget}
             */
            widgetSourceToWidget: function(widgetSource) {

                var dbw;

                //we should get the id as this will correlate with what might be a default
                //dashboard widget, we want to check if it already exists and if so merely
                //extend the parameters of the widget based on the saved version
                var id = widgetSource.id ? widgetSource.id : '';
                id = widgetSource.html && widgetSource.html.id ? widgetSource.html.id : id;

                //does this widget already exist i.e. is it a default widget saved with different parameters
                dbw = this.getDashboardWidgetByID(id) || new DashboardWidget(widgetSource);

                //the logic for different widget types is different
                dbw.setWidgetOptionalProperties(widgetSource);

                //set the template container of the widget
                dbw.setGenericWidgetContainer($(this.parseHTML(this.view.renderWidgetContainer(dbw))));

                return dbw;
            },

            /**
             * TODO
             * @param  {DashboardWidget} widget [description]
             * @return {String}        [description]
             */
            renderWidgetTemplate: function(widget) {
                //expects templates to exist
                assert(templates, "Dashboard.js - renderWidgetTemplate: templates is undefined");

                //uses the type of the widget to select the template to render to
                var targetTemplate = this.getDashboardTemplateType(widget);

                //calls the template with the dashboard widget properties
                return this.parseHTML(this.view.renderWidgetContainerTemplate(targetTemplate, widget));
            },

            /**
             * TODO
             * @return {String} The target template name, e.g. Dashboard_Generic_Chart
             */
            getDashboardTemplateType: function(widget) {
                //all templates will normally render to a generic template
                var targetTemplate = 'Dashboard_Generic';

                //use the custom template if it exists
                if (widget.getCustomTemplate()) return widget.getCustomTemplate();

                //use the type if it is set
                if (widget.getType()) return targetTemplate + '_' + widget.getType().capitalize();

                //use the generic template otherwise
                return targetTemplate;
            },

            /**
             * TODO
             * @return {Dashboardwidget[]}
             */
            getCustomDashboardWidgets: function() {
                //when a dashboard is saved the widgettype is changed to saved
                return this.getDashboardWidgetsByWidgetType('custom');
            },

            /**
             * Gets all dashboard widgets saved including custom and default widgets
             */
            getSavedDashboardWidgets: function() {

                var savedWidgets = _.filter(this.model.getAllWidgets(), function(dbw) {
                    return dbw.isSaved();
                });

                return savedWidgets;

            },

            /**
             * TODO
             * @param  {String} property default|custom
             * @return {DashboardWidgets[]}
             */
            getDashboardWidgetsByWidgetType: function(property) {

                //Cache requests on the model
                // if (this.model.getItem(property)) return this.model.getItem(property);

                var results = _.filter(this.model.getAllWidgets(), function(dbw) {
                    return dbw.getWidgetType() === property;
                });

                // this.model.cacheItem(property, results);

                return results;
            },

            /**
             * Get a dashboard widget
             * @param  {String} id The id of the dashboard widget
             * @return {[type]}    [description]
             */
            getDashboardWidgetByID: function(id) {

                var widget = _.find(this.model.getAllWidgets(), function(dbw) {
                    return dbw.getID() === id;
                }, this);

                return widget;
            },

            /**
             * Get all widgets that are default widgets
             * References DBW in model and result is cached in Model
             * @return {DasboardWidget[]}
             */
            getDefaultWidgets: function() {
                return this.model.getWidgetsByWidgetType('default');
            },

            /**
             * TODO
             * @param  {[type]} widget [description]
             * @return {[type]}        [description]
             */
            baseChartOptions: function(widget) {
                return {
                    options:{
                        chart: {
                            renderTo: widget.getContentID(),
                            type: widget.getChartType() || null,
                            spacingBottom: 0,
                            backgroundColor: null
                        },
                        title: {
                            text: null
                        },
                        plotOptions: {
                            series: {
                                marker: {
                                    enabled: false
                                },
                                shadow: false
                            }
                        },
                        legend: {
                            borderWidth: 0,
                            verticalAlign: 'bottom',
                            itemMarginTop: 5
                            // lineHeight: '16'
                            // y: -10,
                            // margin: 30
                        }
                    }
                };
            },

            /**
             * TODO
             * @param  {[type]} widget [description]
             * @return {[type]}        [description]
             */
            pieChartOptions: function() {
                return {
                    options:{
                        legend: {
                            navigation: {
                                activeColor: '#3E576F',
                                animation: true,
                                arrowSize: 10,
                                inactiveColor: '#CCC',
                                style: {
                                    color: '#333'
                                }
                            }
                        },
                        plotOptions: {
                            pie: {
                                size: '65%',
                                center: ['50%', '50%'],
                                showInLegend: true
                            }
                        }
                    }
                };
            },
            /**
             * TODO
             * @return {Object} The configuration for rendering a line chart
             */
            lineChartOptions: function(title) {
                return {
                    options:{
                        xAxis: {
                            type: 'datetime' || null
                        },
                        yAxis: {
                            title: {
                                text: title || tr('Messages per hour')
                            },
                            min: 0
                        },
                        legend: {
                            navigation: {
                                style: {
                                    lineHeight: '20px'
                                }
                            }
                        }
                    }
                };
            },

            /**
             * TODO
             * @param  {[type]} widget [description]
             * @return {[type]}        [description]
             */
            barChartOptions: function(widget) {},

            /**
             * The default configuration for creating gauges on the dashboard
             * @return {Object} The configuration necessary to render a gauge chart
             */
            solidGaugeChartOptions: function() {
                return {
                    options:{
                        pane: {
                            center: ['50%', '85%'],
                            size: '100%',
                            startAngle: -90,
                            endAngle: 90,
                            background: {
                                innerRadius: '60%',
                                outerRadius: '100%',
                                shape: 'arc'
                            }
                        },

                        tooltip: {
                            enabled: false
                        },

                        // the value axis
                        yAxis: {
                            stops: [
                                [0.1, '#DF5353'], // red
                                [0.5, '#DDDF0D'], // yellow
                                [0.9, '#55BF3B'] // green
                            ],
                            lineWidth: 0,
                            minorTickInterval: null,
                            tickPixelInterval: 400,
                            tickWidth: 0,
                            title: {
                                y: -70
                            },
                            labels: {
                                y: 16
                            }
                        },

                        plotOptions: {
                            solidgauge: {
                                dataLabels: {
                                    y: 5,
                                    borderWidth: 0,
                                    useHTML: true
                                }
                            }
                        }
                    }
                };
            },


            /**
             * TODO
             * @param  {[type]} widget [description]
             * @return {[type]}        [description]
             */
            parseWidgetForText: function(widget) {

            },

            /**
             * TODO
             * @param  {[type]} widget [description]
             * @return {[type]}        [description]
             */
            parseWidgetForList: function(widget) {

            },

            /**
             * TODO
             * @param  {[type]} widget [description]
             * @return {[type]}        [description]
             */
            parseWidgetForReport: function(widget) {

            },

            /**
             * TODO
             * @param  {[type]} widget [description]
             * @return {[type]}        [description]
             */
            parseWidgetForBar: function(widget) {

            },

            /**
             * Aids in the configuration and creation of
             * @param  {DashboardWidget} widget
             */
            parseWidgetForSolidgauge: function(widget) {

                //http://stackoverflow.com/questions/17780729/not-rendering-vu-meter-gauge-chart-using-highcharts-in-durandal
                widget.setChart({
                    options:{
                        series: [{
                            data: [widget.getData()[0].total || 0]
                        }]
                    }
                }).setChart(this.solidGaugeChartOptions());

                this.renderChartToGrid(widget);

            },

            /**
             * Renders table data for display
             * @param  {Dashboardwidget} widget
             */
            parseWidgetForTable: function(widget) {

                //get the widget html
                var templateType = this.getDashboardTemplateType(widget);

                var data = widget.getData();

                var convertedData = this.prepareBreakdownData(widget, data.breakdown);

                //reset the data representation
                var newRep = {
                    breakdown: convertedData,
                    query: data.query
                };

                var $html = this.parseHTML(this.view.getDashboardTable(templateType, newRep));

                this.viewRemoveWidgetLoaders(widget);

                //target the content id of the widget
                $('#' + widget.getContentID()).html($html);

            },

            /**
             * Prepares the data for rendering to a pie chart
             * @param  {[type]} data [description]
             * @return {Object[]}
             */
            convertBreakDownToPie: function(data) {
                var dataConverted = [];

                _.each(data, function(count, label) {
                    dataConverted.push({
                        'name': label,
                        'y': count
                    });
                });

                return dataConverted;
            },

            /**
             * Removes any tags that contain !
             * expects data.timeseries to be passed on timeseries data
             * @param  {Object} data Contains data and name of tag
             * @return {[type]}      [description]
             */
            filterTimeseries: function(data) {
                var filtered = _.filter(data, function(theData) {
                    return theData.name.indexOf('!') === -1;
                });

                return filtered;
            },

            /**
             * Converts the data.timeseries information into renderable format
             * for timeseries highcharts
             * @param  {Object[]} timeseries
             * @return {Object[]}
             */
            convertCustomTimeSeries: function(timeseries) {
                var dataConverted = [];
                _.each(timeseries, function(tagProperties) {
                    dataConverted.push({
                        'name': tagProperties.label,
                        'data': tagProperties.data
                    });
                });
                return dataConverted;
            },

            /**
             * TODO
             * @param  {DashboardWidget} widget [description]
             * @return {[type]}        [description]
             */
            viewRemoveWidgetLoaders: function(widget) {
                $('#' + widget.getID() + ' .dashboard-widget-content').find('.loading').fadeOut(function() {
                    $(this).remove();
                });
            },

            /**
             * TODO
             * @param  {[type]} data   [description]
             * @param  {[type]} widget [description]
             * @return {[type]}        [description]
             */
            renderChartToGrid: function(widget) {
                Chart.render(widget.getChartOptions());
                this.viewRemoveWidgetLoaders(widget);
            },

            /**
             * TODO
             * @param  {DashboardWidget} widget [description]
             * @return {[type]}        [description]
             */
            parseWidgetForPie: function(widget) {
                //there are two circumstances in which data requests can occur for a pie chart.
                //A request to DashboardServlet with params name=message-pie
                //or a request to streams with the params output=breakdown

                var originalData = widget.getData();
                var data = [];
                var series = {};

                //a custom chart will have to be prepared
                if (widget.propertyInParams('output')) {
                    data = this.prepareBreakdownData(widget, originalData.breakdown);
                    data = this.convertBreakDownToPie(data);
                    series = {
                        data: data
                    };
                } else {
                    series = originalData;
                }

                widget.setChart({
                    options:{
                        series: series
                    }
                }).setChart(this.pieChartOptions());

                this.renderChartToGrid(widget);
            },

            /**
             * Uses the response from a request to output=breakdown and filters the results
             * @param  {DashboardWidget} widget
             * @param  {Object} data
             * @return {Object}        The newly formatted array
             */
            prepareBreakdownData: function(widget, data) {
                var untagged = {};
                var convertedData = {};

                _.each(data, function(value, key) {
                    if (widget.getType() !== 'table' && key.indexOf('all') > -1) {
                        return;
                    } else if (key.indexOf('!') === -1) {
                        convertedData[key] = value;
                    } else if (key.indexOf('untagged') > -1) {
                        untagged[key] = value;
                    }
                }, this);

                var totalCount = 0;

                _.each(untagged, function(value) {
                    totalCount += value;
                });

                if (totalCount > 0) {
                    if (_.has(convertedData, 'untagged')) {
                        convertedData.untagged = convertedData.untagged + totalCount;
                    } else {
                        convertedData.untagged = totalCount;
                    }
                }

                //untagged will overshadow the data for charts
                //we remove this to aid the customer
                if (widget.getType() === 'chart') {
                    delete convertedData.untagged;
                }

                return convertedData;

            },

            /**
             * Parses a widget for timeseries display
             * @param  {DashboardWidget} widget
             */
            parseWidgetForTimeSeries: function(widget) {
                //there are two circumstances in which data requests can occur for a timeseries.
                //A request to DashboardServlet with params name=message-timeseries
                //or a request to streams with the params output=timeseries

                var data = widget.getData();

                //original timeseries format is {query : 'blah', timeseries: []}
                //the timeseries is extracted then filtered

                //if the params object contains the property 'output' then it is most likely a custom
                //timeseries widget, this assumption may fail as more options become available
                if (widget.propertyInParams('output')) {
                    data = this.convertCustomTimeSeries(data.timeseries);
                }

                //remove unwanted tags in the display
                data = this.filterTimeseries(data);

                widget.setChart({
                    options:{
                        series: data
                    }
                }).setChart(this.lineChartOptions());

                this.renderChartToGrid(widget);

            },

            /**
             * Determines the process flow based on the type of widget for the chart
             * @param  {DashboardWidget} widget
             */
            parseWidgetForChart: function(widget) {

                //prepare the chart type widget
                widget.setChart(this.baseChartOptions(widget));

                //get the type of chart to be rendered
                var chartType = widget.getChartType();

                //auto render unspecified types to a timeseries
                if (!chartType) {
                    this.parseWidgetForTimeSeries(widget);
                } else {
                    this['parseWidgetFor' + chartType.capitalize()](widget);
                }
            },

            /**
             * TODO
             * @param {[type]} widget [description]
             */
            viewAddWidgetContents: function(widget) {
                var widgetTemplate = this.parseHTML(this.view.renderWidgetTemplate(widget.getCustomTemplate(), widget.getData()));
                $('#' + widget.getContentID()).html(widgetTemplate);
            },

            /**
             * TODO
             * @param  {[type]} widget [description]
             * @return {[type]}        [description]
             */
            parseWidgetForCustom: function(widget) {
                this.viewAddWidgetContents(widget);
                this.viewRemoveWidgetLoaders(widget);
            },

            /**
             * TODO
             * @param  {DashboardWidget} widget
             * @return {[type]}        [description]
             */
            parseDataToWidgetType: function(widget) {

                //retrieve the type of the dashboardwidget
                var type = widget.getType();

                //enforcing a widget type allows us to handle different data sources
                assert(type, 'Dashboard.js - parseDataToWidgetType: a widget must have a type');

                //if the widget has a custom template then we render as follows
                if (widget.getCustomTemplate()) {
                    this.parseWidgetForCustom(widget);
                } else {

                    //ensure that a function exists for the type
                    assert(this['parseWidgetFor' + type.capitalize()], 'Dashboard.js - parseDataToWidgetType: Function missing, must create function parseWidgetFor' + type + ' for widget type ' + type);

                    //dynamically call rendering data function based on widget type
                    //this function must exist to render the widgets data
                    //the reason for this is that sources of data must be handled differently
                    //we manage the complexity of this by passing the type
                    this['parseWidgetFor' + type.capitalize()](widget);
                }

            },

            /**
             * Request the data for a widget and upon completion render the widget
             * data to the type of widget e.g. chart, table, pie e.t.c
             * @param  {DashboardWidget} widget
             */
            renderDataToWidget: function(widget) {

                //request data based on the current parameters of the widget
                widget.requestData();

                //let others know that the data was retrieved
                $(document).trigger('Dashboard:WidgetDataRequested', widget);

                //await the completion of the data request
                $.when(widget.getDeferred()).done($.proxy(function() {
                    this.parseDataToWidgetType(widget);

                    //let others know that the data was retrieved
                    $(document).trigger('Dashboard:WidgetDataComplete', widget);
                }, this));

                return this;

            },

            /**
             * Begin rendering data to widget contents
             * @return {[type]} [description]
             */
            ajaxifyWidgets: function() {

                //using proxy ensures that when setInterval is called
                //it uses this context and not the window object
                var timer = setInterval($.proxy(function() {

                    //use the ajaxed property to signify rendering of widget
                    //this will allow us to add new widgets and render the contents
                    var $widgets = this.getVisibleDashboardWidgets().not('.ajaxed');

                    if ($widgets.length > 0) {

                        _.each($widgets, function(widget) {

                            var dbw = $(widget).data('item');

                            this.renderDataToWidget(dbw);

                            $(widget).addClass('ajaxed');

                        }, this);
                    }

                }, this), 200);

            },

            /**
             * The realtime ajaxification of the dashboard will
             * look at each visible dashboard widget and
             * check the realtime property of the dashboard object
             *
             * This will then be used to retrieve the latest data if 5 minutes have passed
             */
            ajaxifyRealTime: function() {
                var timer = setInterval($.proxy(function() {
                    //TODO
                    //Test: check if ui widget item is equivalent to dataCentre item
                    var $widgets = this.getVisibleDashboardWidgets();

                    if ($widgets.length > 0) {

                        _.each($widgets, function(modal) {

                            var $modal = $(modal);

                            var dbw = $modal.data('item');

                            if (dbw.isRealTime()) {

                                //add the realtime indicator if not already done so
                                this.addRealTimeIndicator($modal, dbw);

                                //can we update it yet?
                                if (!dbw.canRealTime()) return;

                                //indicate that a request is being made
                                this.displayRealtimeLoadingIndicator(dbw);

                                //set the timestamp of the last request for data
                                dbw.setLastRequest(new Date().getTime());

                                //add the realtime indicator
                                //indicate to the user that a realtime request has taken place
                                //this will also say how long ago the request was made
                                this.viewUpdateRealTimeNotifier($modal, dbw);

                                //this will initiate the new data request and then update
                                //the contents of the dashboard widget
                                this.renderDataToWidget(dbw);

                                //the loading indicator will be removed when the data is returned
                                //this is because it is listening for $(document).trigger("Dashboard")
                            }

                        }, this);
                    }
                }, this), 500);
            },

            /**
             * TODO
             * @param {jQuery Dom Element} $modal
             * @param {DashboardWidget} dbw
             */
            addRealTimeIndicator: function($modal, dbw) {

                var lrTarget = '.panel-heading .dashboard-widget-lastRequest';

                //update the last request time based on the realtime updates
                var $lastRequest = $modal.find(lrTarget);

                if ($lastRequest.length === 0) {
                    $modal.find('.panel-heading').prepend(templates.Dashboard_Widget_Realtime_LastRequest({
                        timeStamp: dbw.getLastRequest()
                    }));
                    ajaxifyPrettyDates();
                    $lastRequest = $modal.find(lrTarget);
                    $lastRequest.removeClass('hidden');
                }
            },

            /**
             * Adds a notification to a realtime widget and updates the widget frequently
             * @param  {jQuery DOM Element} $modal The parent container of the dashboard widget
             * @param  {DashboardWidget} dbw
             */
            viewUpdateRealTimeNotifier: function($modal, dbw) {

                var lrTarget = '.panel-heading .dashboard-widget-lastRequest';

                //ensure that a realtime widget if the status is changed to
                //remove the notifier and terminate early
                if (!dbw.isRealTime()) {
                    $modal.find(lrTarget).remove();
                    return;
                } else {
                    this.addRealTimeIndicator($modal, dbw);
                }

                //update the last request time based on the realtime updates
                var $lastRequest = $modal.find(lrTarget);


                if ($lastRequest.length === 0) {
                    $modal.find('.panel-heading').prepend(templates.Dashboard_Widget_Realtime_LastRequest({
                        timeStamp: dbw.getLastRequest()
                    }));
                    $lastRequest = $modal.find(lrTarget);
                } else {
                    //allows us to reset the time of retrieval on the data
                    $lastRequest.find('.PrettyDate').attr('timestamp', dbw.getLastRequest());
                    $lastRequest.find('.PrettyDate').removeClass('ajaxed');
                }

                // //update the date
                ajaxifyPrettyDates();

                //only relevant of initialisation
                $lastRequest.removeClass('hidden');
            },

            /**
             * Add a widget to the gridster grid
             * @param {String} html  The html of the widget to be rendered
             * @param {Number} sizex The width of the widget on the grid
             * @param {Number} sizey The height of the widget on the grid
             * @param {Number} col   The y position of the widget in the grid
             * @param {Number} row   The x position of the widget in the grid
             */
            addDashboardWidgetToGrid: function(html, sizex, sizey, col, row) {

                assert(_dashboard.gridster.getGridsterInstance(), 'Dashboard.js - addDashboardWidgetToGrid: An instance of gridster has not been initialised');
                _dashboard.gridster.getGridsterInstance().add_widget(html, sizex, sizey, col, row);

                return this;
            },

            /**
             * Removes a widget from the dashboard if it is present
             * @param  {DashboardWidget} widget
             */
            removeDashboardWidgetFromGrid: function(widget) {
                var $modal = $('#' + widget.getID());
                if ($modal.length > 0) {
                    this.viewRemoveWidgetFromGrid($modal);
                }
            },

            /**
             * TODO
             * @param  {DashboardWidget} widget [description]
             * @return {Controller}        [description]
             */
            prepareDashboardWidgetForGrid: function(widget) {
                var html = widget.getRenderedTemplate();
                var sizex = Math.floor(widget.getSizeX());
                var sizey = Math.floor(widget.getSizeY());
                var col = widget.getCol();
                var row = widget.getRow();

                //handle the width of widgets differently based on
                //the type of device
                if (window.isMobile === true && $(window).width() < 767) {
                    sizex = _dashboard.gridster.getMaxColumns();
                }

                //here we ensure for all widgets that are added or removed that the html will contain the data-item
                $(html).data('item', widget);

                this.addDashboardWidgetToGrid(html, sizex, sizey, col, row);

                //allow others to act if the dashboard changes
                $(document).trigger('Dashboard:AddedWidget', widget);

                return this;
            },

            /**
             * TODO
             * @param {DashboardWidget[]} widgets [description]
             * @return {Controller}
             */
            addDashboardWidgetsToGrid: function(widgets) {
                //if the user decides to pass a single DashboardWidget
                widgets = !_.isArray(widgets) ? [widgets] : widgets;

                _.each(widgets, function(dbw) {

                    //adds the dashboardwidget to the grid
                    this.prepareDashboardWidgetForGrid(dbw);

                    //sets up the settings for the dashboard widget
                    this.viewAddDashboardWidgetSettings(dbw);

                }, this);

                return this;
            },

            /**
             * Renders Dashboard Widgets to the grid
             * @param {DashboardWidgets[]} existingWidgets This must be prepared before
             * @return {Controller}
             */
            renderWidgetsToGrid: function(existingWidgets) {

                //using existing widgets allows us to reset the dashboard
                var widgets = [];

                //get widgets based on type
                //has their been a set sogrow dashboard to load?
                var requested = $('#dashboard').data('dashboard') || "";

                //load dashboard widgets based on the role of the user
                if(Roles.iCan('UseSoDash')){
                    //load saved widgets as a priority
                    if (this.hasSavedWidgets()) {
                        //retrieves all widgets whose saved property = true
                        widgets = this.getSavedDashboardWidgets();
                    } else {
                        if(requested){
                            //get the requested list
                            var requestList = requested.split(',');
                            _.each(requestList, function(source){
                                widgets = widgets.concat(this.model.getWidgetsByWidgetType(source));
                            }, this);
                        } else {
                            widgets = this.model.getWidgetsByWidgetType('default');
                        }
                    }
                } else {
                    //ideally get a list of the services the user has access to
                    //not seeing this in Creole.user, possibly Creole.user.tags
                    assert(Creole.user, "Dashboard.js - renderWidgetsToGrid: Creole.user is not set and is needed to determine what dashboard");
                    var roles = Roles.PRODUCTS.split(/\s+/gi);

                    var showOnDashboard = [];

                    //sogrow is a role, if it is set, then we will check for it
                    _.each(roles, function(role){
                        if(Roles.iCan(role)){
                            showOnDashboard.push(role.toLowerCase().replace(/^use/gi,''));
                        }
                    }, this);

                    _.each(showOnDashboard, function(product){
                        //here we will only get the Dashboard items that are available
                        widgets = widgets.concat(this.model.getWidgetsByWidgetType(product));
                    });
                }


                this.addDashboardWidgetsToGrid(widgets);

                return this;
            },

            /**
             * Initialises an instance of gridster
             * prepare > prepareDashboard > prepareGrid
             * @return {[type]} [description]
             */
            prepareGrid: function() {
                this.addGridsterPlaceholder();
                this.initGridsterInstance();
                this.removeDashboardPlaceholder();
            },

            /**
             * Takes the saved and default objects, converts them to dashboard widgets
             * and adds them to the model datacentre
             *
             * prepare > prepareDashboard > prepareWidgets
             */
            prepareWidgets: function() {

                //add all widget sources to the model
                //loop through all the data sources and start building widgets

                //all we want to do is add all the sources to the model
                var widgets = [];

                _.each(this.model.getSources(), function(DRO, source){
                    var droData = DRO.getData();
                    var droWidgets = this.getWidgetsFromSource(droData);
                    widgets = widgets.concat(droWidgets);
                }, this);

                this.addWidgetsToModel(widgets);

                return this;
            },

            /**
             * Set the saved property of a widget
             * @param  {DashboardWidget[]} widgets
             * @return {DashboardWidget[]}
             */
            prepareSaved: function(widgets) {
                _.each(widgets, function(dbw) {
                    dbw.setSaved();
                }, this);

                return widgets;
            },

            /**
             * TODO
             * @param  {[type]} data [description]
             * @return {[type]}      [description]
             */
            prepareTrafficReport: function(source, data) {


            },

            /**
             * TODO
             * @param  {[type]} data [description]
             * @return {[type]}      [description]
             */
            prepareUserReport: function(source, data) {

            },

            /**
             * TODO
             * @param  {[type]} data [description]
             * @return {[type]}      [description]
             */
            prepareActivityReport: function(source, data) {

            },

            /**
             * Sets the report data retrieved from the deferred request
             * @param {[type]} source [description]
             * @param {[type]} data   [description]
             */
            setReportData: function(source, data) {

                //making a request to get report data returns an array of reports or an empty array
                //this should not be undefined
                assert(data, 'Dashboard.js - setReportData: Cannot set report data of undefined');

                source.reports[data.subtype] = data && data.length === 0 ? undefined : data.shift();
            },

            /**
             * TODO
             * @return {[type]} [description]
             */
            prepareDashboardWizardReports: function(sources) {

                //a call to an unknown report will return an empty array
                var sourceConfig = {
                    numres: 1,
                    detail: 'heavy'
                };

                //TODO Refactor this

                //TODO we want to do this dynamically as the report names may change
                var latestTrafficReport = DataLoader.get('/reports.json', _.extend(sourceConfig, {
                    type: 'TrafficReport'
                }));

                var latestUserReport = DataLoader.get('/reports.json', _.extend(sourceConfig, {
                    type: 'UserReport'
                }));

                var latestActivityReport = DataLoader.get('/reports.json', _.extend(sourceConfig, {
                    type: 'ActivityReport'
                }));

                //we ensure that by using this in the following calls we always refer to
                //the context of the controller
                $.when(latestTrafficReport).done($.proxy(function(data) {
                    this.setReportData(sources, data);
                }, this));

                $.when(latestUserReport).done($.proxy(function(data) {
                    this.setReportData(sources, data);
                }, this));

                $.when(latestActivityReport).done($.proxy(function(data) {
                    this.setReportData(sources, data);
                }, this));

                // $.when(latestActivityReport).done(this.prepareActivityReport, this, sources));

                //since the reports data has now been retrieved
                //and prepared we will prepare the reports for viewing
                // $.when(latestTrafficReport, latestUserReport, latestActivityReport).done($.proxy(this.processReports, this, sources));

            },



            /**
             * Saves the changes when the save button is clicked on the
             * widget settings
             */
            saveWidgetSettings: function(event) {

                var button = $(event.target);

                //this is the upper most container of the settings wizard for a widget
                var $modal = $(button).closest('.modal');

                //assumptions
                //all modals are saved in the format - settings-<%= context.getID() %>
                //to target this modal's widget we will substring 'settings-' to get the id of the widget
                var modalID = $modal.attr('id');
                var widgetID = modalID.substr(modalID.indexOf('-') + 1, modalID.length);
                var $widget = $('#' + widgetID);
                var dbw = $widget.data('item');

                //serialise the form
                var props = $modal.find('form').serializeObject();

                //prepare the properties for the dashboard widget
                props = this.prepareProperties(props);

                //this will allow default widgets with settings options to render to their url
                //and not adjust the datasource contents affecting their ability to get more data
                if (dbw.getWidgetType() !== 'default') {
                    //take any further action that might affect the way charts are rendered
                    dbw = this.removeStartEndFromStream(dbw);
                }

                //now set the properties of the dashboard widget
                dbw.setWidgetOptionalProperties(props);

                //If we are saving changes to a widget we should check if a chart is being created
                //During the process of updating the widget and its data we will reinitialise the chart properties
                //This ensures that changes to the time period is reflected correctly in the chart timeline
                if (dbw.getType() === 'chart') dbw.resetChart();

                //update the UI and contents of the widget from the change
                this.updateDashboardWidget(dbw);

                //once the dashboard widget is saved we will hide the modal for the user
                $modal.find('button[data-dismiss="modal"]').click();

                //Allow others to act when the widget is saved
                $(document).trigger("Dashboard:WidgetUpdated", dbw);

            },

            /**
             * This handles the edge case where streams with start and end dates
             * are ignored on dashboard widgets. It removes start/end and uses these properties
             * from the dashboard widget instead
             * @param  {DashboardWidget} dbw
             * @return {DashboardWidget}
             */
            removeStartEndFromStream: function(dbw) {


                var queryObject = Url.parseQuery(dbw.getUrl());


                if (queryObject.start) delete queryObject.start;

                if (queryObject.end) delete queryObject.end;

                var rebuiltUrl = [];

                //produces a data request friendly format for the values of a query
                //handles such things as '(' e.t.c
                _.each(queryObject, function(value, key) {
                    rebuiltUrl.push([key, escape(value)].join('='));
                });

                var url = rebuiltUrl.join('&');

                dbw.setUrl(url);

                return dbw;
            },

            /**
             * TODO
             * @param  {Object} props The widget source before being used to create a dashboard widget
             * @return {Object}
             */
            prepareProperties: function(props) {

                if (props.type === 'chart') {
                    $.extend(true, props, {
                        chart: {
                            type: props.chartSubType
                        }
                    });
                }

                props.output = this.getDataFormat(props);

                return props;

            },


            /**
             * Changes the title of the widget on a dashboard
             * @param  {DashboardWidget} widget
             */
            viewUpdateWidgetTitle: function(widget) {
                $('#' + widget.getID() + ' .widget-title').text(widget.getTitle());
            },

            /**
             * Update the html of the widget
             * @param  {DashboardWidget} widget
             */
            updateDashboardWidget: function(widget) {
                //at this stage we have given the dashboard widget a new set of properties

                //update the title if it has changed
                this.viewUpdateWidgetTitle(widget);

                //reset the height of the widget container in case of changes
                this.viewSetWidgetHeight(widget);

                //rerender the data result to the widget contents
                this.renderDataToWidget(widget);

            },

            /**
             * Removes the loader when a data request is complete
             * Tied to event Dashboard:DataComplete
             * @param  {DashboardWidget} widget
             */
            hideWidgetDataLoader: function(widget) {
                var $modal = $('#' + widget.getID());
                var $loading = $modal.find('.dashboard-widget-content .loading');

                if ($modal.length > 0 && $loading.length > 0) {
                    $loading.fadeOut(250);
                }
            },

            /**
             * Adds the loader indicator to the widget contents whilst data is being retrieved
             * @param  {DashboardWidget} widget
             */
            displayWidgetDataLoader: function(widget) {
                var $modal = $('#' + widget.getID());
                var $loading = $modal.find('.dashboard-widget-content .loading');
                var $content = $('#' + widget.getContentID());

                //does the realtime widget exist?
                //we let the realtime widget handle the the indication of a change
                if ($modal.find('.dashboard-widget-lastRequest').length > 0) return;

                if ($modal.length > 0 && $loading.length === 0) {
                    $modal.find('.dashboard-widget-content').append(templates.Misc_loading());
                }
            },

            /**
             * Retrieves the data-row and data-col attributes of a widget
             * and updates the DashboardWidget object
             * @param  {DashboardWidget} widget
             * @return {Dashboardwidget}
             */
            updateWidgetGridRowCol: function(widget) {

                var $widget = $('#' + widget.getID());

                //data-row and data-col are attributes that are applied
                //by gridster
                var row = $widget.attr('data-row');
                var col = $widget.attr('data-col');

                //we set the position of the widget based on the above attributes
                widget.setRow(Math.floor(row)).setCol(Math.floor(col));

                return widget;
            },

            /**
             * Update the X and Y properties of the dashboardwidget
             * @param  {DashboardWidget} dbw   The dashboardwidget to be updated
             * @return {[type]}       [description]
             */
            updateWidgetXY: function(widget) {

                var $modal = $('#' + widget.getID());

                widget.setSizeX($modal.attr('data-sizex')).setSizeY($modal.attr('data-sizey'));

                return widget;

            },

            /**
             * Takes the visible widgets on the dashboard, adds any extra properties
             * and exports the dashboard wizard object
             * @return {DashboardWidget[]} An exported list of DashboardWidgets
             */
            prepareSaveDashboard: function() {

                //get all widgets that are not hidden
                var dashboardWidgets = this.getVisibleDashboardWidgets().not(':hidden');

                var widgetsSource = [];

                _.each(dashboardWidgets, function(widget) {

                    //get the widget
                    var dbw = $(widget).data('item');

                    //lets update the position of the widgets on the grid
                    //i.e set the row and col of the widget
                    this.updateWidgetGridRowCol(dbw);

                    //also update the height(sizey) and width(sizex) of the widgets when saving
                    this.updateWidgetXY(dbw);

                    //a custom widget should be handled differently from a default widget or report widget
                    dbw.setSaved();

                    //add the exported properties that will then be JSON.stringified
                    widgetsSource.push(dbw.export());

                }, this);

                return widgetsSource;

            },


            /**
             * Submits the dashboard configuration to the server
             * @param  {DashboardWidget[]} configuration An array of exported dashboard widget properties
             * @return {Deferred}
             */
            persistDashboard: function(configuration) {

                //this is the url to post to when saving a dashboard
                var url = '/generic/' + Creole.project + ':dashboard';
                var text = [];

                //
                try {
                    text = JSON.stringify(configuration, null, "   ");
                } catch (e) {
                    text = [];
                    console.log(e);
                }

                //these are the parameters that must be included when saving the dashboard
                var data = {
                    text: text,
                    jclass: 'Text',
                    action: 'save-publish',
                    grpname: Creole.project
                };

                //post the configuration to the server
                return $.ajax({
                    url: url,
                    type: 'post',
                    datatype: 'json',
                    data: data
                });
            },

            /**
             * TODO
             * @param  {[type]} deferredRequest [description]
             * @return {[type]}                 [description]
             */
            notify: function(deferredRequest) {
                assert(deferredRequest, "Dashboard.js - notify: You must pass a deferred in order to notify the user");
                deferredRequest.done(
                    $.proxy(this.notifyUser, this, {
                        type: 'info',
                        text: 'Your dashboard configuration is saved.'
                    })).fail($.proxy(function(error) {
                    console.log(error);
                    this.notifyUser({
                        type: 'error',
                        text: 'There was an error updating your dashboard, please contact support@sodash.com for assistance.'
                    });

                    //TODO provide some feedback when dashboard is being saved
                    //e.g spinner
                }, this));
            },


            /**
             * 1. Exports visible widgets on the dashboard
             * 2. Places an ajax request to generic servlet to save the stringified dashboard
             * 3. Notifies the user of the result
             * 4. Triggers the dashboard saved event
             */
            saveDashboard: function() {

                //We export the widgets in a friendly format
                var widgetToSource = this.prepareSaveDashboard();

                //The list of widgets are then pused to the server
                var request = this.persistDashboard(widgetToSource);

                //The request returns a deferred, when the result is complete it is then
                //passed to Notification to update the user
                this.notify(request);

                $(document).trigger('Dashboard:Saved');
            },

            /**
             * Notify the user from a notification type
             * @param  {Object} config an object with teh properties 'info' and 'text'
             */
            notifyUser: function(config) {
                Notifications.post(config);
            },

            /**
             * Reset the dashboard to the default widgets
             */
            resetDashboard: function() {

                // event.preventDefault;
                //rather than page reload we will reinitialise the dashboard with the default
                //here we are going to use the save dashboard method to reset the configuration for the
                //next time we load the dashboard.
                this.notify(this.persistDashboard([]));

                //safely refresh the page on any device
                $(location).attr('href', window.location.href);

            },

            /**
             * Remove a widget from the grid
             * @param  {[type]} event [description]
             */
            viewRemoveWidgetFromGrid: function(event) {
                var $modal = $(event.target).closest('.dashboard-widget');

                //when a widget was removed previously it may not have the data against as
                //part of the js garbage collection.
                //Here we retrieve this by getting the widget from the data centre
                var dbw = this.getDashboardWidgetByID($modal.attr('id'));

                //Provide other domains of the code with the opportunity to take an action
                $(document).trigger('Dashboard:RemovedWidget', dbw);
                _dashboard.gridster.getGridsterInstance().remove_widget($modal);
            },

            /**
             * Controls the display of the sub type of the chart dropdown option in main type
             */
            viewUpdateDashboardWizardChartDropdown: function(event) {
                var $typeDropdown = $(event.target);

                var selection = $typeDropdown.attr('value');

                var $subTypeDropdown = $typeDropdown.closest('form').find('select[name="chartSubType"]');

                if (selection !== 'chart') {
                    $subTypeDropdown.closest('.form-group').addClass('hidden');
                } else {
                    $subTypeDropdown.closest('.form-group').removeClass('hidden');
                }

            },

            /**
             * TODO
             * @param  {jQuery} event
             */
            viewAddDashboardStreamsWidget: function(event) {

                //get the properties from the form
                var props = this.getFormPropertiesForWidget(event);

                //further prepatory steps when creating a new dashboard widget from the streams menu option
                props = this.prepareProperties(props);

                //see Dashboard_Wizard_Stream_Options
                if (props.title.length === 0) props.title = decodeURIComponent(props.tagsetName);

                //in this case we will create a new dashboard widget but not allow widgetSourceToWidget
                //to retrieve an existing dashboard widget
                if ($('#' + props.id).length > 0) props.id = props.id + '-' + new Date().getTime();

                //create an instance of dashboardWidget with the new form properties
                var dbw = this.widgetSourceToWidget(props);

                //We check for a comparison between any widgets with the same datasource
                //parameters, if it is the same source data then we notify the user
                if (this.widgetExistsOnDashboard(dbw)) {

                    //TODO show errors in widget settings and not through notifications
                    Notifications.post({
                        type: 'info',
                        text: 'This dashboard widget already exists.'
                    });
                    return;
                }

                dbw = this.removeStartEndFromStream(dbw);

                //add the widget to the data centre
                this.addWidgetToModel(dbw);

                //this is what adds the widget to the grid and ties into the
                //entire dashboard rendering process
                this.addDashboardWidgetsToGrid([dbw]);

                //some UI elements might want to be updated when this happens
                $(document).trigger('Dashboard:WizardAddedWidget', dbw);

            },



            /**
             * TODO
             * @param {DashboardWidget} dbw check if the dashboard widget exists on the dashboard
             * @return {Boolean}
             */
            widgetExistsOnDashboard: function(dbw) {
                var $widgets = this.getVisibleDashboardWidgets();
                var result = _.find($widgets, function(widget) {
                    return $(widget).data('item').getComparator() === dbw.getComparator();
                }, this);

                //result will actually be the html element that was
                if (result) return true;
                return false;
            },

            /**
             * get the jquery result of all .dashboard-widget on the dashboard
             * @return {jquery}
             */
            getVisibleDashboardWidgets: function() {
                return $('.dashboard-widget');
            },

            /**
             * Sets the output type of the widget
             * @param  {[type]} props
             * @return {String}
             */
            getDataFormat: function(props) {

                //from the properties we are going to determine what type of data format
                //is required
                //if we manually add a stream then we must decide what the data format is
                //i.e. a request for a table or pie chart depends on the same data request type
                if (props.type === 'chart' && !props.chart.type) {
                    return 'timeseries';
                }

                // the default format for all data requests
                return 'breakdown';

            },

            /**
             * TODO
             * @param  {[type]} event [description]
             * @return {[type]}       [description]
             */
            getFormPropertiesForWidget: function(event) {
                var $form = $($(event.target).closest('form'));
                var props = $form.serializeObject();
                //check if the form contains the stream property
                //refer to Dashboard_Wizard_Stream_Options

                //In Streams we use the url and id of the stream
                //but these are stored as a stringified object against
                //the input value within a form
                if (props.streamid) {
                    var $option = $form.find('select[name="streamid"] :selected');
                    props.url = $option.attr('data-url');
                    props.tagsetName = $option.attr('data-tagsetName');
                    props.id = props.streamid;
                }

                return props;
            },

            /**
             * TODO
             * @param  {[type]} widgets [description]
             * @return {[type]}         [description]
             */
            viewUpdateDashboardWizardDefault: function(widgets) {

                var timer = setInterval($.proxy(function() {
                    var $wizardDefault = $('#wizard-default');
                    if ($wizardDefault.length > 0) {
                        clearInterval(timer);

                        var wizardDefaultContents = this.view.getDashboardWizardDefault(widgets);

                        //set the contents of the dashboard widget wizard
                        $wizardDefault.html(wizardDefaultContents);

                    }
                }, this), 200);
            },

            /**
             * Updates the dashboard wizard stream menu contents allowing users to add
             * new custom stream based widgets
             * @param {Object} properties object containing filtered streams and tagsets
             */
            viewUpdateDashboardWizardStreams: function(properties) {
                var timer = setInterval($.proxy(function() {
                    var $wizardStreams = $('#wizard-streams');
                    if ($wizardStreams.length > 0) {
                        clearInterval(timer);

                        //get dashboard wizard streams form controls for adding and removing widgets
                        //Calls  templates.Dashboard_Wizard_
                        var streamsTemplate = this.view.getDashboardWizardStreams(properties);

                        //add the template
                        $wizardStreams.html(streamsTemplate);
                    }
                }, this, properties), 200);
            },

            /**
             * TODO
             */
            sortTagSets: function(TagSetA, TagSetB) {
                if (TagSetA.name.toLowerCase() < TagSetB.name.toLowerCase()) return -1;
                if (TagSetA.name.toLowerCase() > TagSetB.name.toLowerCase()) return 1;
                return 0;
            },

            /**
             * Sort stream alphabetically based on stream name
             */
            sortStreams: function(StreamA, StreamB) {
                if (StreamA.name.toLowerCase() < StreamB.name.toLowerCase()) return -1;
                if (StreamA.name.toLowerCase() > StreamB.name.toLowerCase()) return 1;
                return 0;
            },

            /**
             * TODO Move to TagSets.js
             * @return {TagSets[]} A sorted list of tagsets
             */
            filterTagSets: function() {
                var filteredTagSets = _.filter(Creole.tagSets, function(TagSet) {
                    return Creole.getTopLevelTagSet(TagSet.name);
                });

                return filteredTagSets.sort(this.sortTagSets);
            },

            /**
             * Remove all streams that do not have a stream.json type request
             * @return {Streams[]} A sorted list of streams
             */
            filterStreams: function() {
                var filteredStreams = _.filter(Creole.streams, function(Stream) {
                    return Stream.url.indexOf('/stream') > -1;
                });
                return filteredStreams.sort(this.sortStreams);
            },


            /******************************************************************************************
             * DASHBOARD WIZARD FUNCTIONS *************************************************************
             ******************************************************************************************/

            /**
             * TODO
             * @return {[type]} [description]
             */
            prepareDashboardWizardStreams: function() {

                var properties = {};

                //we only allow streams that are published to be available to
                //the menu options
                $.proxy(_.extend(properties, {
                    tagSets: this.filterTagSets(),
                    streams: this.filterStreams()
                }), this);

                this.viewUpdateDashboardWizardStreams(properties);
            },

            /**
             * TODO
             * @return {[type]} [description]
             */
            prepareDashboardWizardDefault: function() {
                //get the default widgets
                var widgets = this.getDefaultWidgets();

                //set up the default dashboard widgets to be rendered to the dashboard wizard
                this.viewUpdateDashboardWizardDefault(widgets);

            },


            /**
             * TODO
             * @return {[type]} [description]
             */
            prepareDashboardWizard: function() {

                //when we are sure that the resources are available
                $.when(this.model.getSource('default').getDeferred()).done($.proxy(this.prepareDashboardWizardDefault, this));

                //
                this.prepareDashboardWizardStreams();

            },

            /******************************************************************************************
             * DASHBOARD EVENT DRIVEN FUNCTIONS *******************************************************
             ******************************************************************************************/

            /**
             * This will be triggered when a user clicks on the row of the list of default widgets
             * For simplicity this function will check if the widget is on the dashboard and artificially
             * click the close icon which will trigger a series of events
             *
             * In the case of adding the dashboard to the grid an event is thrown which will allow
             * others who care about the event to act accordingly e.g. scrolling to a widget when added
             * @param  {jQuery} event
             */
            viewDashboardWizardDefaultWidgetToggle: function(event) {

                //what row was clicked - we store the id of the widget against the
                //tr as data-id, this way we can loook in the datacentre from our Model
                //and get information about it
                var $row = $(event.target).closest('tr');

                //the icon which reflects the state of the default widgets
                //we will have to update the icon
                var $icon = $row.find('.toggle-widget');

                //get the dashboard widget based on the id of the clicked row
                var dbw = this.getDashboardWidgetByID($row.attr('data-id'));

                //target the modal of the dashboard widget
                var $modal = $('#' + dbw.getID());

                //if the widget is on the grid
                if ($modal.length > 0) {
                    //artificially remove the widget
                    $modal.find('.remove-widget').click();
                } else {
                    //add the widget
                    this.addDashboardWidgetsToGrid(dbw);

                    //notify others that a widget was added through the wizard
                    $(document).trigger('Dashboard:WizardAddedWidget', dbw);
                }

            },


            /**
             * A series of actions to take when a widget is added or removed
             * @param  {DashboardWidget} widget
             * @return {[type]}        [description]
             */
            updateDashboardWizardUI: function(widget, event) {

                //allow the dashboard wizard to update the visibility state of the widget
                //in the default dashboard table
                this.updateDashboardWizardDefaultState(widget, event.type);

                //other elements of the dashboard UI that should be displayed
            },

            /**
             * Toggle the icon of the list of dashboard widgets in the dashboard
             * wizard when the user enables or disables a dashboard widget
             * @param  {DashboardWidget} widget
             * @param  {String} action The name of the event that was triggered e.g. Dashboard:AddedWidget
             */
            updateDashboardWizardDefaultState: function(widget, action) {

                //ignores any changes that are not related to a default widget
                if (!widget && !(widget instanceof DashboardWidget)) return;

                //get the id of the widget
                var id = widget.getID();

                //check if the table row of the widget exists in the default region of the dashboard wizard default section
                var $row = $('#wizard-default').find('tr[data-id="' + id + '"]');

                if (action !== 'Dashboard:AddedWidget') {
                    $row.find('.toggle-widget').removeClass('icomoon-checkmark visible').addClass('icomoon-close');
                    return;
                }

                $row.find('.toggle-widget').removeClass('icomoon-close').addClass('icomoon-checkmark visible');

            },

            /**
             * This is only called when a widget is added to the dashboard
             * If you need to wait for the dashboard widget to be displayed and update
             * when present this function will aid you
             * @param  {Dashboard} widget
             */
            updateDashboardWidgetUI: function(widget) {

                //when the element is visible or is already visible then we will take action
                var $modal = $('#' + widget.getID());

                var timer = setInterval($.proxy(function() {
                    if ($modal.length > 0) {
                        clearInterval(timer);

                        //when a widget is resized or added we might want to take certain actions
                        this.viewUpdateWidgetHeight(widget);
                    }
                }, this), 200);
            },

            /**
             * Updates the dimensions of a widget
             * If the contents contain a chart then this will be handled
             * as expected
             * @param  {DashboardWidget} widget
             */
            viewUpdateWidgetHeight: function(widget) {

                //we always update the height of any widget
                this.viewSetWidgetHeight(widget);

                if (widget.getType() !== 'chart') return;

                var $modal = $('#' + widget.getID());

                var $content = $modal.find('.dashboard-widget-content');

                var timer = setInterval($.proxy(function() {

                    var highchart = $modal.find('.highcharts-container').parent().highcharts();

                    if (highchart) {

                        clearInterval(timer);

                        //this will have already been set by setWidgetHeight for the content height
                        highchart.setSize($content.width(), $content.height());

                    }
                }, this), 200);

            },

            /**
             * Sets the widget height so that overflowing content
             * can be clipped using overflow in css
             * @param {DashboardWidget} widget
             */
            viewSetWidgetHeight: function(widget) {

                //get the widget modal
                var $modal = $('#' + widget.getID());

                var $content = $modal.find('.dashboard-widget-content');

                $content.height(this.calculateWidgetHeight($modal));

            },

            /**
             * Calculates the height of a widget
             * @param  {jquery DOM Element} $modal
             * @return {Number}
             */
            calculateWidgetHeight: function($modal) {
                var $content = $modal.find('.dashboard-widget-content');
                var $header = $modal.find('.panel-heading').outerHeight();
                var $resizeHandle = $modal.find('.gs-resize-handle').height();

                //get the padding of the content
                var pl = $content.css('paddingLeft'); /* can be undef */
                var padding = pl ? Math.floor(pl.replace("px", "")) : 0;

                //set the height of the widget based on the contents
                return $modal.height() - $header - padding * 2 - $resizeHandle;
            },

            /**
             * When a widget is added we will scroll to it
             * @param  {DashboardWidget} widget
             */
            viewScrollToWidget: function(widget) {

                var $modal = $('#' + widget.getID());

                var timer = setInterval($.proxy(function() {

                    if ($modal.length > 0) {
                        clearInterval(timer);
                        //scroll to added widget
                        $.scrollTo(0, $modal, {
                            gap: {
                                y: (-$('#header').outerHeight() - 10) // Scroll to the element top minus header and toolbar height
                            },
                            animation: {
                                duration: 500,
                                complete: function() {}
                            }
                        });
                    }

                }, this), 200);

            },

            /******************************************************************************************
             * DASHBOARD INITIALISATION FUNCTIONS *****************************************************
             ******************************************************************************************/

            /**
             * TODO
             * @param {DashboardWidget} widget
             */
            viewAddDashboardWidgetSettings: function(widget) {
                var dbwSettingsHtml = this.parseHTML(this.view.getDashboardWidgetSettings(widget));
                $('body').append(dbwSettingsHtml);
            },

            /**
             * TODO
             */
            viewAddDashboardWizard: function() {
                this.prepareDashboardWizard();
                var dashboardWizard = this.parseHTML(this.view.getDashboardWizard());
                $('body').append(dashboardWizard);
            },

            /**
             * TODO
             */
            viewAddDashboardWizardButtonControls: function() {
                if ($('.widget-controls').length > 0) return;
                var btns = this.parseHTML(this.view.getDashboardButtons());
                $('.WizardLinks').prepend(btns);
            },

            /**
             * Gets all the visible dashboard widgets and toggles them based on the users
             * ability to edit the dashboard
             */
            updateDisplayBasedOnPermissions: function() {
                var $widgets = this.getVisibleDashboardWidgets();

                //handles the add and remove of the remove icon and settings icon for a widget
                //the dashboard buttons and gridster are handled separately
                var $elements = $widgets.find('.widget-settings, .remove-widget');

                _dashboard.getUserCanEdit() ? $elements.removeClass('hidden') : $elements.addClass('hidden');

            },

            /**
             * TODO
             * @param  {DashboardWidget} widget [description]
             */
            hideRealtimeLoadingIndicator: function(widget) {
                //find the realtime loading indicator
                var $modal = $('#' + widget.getID());
                var $indicator = $modal.find('.panel-heading .dashboard-widget-lastRequest .loading');

                if ($indicator.length > 0) {
                    $indicator.fadeOut();
                }
            },

            /**
             * TODO
             * @param  {Dashboardwidget} widget
             */
            displayRealtimeLoadingIndicator: function(widget) {

                var $modal = $('#' + widget.getID());
                var $indicator = $modal.find('.panel-heading .dashboard-widget-lastRequest .loading');

                $indicator.css('display', 'inline-block').fadeIn();

            },

            /**
             * Rebuilds the dashboard when it is resized
             * It is simpler to destroy the gridster container and recreate it
             * This will avoid problems in the future with ghost resizing areas and other issues
             * we will always keep this container so that gridster can be rerendered to it
             */
            viewRebuildDashboardOnResize: function() {

                //we wait slightly when the browser is resized
                var timer = setInterval($.proxy(function() {

                    //get the resized width for comparison to the previous width of the gridster container
                    var currentWidth = $('#mainbody').width();

                    //get the existing element of gridster
                    var $gridster = $(_dashboard.gridster.getGridsterElement());

                    //recreate the dashboard if the size of the dashboard has changed
                    //this will accommodate mobile orientation changes
                    if (($gridster.width() + 6) !== currentWidth) {

                        //cease checks
                        clearInterval(timer);

                        //animations do not have a context handler
                        var _this = this;

                        //show friendly fade out of dashboard and replace with loader
                        $gridster.fadeTo('250', 0, function() {

                            //this will rebuild a saved or default dashboard
                            //It will ignore any newly added widgets
                            _this.rebuildDashboard();
                        });
                    }
                }, this), 50);
            },

            /**
             * Contains the logic to rebuild the dashboard
             */
            rebuildDashboard: function() {

                var $dashboard = $('#dashboard');

                //add the loading indicator to the dashboard
                $dashboard.append($(this.view.dashboardLoader()));

                //TODO the current configuration, export it, and pass it to the dashboard creator
                //export the current dashboard
                //var widgetSource = this.prepareSaveDashboard();

                //remove the gridster container completely
                $dashboard.find('.gridster').remove();

                //initiate the dashboard loading process
                this.prepareDashboard();

                //creates the illusion that the dashboard is loading
                //There is no event thrown when a dashboard is loaded - this creates the illusion that something is happening
                //but we already have the data and it renders very quickly
                $dashboard.find('.dashboard-loading').fadeTo('500', 0, function() {
                    $(this).remove();
                });

            },

            /**
             * Add the button controls and the dashboard wizard
             */
            prepareDashboardControls: function() {

                //we do not add the wizard if the user is on a mobile or does not have permissions
                if (!_dashboard.getUserCanEdit()) return;

                var timer = setInterval($.proxy(function() {
                    if ($('.WizardLinks').length > 0) {
                        clearInterval(timer);
                        this.viewAddDashboardWizardButtonControls();
                        this.viewAddDashboardWizard();
                    }
                }, this), 200);
            },

            /**
             * This will prepare the dashboard for rendering. This includes
             * setting up gridster, waiting for an instance of gridster to initialise,
             * preparing widgets, rendering widgets to the grid, updating the content of widgets,
             * preparing the dashboard controls and initialising realtime on widgets
             * @param {DashboardWidgets[]} existingWidgets Allows for the dashboard to be re rendered
             */
            prepareDashboard: function() {

                // //only for when a dashboard is resized, we take advantage of this by
                // //checking if the widgets property is set
                // var existingWidgets = widgets;

                //we always reset the dashboard before preparing it
                //other functions may wish to use this
                _dashboard.gridster = undefined;

                //an instance of gridster is initialised by prepareGrid()
                //sometimes it is not available so we check until it is initialised
                var timer = setInterval($.proxy(function() {

                    if($('#dashboard').length >0){
                        clearInterval(timer);

                        //initialise the grid
                        this.prepareGrid();

                        //Add the widgets to the model as DashboardWidgets
                        this.prepareWidgets();

                        //visually add the container of widgets to the dashboard
                        this.renderWidgetsToGrid();

                        //the widget contents have not been populated yet
                        //we have simply added the widget shell whilst
                        //retrieving the data. This will update the widget contents
                        this.ajaxifyWidgets();

                        //enable or disable the controls for persisting the dashboard
                        this.prepareDashboardControls();

                        //determine what controls should be displayed based on whether the
                        //user can edit the dashboard from the dashboard settings
                        this.updateDisplayBasedOnPermissions();

                        //we trigger the realtime ajaxification of widgets that have the realtime widget enabled
                        this.ajaxifyRealTime();

                    }
                }, this), 200);

                return this;
            },

            /**
             * TODO
             */
            prepare: function() {

                //ensure the dashboard element exists before attempting to initialise
                var timer = setInterval($.proxy(function(){
                    if($('#dashboard').length > 0){
                        clearInterval(timer);

                        //When the #dashboard dom element exists we will add the dashboard placeholder
                        //using the addDashboardPlaceholder function from the view
                        this.addDashboardPlaceholder();

                        //allow specific sources to be loaded
                        //check what is within the dom
                        var dashboardSources = [];
                        var requested = $('#dashboard').data('dashboard') || "";

                        //Did a dev set the selected dashboard options?
                        if (requested) {
                            dashboardSources = requested.split(',');
                        } else {
                            dashboardSources = this.model.getSourcesKeys();
                        }

                        //get all widgets and wait until received
                        var sourceDeferreds = this.model.getSourcesDeferreds();
                        var templates = [this.getDashboardTemplates(), this.getReportsTemplates()];
                        var requests = sourceDeferreds.concat(templates);

                        //this should only be done for saved dashboard widgets
                        var savedDro = this.model.getSource('saved');
                        savedDro.getDeferred().done($.proxy(function(data){
                            var preparedSavedData = this.model.convertSavedDashboardData(data);
                            savedDro.setData(preparedSavedData);
                        }, this));

                        //process the widgets when the data is retrieved
                        //call prepare once templates and data is retrieved
                        $.when.apply($,requests).done($.proxy(this.prepareDashboard, this));
                    }
                }, this), 20);

                return this;

            }

        }; // .Controller()

        /**
         * Get if the dashboard has changed
         * @return {Boolean}
         */
        this.isUnsaved = function() {
            return _dashboardChanged;
        };

        /**
         * Set whether the dashboard has changed
         * @param {Boolean} bool
         */
        this.setUnsaved = function(bool) {
            _dashboardChanged = bool;
        };

        /**
         * Actions to take when a dashboard is initialised
         */
        this.init = function() {

            //initialise the MVC
            var model = new Model();
            var view = new View(model);
            this.controller = new Controller(model, view);

            //initialise prerequisites for the dashboard
            this.controller.prepare();

            /*****************************************************************************************
             * EVENTS ********************************************************************************
             *****************************************************************************************/

            /**
             * Save the settings of a widget when the widget properties are changed through the UI
             */
            $(document).on('click', '.save-widget-settings', $.proxy(this.controller.saveWidgetSettings, this.controller));

            /**
             * Save the dashboard
             */
            $(document).on('click', '#save-dashboard', $.proxy(this.controller.saveDashboard, this.controller));

            /**
             * Reset the dashboard to the default widgets
             */
            $(document).on('click', '#reset-dashboard', $.proxy(this.controller.resetDashboard, this.controller));

            /**
             * Remove a widget from the grid
             */
            $(document).on('click', '.remove-widget', $.proxy(this.controller.viewRemoveWidgetFromGrid, this.controller));

            /**
             * Add a custom dashboard widget from the dashboard wizard streams menu option
             */
            $(document).on('click', '.add-custom-widget', $.proxy(this.controller.viewAddDashboardStreamsWidget, this.controller));

            /**
             * This is tied to the Dashboard Wizard Setting in which a change to the type of widget updates
             * the widget settings wizard
             * See template - Dashboard_Widget_Settings_Type
             */
            $(document).on('change', '.widget-settings select[name="type"], .add-custom-stream select[name="type"]', $.proxy(this.controller.viewUpdateDashboardWizardChartDropdown, this.controller));

            /**
             * Switches a default widget on or off depending on the status of the dashboard widget
             */
            $(document).on('click', '.toggle-default-widget', $.proxy(this.controller.viewDashboardWizardDefaultWidgetToggle, this.controller));

            /**
             * Allow actions to be taken when a widget is updated
             * @param  {DashboardWidget}
             */
            $(document).on('Dashboard:WidgetUpdated', $.proxy(function(event, widget) {
                this.updateDashboardWidgetUI(widget);
                this.viewUpdateRealTimeNotifier($('#' + widget.getID()), widget);
                $(document).trigger('Dashboard:Changed');
            }, this.controller));

            /**
             * Determine what action to take when a widget is added
             */
            $(document).on('Dashboard:AddedWidget', $.proxy(function(event, widget) {
                this.updateDashboardWizardUI(widget, event);
                this.updateDashboardWidgetUI(widget);
            }, this.controller));

            /**
             * Determine what action to take when a widget is removed
             */
            $(document).on('Dashboard:RemovedWidget', $.proxy(function(event, widget) {
                this.updateDashboardWizardUI(widget, event);
                $(document).trigger('Dashboard:Changed');
            }, this.controller));

            /**
             * When a widget on the grid is resized we are going do some actions
             * @param  {jQuery event} event
             * @param  {DOM Element} $modal This will be the .dashboard-widget container that is provided by gridster
             */
            $(document).on('Gridster:Resized', $.proxy(function(event, $modal) {
                $modal = $($modal);
                this.viewUpdateWidgetHeight($modal.data('item'));
                $(document).trigger('Dashboard:Changed');
            }, this.controller));


            /**
             * Allow newly added widgets from the dashboard wizard to be scrolled to
             * @param  {DashboardWidget}
             */
            $(document).on('Dashboard:WizardAddedWidget', $.proxy(function(event, widget) {
                this.viewScrollToWidget(widget);
                $(document).trigger('Dashboard:Changed');
            }, this.controller));

            /**
             * When a change is made to the dashboard set the dashboard state to true
             */
            $(document).on('Dashboard:Changed', $.proxy(this.setUnsaved, this, true));

            /**
             * When a dashboard is saved then update the dashboard saved value
             */
            $(document).on('Dashboard:Saved', $.proxy(this.setUnsaved, this, false));

            /**
             * If the dashboard has changed then this will attempt to inform the user
             * We use the dashboard object to determine if we should provide teh user with the option
             */
            $(window).on('beforeunload', function() {

                //_dashboard.isUnsaved() is updated when the events
                //'Dashboard:Changed' or 'Dashboard:Saved' are called
                if (_dashboard.isUnsaved()) {

                    //we set this as Notifications.js will not fire a notification when the isUnloading property
                    //is set if a user cancels the unloading process
                    window.isUnloading = false;

                    return "You have unsaved dashboard changes, if you would like to save your changes please cancel and click save on the dashboard.";
                }
            });

            /**
             * Allows us to signal that a request is complete
             */
            $(document).on('Dashboard:WidgetDataComplete', $.proxy(function(event, widget) {
                this.hideRealtimeLoadingIndicator(widget);
                this.hideWidgetDataLoader(widget);
                this.viewUpdateWidgetHeight(widget);
            }, this.controller));

            /**
             * Allow some ui element or anything to listen for when data is requested
             */
            $(document).on('Dashboard:WidgetDataRequested', $.proxy(function(event, widget) {
                this.displayWidgetDataLoader(widget);
            }, this.controller));

            /**
             * Handle changes to the orientation of a screen when displaying the dashboard
             * The context will always be the controller object of the dashboard
             */
            $(window).on('resize onorientationchange', $.proxy(this.controller.viewRebuildDashboardOnResize, this.controller));
        };

    } // .Dashboard()

    /**
     * UserCanEdit is immediately determined on dashboard initialisation
     * This is dependent on the role of the user which will
     * give them privileges to modify the dashboard
     * @return {Boolean}
     */
    Dashboard.prototype.getUserCanEdit = function() {
        return this.userCanEdit;
    };

    //create a new instance of the dashboard and initialise
    var dashboard = new Dashboard();
    dashboard.init();

    //allow global access to the dashboard
    //through Creole - Creole.cache.dashboard
    Creole.cacheItem('dashboard', dashboard);

    /**********************************************************************
     * EVENTS                                                             *
     * All events are bound through the controller                        *
     * Controller object                                                  *
     **********************************************************************/

}());

/*********************************************************************************
 * LEGACY CODE
 *********************************************************************************/

/**
 * Loads message or follower data from the supplied url. Automatically adds the
 * `.json` page extension, and `numres=1` url parameter. Adds to the callback
 * context `this.pageUrl`, that provides a link to the page for the relevant url
 * and parameters. If there is no data returned, the returned promise is
 * rejected.
 *
 * TODO document the html assumptions, and lifecycle
 *
 * @param {String} url - Url to fetch the data from. `.json` is always appended
 * to the end of the supplied value.
 * @param {Object} params - Map of options to be appended to the url. `numres=1`
 * is always appended to this map.
 * @returns A <a href="http://api.jquery.com/jQuery.ajax/#jqXHR">jqXHR object</a>.
 *
 * Is loaded in dashboardservlet.java
 **/
function getLatestMessageData(url, params) {
    return DataLoader.get(url + '.json', $.extend({}, params, {
        numres: 1
    })).always(function() {
        this.pageUrl = url + '?' + $.param(params);
    }).then(function(data, status, jqXHR) {
        if (data.length === 0) {
            return $.Deferred().rejectWith(this, [jqXHR]);
        } else {
            return $.Deferred().resolveWith(this, [data, status, jqXHR]);
        }
    });
}

/**
 * Renders the default dashboard view of the latest messages and followers for
 * the current user.
 * @param {String} [xid=Creole.user.xid] - Xid of the user for which the data
 * should be obtained and displayed.
 * @param {String} [name=Creole.user.name] - Name to display for the user.
 * @param {String} [img=Creole.user.img] - Url for the avatar of the user.
 **/
templates.fn.getUserSummary = function(options) {
    options = $.extend({
        xid: Creole.user.xid,
        name: Creole.user.name,
        img: Creole.user.img
    }, options);

    getLatestMessageData('/inbound', {
        to: 'me',
        as: options.xid
    }).then(function(data, status, jqXHR) {
        $('#dashboard-latest-inbox').html(templates.Dashboard_LatestMessage({
            xid: options.xid,
            img: options.img,
            url: this.pageUrl,
            location: 'Inbox',
            message: data[0]
        }));

        ajaxifyPrettyDates();
    }).fail(function() {
        $('#dashboard-latest-inbox').html(templates.Dashboard_NoMessages({
            xid: options.xid,
            img: options.img,
            url: this.pageUrl,
            location: 'Inbox'
        }));
    });

    getLatestMessageData('/inbound', {
        facet: 'FAO',
        assigned: '@!me or @!trueme',
        tag: '-closed and -future and -tomorrow and -!none-of-topic',
        xid: options.xid
    }).then(function(data, status, jqXHR) {
        if (data.length === 0) {
            return $.Deferred().rejectWith(this, jqXHR);
        }

        $('#dashboard-latest-task').html(templates.Dashboard_LatestMessage({
            xid: options.xid,
            img: options.img,
            url: this.pageUrl,
            location: 'Tasks',
            message: data[0]
        }));

        ajaxifyPrettyDates();
    }).fail(function() {
        $('#dashboard-latest-task').html(templates.Dashboard_NoMessages({
            url: this.pageUrl,
            location: 'Tasks'
        }));
    });

    getLatestMessageData('/people', {
        reln: 'followers',
        xid: options.xid
    }).then(function(data, status, jqXHR) {
        if (data.length === 0) {
            return $.Deferred().rejectWith(this, jqXHR);
        }

        $('#dashboard-latest-followers').html(templates.Dashboard_RecentFollowers({
            xid: options.xid,
            followers: data
        }));

        ajaxifyPrettyDates();
    }).fail(function() {
        $('#dashboard-latest-followers').html(templates.Dashboard_NoFollowers({
            url: this.pageUrl,
            location: 'Followers'
        }));
    });
};

/**
 * Produces the html of a table from the context
 * @param {Object} context.breakdown The data to be rendered
 * @param {String} query the request url
 * @return {String} The html to be outputted
 */
templates.fn.Dashboard_RenderTable = function(context) {

    var html = '';

    var keys = _.keys(context.breakdown);

    //sort table alphabetically
    keys.sort(function(a, b) {

        a = a.toLowerCase();
        b = b.toLowerCase();

        if (a < b) return -1;

        if (a > b) return 1;

        return 0;

    });

    var filterName = 'tag';

    // Assign-to should be treated as its own filter rather than as a tagset
    var tagsetName = Url(context.query).query.tagset;
    if (tagsetName) tagsetName = tagsetName.replace(/^tagset=/i, '');
    if (tagsetName === 'assign-to') filterName = 'assigned';


    // We'll need a list of tags which have user-friendly aliases for display, so we can translate them back when we construct a query
    if (!Creole.tagAliases) {
        Creole.tagAliases = {};
        for (var i = 0; i < Creole.tagSets.length; i++) {
            if (Creole.tagSets[i].showNoneOf) {
                Creole.tagAliases[Creole.tagSets[i].showNoneOf] = '!none-of-' + Creole.tagSets[i].name;
            }
        }
    }

    /* Total first, for easy viewing */
    var total = context.breakdown.all ? context.breakdown.all : 0;
    var allStream = new Stream(decodeURI(context.query));
    html += "<tr><th><a href='" + allStream + "'>Total</a></th><th>" + total + "</th></tr>";


    _.each(keys, function(key, i, list) {
        if (key === 'all') return;

        var n = context.breakdown[key];
        var stream = new Stream(decodeURI(context.query));

        // Insert correct string into query for tags like !none-of-sentiment, which displays as "neutral"
        // Construct full 'untagged:tagsetname' string for 'untagged'
        if (Creole.tagAliases[key]) {
            stream.addFilter(filterName, Creole.tagAliases[key]);
        } else if (key === 'untagged') {
            stream.addFilter(filterName, key + ':' + tagsetName);
        } else {
            stream.addFilter(filterName, key);
        }

        //if you want dashboard table urls to display all data for the query then
        //uncomment this
        // if(getIfDefined(stream, 'filters.start')){
        //   delete stream.filters.start;
        // }

        html += "<tr><td><a href='" + stream + "'>" + key + "</a></td><td>" + n + "</td></tr>";

    });

    return html;
};

/**
 * Get all users online and offline ordered by name with
 * online showing first
 * @param {Object} context The result of /creole/groups.json?detail=heavy
 */
templates.fn.Dashboard_WhosOnline = function(context) {

    //get the members based on teh project
    var members = context.groups[Creole.project].members;

    //sort profiles by name
    members = _.sortBy(members, function(User) {
        return User.name.toLowerCase();
    });

    //filter members by sodash users
    var onlineMembers = [];
    var offlineMembers = [];

    //assign members to online/offline state
    _.each(members, function(User) {
        if (User && User.service !== 'soda.sh') return;
        User && User.online ? onlineMembers.push(User) : offlineMembers.push(User);
    });

    //merge ordered offline after ordered online
    members = onlineMembers.concat(offlineMembers);

    return members;
};

window.getStreamPrefix = function(stream) {
    switch (stream.params.servlet) {
        case '/stream':
            return '';
        case '/inbound':
            return 'Inbound: ';
        case '/outbox':
            return 'Outbound: ';
        case '/people':
            return 'People: ';
        default:
            return '';
    }
};

window.getStreamDescription = function(stream) {
    var ret = '';

    // Does it have a user-defined name?
    if (stream.name) {
        return stream.name;
    }

    // Display the query first.
    if (stream.params['q']) {
        ret += stream.params['q'];
    }

    // Display all other parameters (except servlet).
    for (var paramName in stream.params) {
        if (!stream.params[paramName] || paramName === 'servlet' || paramName === 'q') {
            continue;
        }

        // HACK Hide the service 'nofresh'.
        if (paramName === 'service' && typeof stream.params[paramName] === 'string' && stream.params[paramName].contains('nofresh')) {
            continue;
        }

        // Insert a newline between parameters.
        if (ret.length > 0) {
            ret += '\n';
        }

        // Begin the line with the parameter name.
        ret += paramName + ': ';

        if (_.isArray(stream.params[paramName])) {
            ret += stream.params[paramName].join(',');
        } else {
            ret += stream.params[paramName];
        }
    }

    // Fallback to name if blank
    if (ret.length === 0) {
        return stream.name;
    }

    return ret;
};
