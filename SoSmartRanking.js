var Rank = function() {

    //rank terms?
    var ranktitle = ranktitle || {};

    var rankList = ["beginner", "intermediate", "advanced", "superuser"];

    /**
     * Determine the rank of the user based on the users completed tasks
     * @return {String} The resultant rank of the user based on a set of criteria
     */
    this.determineRank = function() {
        //sort completed tasks by level
        var userListByLevel = soSmartMediator.getUserCompletedTutorialsByLevel();

        var taskListByLevel = soSmartMediator.getTutorialsOrganisedByLevel();

        var userRank = "beginner";

        var rankIndex = 0;

        var levelStatus = levelStatus || [];

        //loop over what the user has completed
        _.each(taskListByLevel, function(tutorials, level, list) {

            //lets trully determine if the user has not completed the tutorials
            //get the objects which have not been completed for each level
            var difference = _.difference(taskListByLevel[level], userListByLevel[level]) || [];

            //store the result
            levelStatus.push(difference);
        });

        var statusTracker = statusTracker || [];

        //now lets check to see what has been completed
        _.each(levelStatus, function(remainingTutorials, key, list) {

            if (key === 0) {
                if (remainingTutorials.length > 0) {
                    userRank = rankList[key];
                } else if (remainingTutorials.length === 0) {
                    userRank = rankList[key + 1];
                }
            } else {
                if (userRank !== rankList[key - 1]) {
                    if (remainingTutorials.length > 0) {
                        userRank = rankList[key];
                    } else if (remainingTutorials.length === 0) {
                        userRank = rankList[key + 1];
                    }
                }

            }
        });

        return userRank;
    };

    /**
     * [rankBuilder description]
     * @param  {[type]} level           [description]
     * @param  {[type]} userListByLevel [description]
     * @param  {[type]} taskListByLevel [description]
     * @return {[type]}                 [description]
     */
    this.rankBuilder = function(level, userListByLevel, taskListByLevel) {

        var userRank = "";

        if (userListByLevel[level]) {
            //get the total number of tutorials
            if (userListByLevel[level].length < taskListByLevel[level].length) {
                userRank = "beginner";
            } else if (userListByLevel[level].length === taskListByLevel[level].length) {
                userRank = "intermediate";
            }
        }
    };

};

/**
 * The scoring system used to score sodash users based on tasks completed
 * This is closely connected to the level of the task
 * Scoring will be based on 2^level for each completed task within that level
 */
var Score = function() {
    /**
     * for scope based issues within other library bound scopes
     * such as loops e.g. $.each or _.each
     */
    $this = this;

    /**
     * Add points to the score of the user
     * @param {Number} level    The level of the completed task
     * @returns {Number}        The calculated points
     */
    this.addPoints = function(level) {
        return Math.pow(2, level);
    };

    /**
     * Takes the total number of tasks completed by the user and returns the accumulated score
     * @param  {Person} user Typically Creole.user
     */
    this.calculatePoints = function(completedtutorials) {
        var completedTasks = !completedtutorials ? soSmartMediator.getUserCompletedTutorialsOrderBySlug() : completedtutorials;
        var points = 0;
        _.each(completedTasks, function(value, key) {
            points += $this.addPoints(value.level);
        });
        return points;
    };

};