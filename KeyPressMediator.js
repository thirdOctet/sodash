/**
 * This object keeps track of the binding of keypresses to functions
 */
var KeyPressMediator = (function() {

    var _instance;
    /**
     * If you wish to take advantage of the key press handler
     * @type {Object}
     */
    var _keyPressToFunctionMap = {
        222: 'keyPressAt',
        192: 'keyPressAt',
        8: 'keyPressBackSpace',
        188: 'keyPressComma',
        46: 'keyPressDelete',
        40: 'keyPressDown',
        35: 'keyPressEnd',
        13 : 'keyPressEnter',
        27 : 'keyPressEscape',
        106: 'keyPressNumpadMultiply',
        33 : 'keyPressPageUp',
        34 : 'keyPressPageDown',
        32 : 'keyPressSpace',
        9: 'keyPressTab',
        38: 'keyPressUp',
        80: 'keyPressP',
        67: 'keyPressC',
        78: 'keyPressN'
    };

    /**
     * [getKeyPressFn description]
     * @param  {[type]} key [description]
     * @return {[type]}     [description]
     */
    var _getKeyPressFn = function(key){
        return _keyPressToFunctionMap[key] !== undefined ? _keyPressToFunctionMap[key] : -1;
    };

    /**
     * Publically available methods
     */
    var init = function() {
        return {
            getKeyPressFn: function(key) {
                return _getKeyPressFn(key);
            }
        };
    };

    /**
     * Here have your singleton object
     */
    return {
        getInstance: function() {
            if (!_instance) {
                _instance = init();
            }
            return _instance;
        }
    };

})();