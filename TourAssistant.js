/**
 * The TourAssistant library is used to add additional functionality to the hopscotch tours
 * It uses the singleton pattern as per Addy Osmani
 *
 * The TourAssistant is responsible for:
 * -- creating Tours and TourSteps
 * -- extending hopscotch tour methods such as onShow so that multiple methods can be called against the method
 * -- keeping track of the tours available
 * -- setting, deleting and updating the cookie of an active tour
 * -- determining when a tour should be displayed - adds delays to cross page elements as our html is dynamic
 * -- waiting for an element to be displayed as opposed to depending on setting the delay property of a Tour{}
 * -- ensuring the tour fails politely with informative notifications
 * -- initialising default settings on Tour{} objects e.g. removing the cookie when the Tour is closed or when it finishes
 *
 * It should be noted that a number of tours require some preconditions in order for them to function correctly
 * e.g. the twitter authorisation tour should not be an admin, and should also have a twitter service enabled on the workspace
 *
 * The tourAssistant object contains the inheritance scheme used to enable the application of multiple methods against
 * a Tour{} and TourStep{}.
 * The prototype model is used and works fairly well except in cases were methods are tied to events such as click. --> This is a work in progress
 *
 * @depends cookie.js
 * @depends Creole
 * @depends Hopscotch
 * @depends jquery
 * @depends underscore
 * @depends SoSmartMediator
 * @depends Notifications
 *
 * @author Jonathan
 */

var TourAssistant = (function() {

    /**
     * The singleton representation of the TourAssistant object
     */
    var instance;

    /**
     * A reference to itself in order for method calls to be executed in external libraries
     * @type {TourAssistant{}}
     */
    var $this = this;

    /**
     * The default cookiename for all tours
     * @type {String}
     */
    var _defaultCookieName = 'sodash-tour';

    /**
     * Store hopscotch tours
     * @type {Tour{}}
     */
    var _hopscotchTourStore = _hopscotchTourStore || {};

    /**
     * Refer to the current tour being modified
     */
    var _currentTour = _currentTour || {};

    /**
     * Refer to the current tour step being modified
     */
    var _currentTourStep = _currentTourStep || {};

    /**
     * The method called when the tour assistant is initialised
     * based on the Singleton pattern by Addy Osmani - Learning javascript design patterns
     */
    var init = function() {
        return {

            /**
             * Get the default cookie name
             * @return {String} The preset cookie name for all tours
             */
            getDefaultCookieName: function() {
                return _defaultCookieName;
            },

            /**
             * Add hopscotch tour to tour object
             * Includes the setup to teh tour object
             * @param {Tour{}} Tour The hopscotch tour object
             */
            addHopscotchTour: function(Tour) {
                //pre initialise tour with steps
                // this.attachTourSteps(Tour, TourSteps);

                //the tour should trigger on these states for multipage items
                var stateTrigger = this.getMultiPageStepsToCheck(Tour);

                $.extend(Tour, {
                    state: stateTrigger
                });

                //lets ensure that all defaults are set up for the tours
                this.initTourDefaults(Tour);

                //lets ensure that all final checks are completed
                this.finalTourChecks(Tour);

                //store the modified tour
                _hopscotchTourStore[Tour.id] = Tour;
            },

            /**
             * Get all hopscotch tours
             * @return {Tours{}}
             */
            getAllHopscotchTours: function() {
                return _hopscotchTourStore;
            },

            /**
             * Get a stored hopscotch tour
             * @param  {String} tourid The slug of the tour stored against the tour
             * @return {Tour{}}        [description]
             */
            getHopscotchTour: function(tourid) {
                return _hopscotchTourStore[tourid] ? _hopscotchTourStore[tourid] : {};
            },

            /**
             * This is a function which does its best to assist tours that happen across multiple pages
             * Some tours especially in welcome tours need continuous tracking and the hopscotch tour does not
             * accommodate for this
             *
             * The cookie which is set is forced against the base url
             * if this is removed there will be multiple instances of the cookie with the same name stored
             *
             * @param  {hopscotch{}} hopscotch the currently active hopscotch tour object
             * @param  {String} cookiename  set this if you would like a custom cookie name
             */
            tourTracker: function(hopscotch, cookiename) {

                //check if cookie set
                if (!cookiename) {
                    cookiename = _defaultCookieName;
                }

                //where are we in the current tour lifecycle
                var state = hopscotch.getState();

                //check if cookie exists
                var tourdata = this.getTourCookie() || {};

                //the tour next step
                var toursteps = tourdata.step + 1;

                //reset the tourdata object
                tourdata = {};

                //we include the following information to aid in the tour processes
                $.extend(tourdata, {
                    step: toursteps,
                    tour: hopscotch.getCurrTour().id,
                    referrer: document.referrer,
                    href: window.location.href,
                    servlet: Creole.servlet,
                    hopscotchState: state,
                    user: Creole.trueUser.name
                });

                //we set the cookie consistently against the tour
                //if the '/' is removed multiple copies will exist
                setCookie(cookiename, JSON.stringify(tourdata), '/');
            },

            /**
             * Get the tour data stored in cookies and return a tour object
             * If there are no tours then the object will be empty
             *
             * @param  {String} cookiename optional The preferred name to be used by the developer
             * @return {Tour{}}
             */
            getTourCookie: function(cookiename) {

                if (!cookiename) {
                    cookiename = this.getDefaultCookieName();
                }

                var tourCookie = getCookie(cookiename);

                return tourCookie && tourCookie !== '' ? JSON.parse(tourCookie) : {};
            },

            /**
             * Set the starting cookie
             * @param {Tour{}} Tour the tour to begin
             */
            setStartingCookie: function(Tour) {
                $.extend(Tour, {
                    start: true,
                    step: 0
                });
                setCookie(_defaultCookieName, JSON.stringify(Tour), '/');
            },

            /**
             * Load a hopscotch tour when the element is on the page
             * This method uses the setInterval method to repeatedly check for the existence of
             * and element when the page is loaded
             * It works well with the hopscotch tours as it uses the target property of the tour steps
             * to check for the element
             * The method checks for an element every 200ms
             *
             * Note: It is best to use this when the tour is being triggered over multiple pages
             * @param  {Tour{}} Tour The name of the hopscotch tour object within the file
             */
            loadHopscotchTour: function(Tour) {

                //if the user logs out and logs in as another user then this should not trigger a tour
                //lets get the tour cookie
                var currentTour = this.getTourCookie();

                //captures a case where the user has logged out and the tour was incomplete or the currentTour has no property name
                if (currentTour.user) {
                    if (Creole.trueUser.name !== currentTour.user) {
                        hopscotch.endTour();
                        return;
                    }
                }

                //get the number of the tour from the previous page
                var state = Math.floor(hopscotch.getState().split(':')[1]) + 1;

                //verify if it is teh last step in the tour
                var lastStep = false;

                //ensure that we are not at the end of the tour
                if (state >= Tour.steps.length) {
                    state--;
                    lastStep = true;
                }

                //container to target the elements on a page when they are added
                var targetElements;

                //a hopscotch tour can target more than one element if needed
                //those with more than one element will be an array, the other a string
                //we create an array to conform with the following code
                if (_.isArray(Tour.steps[state].target) === true) {
                    targetElements = Tour.steps[state].target;
                } else {
                    targetElements = [Tour.steps[state].target];
                }

                //set up the elements for checking
                for (var i = targetElements.length - 1; i >= 0; i--) {

                    //ids are not included as part of the tours
                    //we ignore classes
                    if (targetElements[i].charAt(0) !== '.') {
                        targetElements[i] = '#' + targetElements[i];
                    }
                }

                //sometimes things will continue for ever if no checks are made
                var timeCheck = false;

                //When this is completed we will assume that something is wrong
                //default time 10s
                setTimeout(function() {
                    timeCheck = true;
                }, 10 * 1000);

                //begin interval check
                var testForElement = setInterval(function() {

                    //set up jquery element
                    var $targetElements = $(targetElements.join(','));

                    //if the element exists then call the hopscotch tour
                    if ($targetElements.length > 0) {

                        //start the tour
                        hopscotch.startTour(Tour);

                        //check if it is the final step
                        //lets persist if this is the case
                        //for tours that are not part of sosmart but can be later
                        if (lastStep === true && typeof soSmartMediator.getSingleTutorial(Tour.id) !== 'undefined') {
                            soSmartMediator.persistCompletedTutorial(Tour.id);
                        }

                        //clear the interval
                        clearInterval(testForElement);
                    } else {
                        //what heppens if the time elapsed is too long
                        //we should check the status of a number of things and then display a message to the user
                        if (timeCheck === true) {
                            //lets begin the checks
                            //if the target is not present does this mean that their is a user setting preventing the tour?
                            // if (Creole.trueUser.name === 'admin' || (Creole.trueUser.name === Creole.user.name)) {
                            //     Notifications.post({
                            //         type: 'info',
                            //         duration: 10000,
                            //         text: 'As an admin there are parts of the tutorial which might not display correctly or work. Please ensure that you are a normal user and you have atleast one service such as twitter assigned to you.'
                            //     });
                            // } else {
                            //     Notifications.post({
                            //         type: 'info',
                            //         duration: 5000,
                            //         text: 'An error occurred whilst trying to begin/continue the tour. Please try again or contact an Account Manager.'
                            //     });
                            // }

                            Notifications.post({
                                type: 'error',
                                text: 'An error occurred whilst trying to begin/continue the tour. Please try again or contact an Account Manager.'
                            });

                            //if the tour has failed lets terminate it
                            hopscotch.endTour();

                            //     //if the user is an admin then tell them
                            //     if(Creole.trueUser.name === 'admin'){
                            //         Notifications.post({
                            //             type: 'info',
                            //             duration: 5000,
                            //             text: 'SoSmart Tutorials should be completed by non-admin users, please log out and then login in as a normal user'
                            //         });
                            //     } else if(){
                            //         Notifications.post({
                            //             type: 'info',
                            //             duration: 5000,
                            //             text: 'SoSmart Tutorials should be completed by non-admin users, please log out and then login in as a normal user'
                            //         });
                            //     }

                            //clear the interval
                            clearInterval(testForElement);

                        }
                    }
                }, 200);
            },

            /**
             * A function which retrieves all of the elements for multipages
             *
             * This means that rather than generating the string of elements manually
             * it will be generated dynamically based on the multipage property of a TourStep{}
             * @param {Tour{}} Tour A Tour object
             * @return {String} The elements to check for on multipage loads against hopscotch.getState()
             */
            getMultiPageStepsToCheck: function(Tour) {

                assert(Tour.steps, 'TourAssistant.getMultiPageStepsToCheck() : A tour must have steps');

                //string to check against when tours are loaded
                var crossPageCheckString = '';

                //retrieve tour steps
                var tourSteps = Tour.steps;

                //get all hopscotch multipage items
                var multipageElements = _.filter(tourSteps, function(Step, index, list) {

                    //we determine if this tour leads to another page
                    //we need additional information
                    Step['position'] = index;
                    Step['step'] = index + 1;

                    if (index > 0) {
                        //this in fact includes all multipage elements and the steps ahead
                        return typeof Step.multipage !== 'undefined' || (typeof list[index - 1].multipage !== 'undefined' && index !== 0);
                    } else {

                        return typeof Step.multipage !== 'undefined';
                    }

                });

                //get the tour slug
                var tourid = Tour.id;

                //generate the string of elements to be checked for cross page
                _.each(multipageElements, function(Step, index) {
                    crossPageCheckString += tourid + ':' + Step.position;
                });

                return crossPageCheckString;
            },

            /**
             * Sets up the Tour object stored within _hopscotchTourStore by attaching the tour steps
             *
             * Although the tour object is returned it should not matter
             * javascript operates by a pass by reference scheme for objects
             *
             * @param  {TourSteps{}} TourSteps the steps created within the tour file
             * @param {Tour{}} Tour The tour to store the steps against
             * @returns {Tour{}} The modified tour with the steps added
             */
            attachTourSteps: function(Tour, TourSteps) {
                var toursteps = this.tourStepsToArray(TourSteps);
                $.extend(Tour, {
                    steps: toursteps
                });
                return Tour;
            },

            /**
             * Converts the tour step object to an array of steps
             * @param  {[type]} TourSteps The object which contains the tour steps
             * @return {TourSteps[]}    An array of tour steps, returns empty
             */
            tourStepsToArray: function(TourSteps) {
                var steps = _.toArray(TourSteps);
                return steps;
            },

            /**
             * Checks if a hopscotch tour is currently active for crosspage tours
             * The tour object will reflect the modified state that is stored in _hopscotchTourStore
             * @param  {String} hopscotchstate The current active tour from hopsotch.getstate()
             * @param  {Tour{}} Tour           The tour
             */
            initTourIfActive: function(hopscotchstate, Tour) {
                if (hopscotchstate !== null && Tour.state.indexOf(hopscotchstate) > -1) {
                    this.loadHopscotchTour(Tour);
                }
            },

            /**
             * Set all the default elements of tours
             * @param  {Tour{}} Tour A tour object
             */
            initTourDefaults: function(Tour) {
                $.extend(Tour, {
                    //all tours should advance if the target elements are clicked
                    nextOnTargetClick: true,

                    //all tours should remove the cookie based on these methods
                    //This is a hack and should of worked in the prototypical inheritance implementation
                    //for the Tour objects, but didn't
                    //
                    //TODO try and integrate baseline methods using prototypical inheritance scheme
                    onClose: function() {
                        deleteCookie(TourAssistant.getInstance().getDefaultCookieName());
                    },
                    onError: function() {
                        deleteCookie(TourAssistant.getInstance().getDefaultCookieName());
                    },
                    onEnd: function() {
                        deleteCookie(TourAssistant.getInstance().getDefaultCookieName());
                    }
                });
            },

            /**
             * This ensures that all tours in SoSmart will be persisted if the final step of a tour
             * occurs on the same page as the previous step
             * Other final checks can be applied here if required
             * @param  {Tour{}} Tour
             */
            finalTourChecks: function(Tour) {
                //lets verify that the tour is a part of soSmart
                if (soSmartMediator.getTutorialsOrderBySlug()[Tour.id]) {

                    //get the last and second to last step
                    var lastStep = Tour.steps[Tour.steps.length - 1];
                    var penultimateStep = Tour.steps[Tour.steps.length - 2];

                    //check if the second to last step does not have a multipage element
                    if (!penultimateStep.multipage) {

                        //so the tour ends on the same page
                        //lets extend the last steps onShow method
                        //this is otherwise handled by loadHopscotchTour for multipage elements and committing completed tours
                        this.extendTourFunctions('onShow', function() {
                            soSmartMediator.persistCompletedTutorial(Tour.id);
                        }, lastStep);
                    }

                    //auto extend all steps that do not have the previous step on a separate page
                    //allows the user to navigate between steps
                    _.each(Tour.steps, function(Step, i, TourSteps) {
                        //the first tour is always the sosmart introduction
                        if (i > 0) {

                            //make sure the previous tour was not on another page
                            if (!TourSteps[i - 1].multipage) {

                                //if the showPrevButton is not saved against the tour step
                                //then we can take action
                                if (_.has(Step, "showPrevButton") === false) {

                                    //if this step has buttons enabled then leave else add the back button
                                    //add the previous button to the current step
                                    //if it doesn't alreay exist
                                    if (!Step.showPrevButton) {
                                        $.extend(Step, {
                                            showPrevButton: true
                                        });
                                    }
                                }
                            } else {

                                

                                //lets verify if the step does not already have a manually set delay
                                //This may be because the item shown is available but the tour executes too quickly
                                //and unfortunately skips to the next step
                                if (_.has(Step, "delay") === false) {

                                    //If the previous step has a multipage
                                    //This step should have a slight delay
                                    $.extend(Step, {
                                        delay: 500
                                    });
                                }
                            }
                        }
                    });

                    //the user should never be allowed to click back on the last step
                    //even if we have already set it
                    $.extend(lastStep, {
                        showPrevButton: false
                    });
                }
            },

            /**
             * Create an instance of a Tour
             * Must include a name, tours need to be uniquely identifiable
             * @param  {String} name The name of the tour
             * @return {Tour{}} A new tour object
             */
            createTour: function(name) {
                assert(_.isString(name) === true, 'TourAssistant:createTour() - A tour must have a name');
                var newTour = new Tour(name);
                _currentTour = newTour;
                return newTour;
            },

            /**
             * Get a tourstep object
             * @param {Object} optional an object of paramaters for the tour step
             * @return {TourStep{}} The default tourstep object
             */
            addTourStep: function(optional) {

                //create our tour step
                var tourStep = new TourStep();

                //only extend if the optional variable is an object
                if (_.isObject(optional) === true) {
                    $.extend(tourStep, optional);
                }

                //set the current tour step
                _currentTourStep = tourStep;

                //store the step against the current tour
                _currentTour.steps.push(tourStep);

                return tourStep;
            },
            /**
             * Retrieve the current tour during the processing of a Tour
             * @return {Tour{}}
             */
            getCurrentTour: function() {
                return _currentTour;
            },

            /**
             * retrieve the current tour step
             * @return {TourStep{}}
             */
            getCurrentTourstep: function() {
                return _currentTourStep;
            },
            /**
             * A way to add optional functions to a method when it is called
             * At the moment this only works for the onShow method
             *
             * @param {String} methodToExtend    The known method that you would like to add extra functionality
             * @param {function[] || function} functionReference A reference to a function or an array of references to functions
             * @param {Tour{} || TourStep{}} ObjectToExtend The tour step or tour object that needs to be extended
             */
            extendTourFunctions: function(methodToExtend, functionReference, ObjectToExtend) {

                if (!ObjectToExtend) {
                    ObjectToExtend = _currentTourStep;
                }

                var extendingMethod = functionReference;
                //lets check if the property exists then create it
                if (typeof ObjectToExtend.customFunctions[extendingMethod] === 'undefined') {
                    ObjectToExtend.customFunctions[methodToExtend] = [];
                }

                //lets check if we have received an array of functions or just the function
                if (_.isArray(extendingMethod) === true) {
                    //we need to merge the arraylist with the current if there are already items in it 
                    ObjectToExtend.customFunctions[methodToExtend] = ObjectToExtend.customFunctions[methodToExtend].concat(extendingMethod);
                } else {
                    ObjectToExtend.customFunctions[methodToExtend].push(extendingMethod);
                }
            }
        };
    };

    /***************************************************************
     * Tours
     ***************************************************************/
    /**
     * The default tour step object in which all tours inherit from
     */
    function TourBase() {

        //when a tour shows,closes or exits
        // var functionList = ['onClose', 'onError', 'onEnd'];

        // //generate the default functions that should be initialised against
        // //the Tour object
        // for (var i = functionList.length - 1; i >= 0; i--) {
        //     TourBase.prototype[functionList[i]] = this[functionList[i]] = function() {
        //         deleteCookie(TourAssistant.getInstance().getDefaultCookieName());
        //     };
        // }
    }

    TourBase.prototype.onClose = function() {
        deleteCookie(TourAssistant.getInstance().getDefaultCookieName());
    };

    // TourBase.prototype.onEnd = function() {
    //     deleteCookie(TourAssistant.getInstance().getDefaultCookieName());
    // };

    // TourBase.prototype.onError = function() {
    //     deleteCookie(TourAssistant.getInstance().getDefaultCookieName());
    // };

    var Tour = function(name) {
        this.id = name;
        this.steps = [];
        this.customFunctions = {};
    };

    Tour.prototype = new TourBase();
    Tour.prototype.constructor = Tour;
    Tour.prototype.parent = TourBase.prototype;

    //would be ideal to dynamically generate prototypes
    //based on an array of function names
    //for reference - http://javascript.crockford.com/inheritance.html
    Tour.prototype.onClose = function() {

        var thisTour = hopscotch.getCurrTour();

        //we get the name of the method being called e.g. onShow, onClose
        //if we could dynamically get the name of this function this could be automated
        thisTour.callCustomFunctions('onClose');

        //When all is completed we call the parent or base object method
        //this way we ensure that all tours are guaranteed to implement the actions
        //we want to happen across all tours
        //
        //It also ensures that we have more control over the tour process
        thisTour.parent['onClose'].call(this);
    };

    // Tour.prototype.onError = function() {

    //     //we get the name of the method being called e.g. onShow, onClose
    //     //if we could dynamically get the name of this function this could be automated
    //     // this.callCustomFunctions('onError');

    //     // //When all is completed we call the parent or base object method
    //     // //this way we ensure that all tours are guaranteed to implement the actions
    //     // //we want to happen across all tours
    //     // //
    //     // //It also ensures that we have more control over the tour process
    //     // this.parent['onError'].call(this);
    // };

    // Tour.prototype.onEnd = function() {

    //     //we get the name of the method being called e.g. onShow, onClose
    //     //if we could dynamically get the name of this function this could be automated
    //     // this.callCustomFunctions('onEnd');

    //     // //When all is completed we call the parent or base object method
    //     // //this way we ensure that all tours are guaranteed to implement the actions
    //     // //we want to happen across all tours
    //     // //
    //     // //It also ensures that we have more control over the tour process
    //     // this.parent['onEnd'].call(this);
    // };

    /***************************************************************
     * TourSteps
     ***************************************************************/

    /**
     * The default tour step object in which all tours inherit from
     */
    function TourStepBase() {

        //by default we place all tours below the item
        this.placement = 'bottom';

    }

    /**
     * Tour objects by default contain an onShow method
     * With all tours we want to log the tourstate for a better experience
     * This is mostly handled by the TourAssistant object
     *
     */
    TourStepBase.prototype.onShow = function() {
        TourAssistant.getInstance().tourTracker(hopscotch);
    };

    var TourStep = function() {
        //when a tour shows,closes or exits 
        this.customFunctions = {};
    };

    /**
     * We store the base tour against the step that will be generated
     */
    TourStep.prototype = new TourStepBase();

    /**
     * Avoid referencing the base tour when constructed
     */
    TourStep.prototype.constructor = TourStep;

    /**
     * Maintain a reference to the parent object in which the step inherits from
     */
    TourStep.prototype.parent = TourStepBase.prototype;

    /**
     * When we create a new tour step we want to know if the developer has created
     * a custom action to execute against the tour
     * To do this we ask that the developer creates a function and stores this against a
     * customfunctions property within the tour step object
     *
     * This also means that the developer may want to create other custom functions against other methods
     * within hopscotch tours
     *
     * In this case we recommend storing the method calls within an array stored against the name of the method within the customfunctions
     * property
     *
     */
    TourStep.prototype.onShow = function() {

        //We need to get the current tourstep
        //this will be provided by hopscotch
        var thisStep = hopscotch.getCurrTour().steps[hopscotch.getCurrStepNum()];

        //we get the name of the method being called e.g. onShow, onClose
        //if we could dynamically get the name of this function this could be automated
        thisStep.callCustomFunctions('onShow');

        //When all is completed we call the parent or base object method
        //this way we ensure that all tours are guaranteed to implement the actions
        //we want to happen across all tours
        //
        //It also ensures that we have more control over the tour process
        thisStep.parent['onShow'].call(this);
    };

    /**
     * Developers who have created custom functions can append them to the functionlist
     * When a hopscotch action is taken
     *
     * This means that developers can create multiple functions and then have them called when the tour is created
     * against the tour step. This in essence accommodates the inheritance structure of the code
     * @param  {String} functionName The function to call when methods are stored against it
     */
    TourStep.prototype.callCustomFunctions = function(functionName) {

        //need to get the currently active tour step
        //using this will at times refer to the window object and not the Tourstep
        var thisStep = hopscotch.getCurrTour().steps[hopscotch.getCurrStepNum()];

        if (typeof thisStep !== 'undefined') {
            //lets get the name of all functions
            var functionList = _.functions(this);

            //lets verify that the function is included in the object
            if (functionList.indexOf(functionName) > -1) {

                //and ensure we have populated the customFunction object
                if (typeof this.customFunctions !== 'undefined') {

                    //determine if the function is in customFunctions List and that there are actions to take
                    if (this.customFunctions[functionName] && this.customFunctions[functionName].length > 0) {

                        for (var i = 0; i < this.customFunctions[functionName].length; i++) {
                            //call the method stored in the customFunctionObject
                            this.customFunctions[functionName][i]();
                        }
                    }
                }
            }
        }
    };

    Tour.prototype.callCustomFunctions = function(functionName) {
        //using this will at times refer to the window object and not the Tour
        var thisTour = hopscotch.getCurrTour();

        if (typeof thisTour !== 'undefined') {
            //lets get the name of all functions
            var functionList = _.functions(this);

            //lets verify that the function is included in the object
            if (functionList.indexOf(functionName) > -1) {

                //and ensure we have populated the customFunction object
                if (typeof this.customFunctions !== 'undefined') {

                    //determine if the function is in customFunctions List and that there are actions to take
                    if (this.customFunctions[functionName] && this.customFunctions[functionName].length > 0) {

                        for (var i = 0; i < this.customFunctions[functionName].length; i++) {
                            //call the method stored in the customFunctionObject
                            this.customFunctions[functionName][i]();
                        }
                    }
                }
            }
        }
    };

    return {
        getInstance: function() {
            if (!instance) {
                instance = init();
            }
            return instance;
        }
    };
})();
