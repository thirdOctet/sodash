/**
 * An object which aids in integrating the gridster widget into the dashboard
 * This object is responsible for
 *  - TODO: describe functionality
 */
var GridsterWrapper = (function() {
    var GridsterWrapper = function() {
        //begin gridster
        //set the number of columns by which the grid should span
        var MAX_COLS = 12;

        var margin = 10;

        var widgetWidth;

        var gridSize = 12;

        var gridsterContainer;

        var gridsterInstance;

        var instance;

        var widgetHeight = 30;

        var configuration = {};

        var functionalityStatus = false;

        /**
         * TODO
         */
        this.prepareGridsterContainer = function() {            
            gridsterContainer = $('.gridster .gridster-container');
            return this;
        };

        /**
         * Set the width of the widgets
         * @param {[type]} containerWidth [description]
         */
        this.setWidgetWidth = function(containerWidth) {
            if(!containerWidth) containerWidth = $('#mainbody').width(); 
            widgetWidth = Math.floor((containerWidth / MAX_COLS) - margin * 2);
            return this;
        };

        /**
         * Set teh width of the gridster container
         * @param {[type]} width [description]
         */
        this.setGridsterWidth = function(width){
            if(width){
                gridsterContainer.width(width);
            } else{
                gridsterContainer.width($('#mainbody').width());
            }
            return this;
        };

        /**
         * Returns the attached to the gridster instance
         * @return {Dom Element}
         */
        this.getGridsterElement = function(){
            return gridsterInstance.$el;
        };

        /**
         * TODO
         * @return {[type]} [description]
         */
        this.getGridsterInstance = function(){
            return gridsterInstance;
        };



        /**
         * Get the current status of the functionality
         * @return {Boolean}
         */
        this.getFunctionalityStatus = function() {
            return functionalityStatus;
        };

        

        /**
         * The default gridster configuration
         * @type {Object}
         */
        var defaultGridsterOptions = function() {
            return {
                widget_margins: [margin, margin],
                max_cols: MAX_COLS,
                widget_base_dimensions: [widgetWidth, widgetHeight],
                resize: {
                    start: function(e, ui, widget) {},
                    stop: function(e, ui, $widget) {                       

                            //trigger an event that resizes the height of the container
                            $(document).trigger("Gridster:Resized", $widget);
                    }
                },
                max_size_x: MAX_COLS
            };
        };

        /**
         * Get the gridster container
         * @return {jQuery Object}
         */
        this.getGridsterContainer = function() {
            return gridsterContainer;
        };

        /**
         * force grid to expand to screen size
         * @param  {Number} width The width to set gridster parent to
         */
        this.resetGridsterWidth = function(width) {
            gridsterContainer.width(width);
        };


        /**
         * Get the default margins for the gridster container
         * @return {Number} The default margin
         */
        this.getMargin = function() {
            return margin;
        };

        /**
         * set the margins for gridster
         * @param {Number} margin [description]
         */
        this.setMargin = function(marginSet) {
            margin = marginSet;
        };

        /**
         * Set the maximum number of columns
         * @param {Number} columnNumber The number of columns. If not set defaults to 12
         */
        this.setMaxColumns = function(columns) {
            MAX_COLS = columns;
        };

        /**
         * Get the maximum number columns of gridster instance
         * @return {Number} [description]
         */
        this.getMaxColumns = function() {
            return MAX_COLS;
        };

        /**
         * Set the height of the rows of the grid
         * @param {Number} height [description]
         */
        this.setRowHeight = function(height) {
            widgetHeight = height;
        };

        this.getRowHeight = function() {
            return widgetHeight;
        };

        this.getWidth = function() {
            return widgetWidth;
        };

        /**
         * Recursively extend the gridster options
         * @param  {Object} options The additional options to be provided when gridster is created
         */
        this.extendGridsterOptions = function(options) {
            $.extend(true, configuration, options);
            return this;
        };

        /**
         * Get the default options of the gridster widget
         * @return {Object}
         */
        this.getDefaultOptions = function() {
            return defaultGridsterOptions();
        };

        /**
         * Create a gridster instance based on the configured options
         * The default is to disable the dragging of the library and only enable for admins
         * @param {Boolean} abilityToEdit
         * @return {GridsterWrapper} [description]
         */
        this.createInstance = function(abilityToEdit) {

            $.extend(true, configuration, defaultGridsterOptions());

            this.extendGridsterOptions({
                resize: {
                    enabled: abilityToEdit
                }
            });

            gridsterInstance = abilityToEdit && abilityToEdit === true ? gridsterContainer.gridster(configuration).data('gridster') : gridsterContainer.gridster(configuration).data('gridster').disable();

            return this;
        };

        /**
         * Get the current gridster configuration
         * @return {Object{}} Gridster configuration
         */
        this.getGridsterConfiguration = function() {
            return configuration;
        };

    };



    return GridsterWrapper;
})();
