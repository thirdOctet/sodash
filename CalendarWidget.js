/**
 * The Calendar Widget is based on fullCalendar - http://fullcalendar.io/
 * @author Jonathan Hussey <jonathan@winterwell.com>
 *
 * @requires FullCalendar 
 * @requires lodash
 * @requires underscore
 * @requires moment
 * @requires jquery
 * @requires templates
 * @requires  ProfileManager
 *
 * Data sources ----------------------------------------------------------------------------------------------------
 * The calendar widget is populated from two main sources
 * - Firstly it will retrieve messages from outbox.js' getdata method which is built upon DataLoaders' .get method.
 *     This can be any object which provides a deferred     
 * - Secondly it is listening for events raised by MessageStream when new messages are retrieved
 *     This is tied to MessageStream.processIncomingMessages and a new event will be raised for each message
 * -----------------------------------------------------------------------------------------------------------------
 */
var CalendarWidget = function() {

    /**
     * Reference to self due to external libraries
     * @type {CalendarWidget}
     */
    var _this = this;

    /**
     * The ui elements for the calendar widget
     */
    var _calendarInstance, _calendarMessageEditor;

    /**********************************************************************************
     * Calendar Plugin properties
     **********************************************************************************/

    /**
     * The default calendar configuration for which all calendar widgets are based
     * @type {Object}
     */
    var _defaultConfiguration = {
        header: {
            left: 'prev,next',
            center: 'title',
            right: 'today' // TODO weekly view 'month,agendaWeek'
        }
    };

    /**
     * The configuration for the calendar instance
     * @type {Object}
     */
    var _configuration = {
        //wire up the click event against the calendar columns
        dayClick: function(date, event, view) {
            $(document).trigger('Calendar:dayClick', [date, view, event]);
        },
        eventClick: function(date, event, view) {
            $(document).trigger('Calendar:eventClick', [date, view, event]);
        },

        //append the calendar to the date based on the calendar contents
        eventRender: function(event, element) {
            $(element).append([event.contents]);
        },

        eventLimitClick: function(event, event2, event3) {
            $(document).trigger('Calendar:eventClick', [event.date]);
        },
        eventLimit: 3
    };

    /**********************************************************************************
     * Data Management properties
     **********************************************************************************/

    /**
     * A reference to an object that creates deferreds in which this calendar
     * will focus data requests when populating the calendar
     */
    var _deferredProvider;

    /**
     * Maximum events to render per go on incoming data within the calendar
     * @type {Number}
     */
    var _maxEventsRender = 5;

    /**
     * Allows processing on incoming data but stores events on month basis 
     * keeping track of oldest requested date for month  e.g. 
     * "April2015" -> { 
     *     startDate : Date, 
     *     endDate : Date, 
     *     oldest : Date,
     *     requests : {
     *         unixtimestamp : deferred //i.e 1435837591 : deferred
     *     }
     * }
     *
     * This is normally generated in 'getCalendarDataforMonth'
     * @type {Object}
     */
    var _dataMonthNavigator = {};

    /**
     * An object which will enable the _dataMonthNavigator to
     * control data requests
     * @type {Navigator}
     */
    var Navigator = function(){
        this.startDate = undefined;
        this.endDate = undefined;
        this.oldest = undefined;
        this.requests = {};
    };

    /**
     * The amount of time to wait before making further requests
     * @type {Number}
     */
    var _intervalBetweenDataRequests = 100;


    /**********************************************************************************
     * Setters
     **********************************************************************************/


    /**
     * Is set within createCalendar when a new calendar is created
     * @param {[type]} calendarElement The FullCalendar 
     */
    var _setCalendarInstance = function(calendarElement) {
        _calendarInstance = calendarElement;
    };

    /**
     * Is set after the calendar is initialised
     * @param {[type]} calendarEditor [description]
     */
    var _setCalendarEditor = function(calendarEditor) {
        _calendarMessageEditor = calendarEditor;
    };

    /**
     * Should be initialised as early as possible against an object
     * in which the calendar displays data
     * @param {Object} deferredProvider any object that has a getData method
     */
    this.setDeferredProvider = function(deferredProvider) {
        _deferredProvider = deferredProvider;
    };

    /**
     * Updates the Message editor date for retrieving messages for rendering
     * @param {String} date    [description]
     */
    this.setEditorDate = function(date) {
        _calendarMessageEditor.attr('data-date', date);
        this.editorDate = new Date(date);
    };

    /**********************************************************************************
     * Getters
     **********************************************************************************/

    /**
     * Retrieve the calendar message editor
     * @return {jQuery} The jquery object for the calendar message instance
     */
    this.getCalendarEditor = function() {
        return _calendarMessageEditor;
    };

    /**
     * Retrieve the calendar instance 
     * @return {[type]} The jquery object for the calendar instance
     */
    this.getCalendarInstance = function() {
        return _calendarInstance;
    };

    /**
     * Retrieve the preset object for the calendar instance for initialisation
     * @return {} 
     */
    this.getDefaultConfiguration = function() {
        return _defaultConfiguration;
    };

    /**
     * Retrieve the latest configuration  
     * @return {}
     */
    this.getConfiguration = function() {
        return _configuration;
    };

    /**********************************************************************************
     * Calendar UI Preparation and Creation
     **********************************************************************************/

    /**
     * Uses the default calendar configuration options and the passed in parameters to extend a
     * configuration object unique to this instance of the calendar 
     * @param  {Object} parameters The object containing the configuration for the calendar
     * @return {Object}            The latest configuration object for the calendar
     */
    this.extendConfiguration = function(parameters) {
        _configuration = _.extend(_configuration, _defaultConfiguration);
        _configuration = _.extend(_configuration, parameters);
        return _configuration;
    };

    /**
     * Creates a calendar instance    
     * @param  {Object} parameters You can add custom parameters to the calendar instance based on the fullcalendar init instructions
     * @param  {String} classname a custom class name in the case where 
     * @return {[type]}            [description]
     */
    this.createCalendar = function(parameters, classname, targetid) {

        var params = parameters || {};

        //jquery reference to calendar container
        //this updated to fullcalendar instance
         var $calendarTemplate = $(templates.CalendarWidget_Wrapper());

        //prepare the calendar
        //makes assumptions about the location of the calendar        
        targetid ? $(targetid).append($calendarTemplate) : $('#mainbody').append($calendarTemplate);

        _this.extendConfiguration(params);

        //save the element and initialise the calendar
        _setCalendarInstance($calendarTemplate.fullCalendar(_configuration));

        //sets the calendar widgets editor
        _setCalendarEditor($(templates.CalendarWidget_MessageList()));

        //editor for selected days to display editable messages within a sidebar
        _calendarInstance.append(_calendarMessageEditor);

        
    };

    /**
     * Set up the calendar event object
     * @param  {Number} timeStamp The unix timestamp
     * @return {Object}           The calendar event object
     */
    this.getCalendarEvent = function(timeStamp) {

        //generate date object
        var date = new Date(timeStamp);
        //
        var fullDate = this.getDatetime(date);

        //matches the calendar implementation for each
        //day of a month
        var formattedDate = this.getCalendarFormattedDate(date);

        var time = this.getTime(date);
        var friendlyTime = this.getUserFriendlyTime(date);

        var calendarEvent = {
            //plugin specific
            start: fullDate,
            end: fullDate,

            //custom
            startTime: fullDate,
            time: time,
            date: formattedDate,
            friendlyTime: friendlyTime

        };

        return calendarEvent;
    };

    /**********************************************************************************
     * Calendar UI updates
     **********************************************************************************/

    /**
     * when a calendar is initialised the first set of data should be called against this
     * function.
     * @param   {iText[]} initialMessages A list of messages
     * @param   {Boolean} removeLoader Custom remove the loader on completion or manually remove
     */
    this.addMessagesToCalendar = function(messages, removeLoader) {

        // _this.addLoader();

        //Render the new data but in chunks
        _this.staggerCalendarRendering(messages);

        removeLoader && removeLoader === true ? '' : _this.removeLoader();
    };

    /**********************************************************************************
     * Calendar Events
     **********************************************************************************/

    /**
     * Prepare from scheduled messages a list of calendar Events
     * @param  {Object[]} messages [description]
     * @return {Object[]}          [description]
     */
    this.prepareCalendarEvents = function(messages) {

        //store the events
        var calendarEvents = [];

        //build the calendar event object based on messages
        _.each(messages, function(Message, i, smList) {
            calendarEvents.push(_this.prepareCalendarEvent(Message));
        });

        return calendarEvents;
    };

    /**
     * Prepares a message based on DBText for rendering within
     * the calendar plugin
     * @param  {[type]} Message [description]
     * @return {[type]}         [description]
     */
    this.prepareCalendarEvent = function(Message) {

        assert(Message.pubDate !== undefined, 'CalendarWidget.js - prepareCalendarEvent: Message does not have a pubDate');

        //get the social icons for template rendering
        var socialIcons = templates.fn.StreamRow_GetReplyAsIconMap();

        var pubDate = Message.pubDate;

        //produces a calendar event object for the calendar
        var calendarEventConfig = _this.getCalendarEvent(pubDate);

        var serviceIcon = socialIcons[Message.service];

        //the template expects the time, Message object and service icon for rendering within the calendar
        var renderedTemplate = _this.renderMessagePreviewTemplate({
            time: calendarEventConfig.friendlyTime,
            message: Message,
            service: serviceIcon
        });

        //convert to jquery object for ease of referencing
        renderedTemplate = $(renderedTemplate);

        //At this point we set up the object to be rendered by the calendar plugin with additional
        //properties
        $.extend(true, calendarEventConfig, {
            contents: renderedTemplate,
            message: Message,
            service: serviceIcon
        });

        return calendarEventConfig;
    };

    /**
     * Retrieve the list of calendar events tied to the
     * calendar plugin
     * @return {Event[]} The list of events
     */
    this.getCalendarEvents = function() {
        return _calendarInstance.fullCalendar('clientEvents');
    };

    /**
     * Checks if an event exists within the calendar
     * Uses the xid of the message to compare the messages
     * @param  {Message{}} Message [description]
     * @return {Event{} || undefined}         [description]
     */
    this.findEvent = function(Message) {
        //get all current calendar events
        var calendarEvents = _this.getCalendarEvents();

        var foundExisiting = _.find(calendarEvents, function(theEvent, key, list) {
            return Message.xid === theEvent.message.xid;
        });
        return foundExisiting;
    };

    /**
     * Retrieves a list of calendar events based on date provided
     * @param  {String} date The calendar date in format "2015-04-17"
     * @return {Event[]}      The list of filtered calendar events
     */
    this.getCalendarEventsByDate = function(date) {
        assert(date, 'CalendarWidget.js - getCalendarEventsByDate: No date. Must provide a date formatted as 2015-04-17');
        var activeEvents = _this.getCalendarEvents();
        var groupedEvents = _this.groupByDate(activeEvents);
        return groupedEvents[date];
    };

    /**********************************************************************************
     * Calendar UI template rendering
     **********************************************************************************/

    /**
     * Render the message template for the calendar
     * @param  {Object} properties expects time, message and service keys of the object
     * @return {String}            The rendered template
     */
    this.renderMessagePreviewTemplate = function(properties) {
        assert(properties !== undefined, 'CalendarWidget.js - renderMessagePreviewTemplate: properties is undefined');
        return templates.CalendarWidget_MessagePreview(properties);
    };


    /**********************************************************************************
     * Calendar UI interactivity
     **********************************************************************************/

    /**
     * Toggle the clicked calendar day/event
     * This function is dependent on a plugin type as 
     * @param  {String} date The calendar date formatted as '2015-04-17'
     */
    this.highlightClicked = function(date) {
        var editorDate = _calendarMessageEditor.attr('data-date');
        _calendarInstance.find('.fc-day[data-date="' + editorDate + '"]').removeClass('active');
        _calendarInstance.find('.fc-day[data-date="' + date + '"]').addClass('active');
    };

    /**
     * Calendar date object from the FullCalendarPlugin retrieved when event raised from
     * clicking calendar cell. May be a date object or moment object
     * @param  {Object} date The fullcalendar date object or Moment object from clicking the claendar cell
     * @return {String}      The dtae string formatted in FullCalendar format
     */
    this.getSelectedDate = function(date) {
        return _.isString(date.date) === true ? date.date : date.format();
    };

    /**
     * Removes the loading indicator from the plugin after data is retrieved
     */
    this.removeLoader = function() {
        _calendarInstance.find('.calendar-loading').fadeOut(function() {
            $(this).remove();
        });
    };

    /**
     * Adds the loading indicator whilst data is being retrieved
     */
    this.addLoader = function() {
        var $loader = $(templates.CalendarWidget_CustomLoading());
        $loader.hide();
        if (_calendarInstance.find('.calendar-loading').length === 0) {
            _calendarInstance.find('.fc-center').prepend($loader);
            $loader.fadeIn();
        }
    };

    /**
     * Update the components of the calendar that 
     * have changed or been added
     */
    this.refreshAjaxed = function() {
        //rerender any twitter counters
        activateCounters();

        //update using datefield widget
        ajaxifyDateFields();

        //update textareas with content clipping
        ajaxifyCollapsible();
    };

    /**********************************************************************************
     * Calendar UI MessageEditor Area
     **********************************************************************************/

    /**
     * Renders the list of messages for a selected day
     * within the message editor/viewer
     * @param  {Event[]} listOfEvents An ordered llist of Events
     */
    this.populateMessageEditor = function(listOfEvents) {
        var $messagesArea = _calendarMessageEditor.find('.messages');
        $messagesArea.empty();
        _.each(listOfEvents, function(Event, key, list) {
            //get the message object against the calendar event
            var message = Event.message;

            //create the template from exising components
            message.element = $(templates.CalendarWidget_Message(Event));

            //set the event data against the message
            message.element.data('item', Event);

            //add to the message area
            $messagesArea.append(message.element);

            //handle attachments
            if (message.attachments && message.attachments.length > 0) {
                $(message.element).find('.add-attachments').sodashuploader({
                    file: message.attachments[0]
                });
            } else {
                $(message.element).find('.add-attachments').sodashuploader();
            }

            $(message.element).find('.add-attachments').on('uploader:updated', function(event, uploadedFiles) {
                this.refreshTextCounter(message, uploadedFiles.length);
            }.bind(this));

        });



        if (_calendarMessageEditor.is(':hidden')) {
            _calendarMessageEditor.show();
        }
    };

    /**
     * Removes the no messages element from the message editor
     */
    this.removeNoMessages = function() {
        _calendarMessageEditor.find('.no-messages').remove();
    };

    /**
     * Adds the no messages element to the message editor
     */
    this.addNoMessages = function() {
        if (_calendarMessageEditor.find('.scheduled-message,.published-message').length === 0) {
            if (_calendarMessageEditor.find('.no-messages').length === 0) {
                _calendarMessageEditor.find('.messages').append(templates.CalendarWidget_NoMessages());
            }

        }
    };

    /**
     * Retrieve the start and end date for the active month 
     * @return {Object} An on object of date objects typically startDate, endDate
     */
    this.getCalendarDateOnScroll = function() {
        //Trigger polling on selected month
        //a moment object
        var date = _calendarInstance.fullCalendar('getDate');

        var month = date.month();
        var year = date.year();

        //allows us to then get next time period but changes the
        //moment object to the next month
        date.add(1, 'months');

        //date is always going to be current date plus 1
        //to get all month
        var nextMonth = date.month();
        var nextYear = date.year();

        var startDate = new Date(year, month, 1); //this month
        var endDate = new Date(nextYear, nextMonth, 1); //next month

        return {
            startDate: startDate,
            endDate: endDate
        };
    };

    /**
     * Allows a calendar to render but in chunks, may want more chunks depending on the data size
     * Default delay time is 250ms
     * @param  {iText[]} theData A list of Sodash messages
     */
    this.staggerCalendarRendering = function(theData) {

        //a chunk actually refers to how many per array is required
        //split data into multiple segments
        var dataSplit = _.chunk(theData, _maxEventsRender);

        //allows us to render the calendar display without affecting performance too much
        _.each(dataSplit, function(data, key, list) {

            //we have lots of data to render so lets stagger it so that the UI can render happily
            _.delay(function(data) {
                _this.updateCalendarOnDataRequest(data);
            }, 250, data);
        });
    };

    /**
     * Retrieves all messages 
     * @param  {[type]} latestMessage [description]
     * @return {[type]}               [description]
     */
    this.getOldestMessage = function() {
        return _dataMonthNavigator;
    };

    /**
     * Searches for an event and uploads the calendar with
     * the changes
     * @param  {iText[]} data The retrieved messages
     */
    this.updateCalendarOnDataRequest = function(data) {

        //sort by newest
        _this.sortByDate(data);

        //this handles the bulk update of the 
        //calendar if necessary
        //It is best to do the rendering this way
        //The calendar does not perform well on hundreds of updates
        var notInCalendar = [];

        //used to assist in rendering and extending existing events
        var eventObject, foundExisiting;

        //begin processing incoming messages
        _.each(data, function(Message, key, list) {
            //only process the data that has changed
            foundExisiting = _this.findEvent(Message);

            eventObject = _this.prepareCalendarEvent(Message);

            //if the event does not exist in the calendar
            if (foundExisiting === undefined) {
                //prepare the object for the calendar
                notInCalendar.push(eventObject);
            } else {

                //we have to account for data that is already requested
                //there is no filtering mechanism
                //the data request merely retrieves all the latest messages
                //from the backend

                //the only thing that is trully unique is the contents
                if (_.isEqual(foundExisiting.message, Message)) {
                    return;
                }

                foundExisiting = _.extend(foundExisiting, eventObject);

                _calendarInstance.fullCalendar('updateEvent', foundExisiting);

            }
        });

        //only update the calendar if new events were found
        if (notInCalendar.length > 0) {
            //this will allow us to render the calendar without causing the browser 
            //to crash due to memory errors
            _calendarInstance.fullCalendar('addEventSource', notInCalendar);
        }
    }; // ./updateCalendarOnDataRequest()

    /**
     * Updates the UI with 
     * @param {[type]} event [description]
     */
    this.addScheduledMessage = function(event) {

        //open the message creator
        if ($('.create-message').length > 0) return;

        // The date for the selected cell to pass into the form and hence the Scheduler widget
        var sdate = _calendarMessageEditor.attr('data-date');
        var thisDay = null;
        if (sdate) {
            var d = new Date(sdate);
            d.setHours(9); // Default to 9am. How should we handle time-of-day??
            thisDay = d.getTime();
        }
        var form = templates.CalendarWidget_CreateMessage({
            thefn: '$(document).trigger(\'CalendarWidget:DeleteNewMessage\',this);',
            pubDate: thisDay
        });
        var $msgs = _calendarMessageEditor.find('.messages');
        assert($msgs.length === 1, "CalendarWidget.js - addScheduledMessage: .messages is not in container to render to");
        $msgs.prepend(form);

        $msgs.find('.add-attachments').sodashuploader();

        _this.refreshAjaxed();
    };

    /**
     * TODO refactor -- reply widget code like this belongs elsewhere.
     * Updates the reply widget with the new selected user.
     * @param  {Event} event The event object in which this function depends based on user click
     */
    this.selectPuppet = function(event) {

        event.preventDefault();

        //the setup
        var $this = $(this),
            $autoSelect = $this.closest('.auto-select'),
            $input = $autoSelect.find('.input-group input'),
            $serviceIcon = $autoSelect.find('.input-group .service');

        //we might need to update the service icon based on the users selection
        var icons = templates.fn.StreamRow_GetReplyAsIconMap();

        var $form = $this.closest('form');

        //lets get the value and start changing things
        var xid = $this.attr('data-xid');

        var puppet = ProfileManager.get().getProfileByXid(xid);

        //desired service will be extracted from the selected xid
        var service = XId.service(xid);

        $form.find('input[name="service"]').val(service);
        var $textarea = $form.find('textarea');

        _.each(icons, function(iconName, service, list) {
            $serviceIcon.removeClass('icomoon-' + iconName);
        });

        //set the service icon to the new puppets unless is not a service
        service ? $serviceIcon.addClass('icomoon-' + icons[service]) : $serviceIcon.addClass('icomoon-bullhorn');

        //update the service and the text counter attached to the textarea
        $textarea.attr('data-service', service);
        $textarea.textCounter('update', {
            service: service
        });

        //set the xid of the input element
        $input.attr('data-xid', puppet.xid);

        //set the text
        $input.val($.trim(puppet.name));
    };

    /**
     * Allows data to be fetched for the calendar against a 
     * deferred creator/provider
     *
     * 
     * data requests execute in the following way
     * 1st Req : Jun 1st -> Jul 1st  - return latest n messages -> oldest message is Jun 25th
     * 2nd Req : Jun 1st -> Jun 25th - return latest n messages -> oldest message is Jun 19th
     * 3rd Req : Jun 1st -> Jun 19th - e.t.c
     *
     */
    this.getCalendarDataforMonth = function() { 

        _this.addLoader();  

        //Trigger polling on selected month
        //retrieves an object containing start and end date objects
        var dates = _this.getCalendarDateOnScroll();

        //we store month year-> oldest date format
        var currentMonth = dates.startDate.format('Myy');

        //continue to retrieve the data until all is retrieved or 
        //the end date is less than the earliest request date for the month
        //makes several small data requests to the server every x milliseconds
        var dataRetrieval = setInterval(function() {

            //Create new structure for the month when data is being requested
            if(_.isUndefined(_dataMonthNavigator[currentMonth])) _dataMonthNavigator[currentMonth] = new Navigator();

            var dataMonth = _dataMonthNavigator[currentMonth];

            //create new date object for requests until time complete
            var timeTo;

            //uses the oldest message pubdate if set otherwise sets it to the calendar date
            if(dataMonth && dataMonth.oldest){
                timeTo = new Date(dataMonth.oldest.pubDate);
            } else {
                 timeTo = dates.endDate; 
            }

            //get the unix timestamp of the last message
            var lastTimeStamp = timeTo.valueOf();

            //set up the value of the timestamp to be undefined before being set to a deferred
            if(_.isUndefined(dataMonth.requests[lastTimeStamp])){
                dataMonth.requests[lastTimeStamp] = undefined;
            }

            //we have already placed a data request for this time period
            //no more server requests until our $.when section updates the oldest time
            if(!_.isUndefined(dataMonth.requests[lastTimeStamp])){
                return;
            } 

            //get the data
            var messages = _deferredProvider.getData(dates.startDate, timeTo);

            //store a reference to the original deferred request
            dataMonth.requests[lastTimeStamp] = messages;            

            // //set up the data attached to the month and year
            $.extend(true, dataMonth, {                
                startDate: dates.startDate,
                endDate: timeTo
            });           

            //entry point into staggering
            //This will continue to get all messages for the month
            $.when(messages).done(function(theData) {

                //fast fail if there is no data returned from the request
                if(theData.length === 0){
                    _this.removeLoader();
                    clearInterval(dataRetrieval);
                    return;
                }

                //ensure date is in descending order
                _this.sortByDate(theData);

                //Add the data which is further split and added every 250ms
                //the true parameter allows us to remove the loader when we want
                _this.addMessagesToCalendar(theData,true);

                //for the current month set the oldest message of the current request
                $.extend(true, dataMonth, {                    
                    oldest: theData[theData.length -1]
                });                

                //once the data returned is less than the max allowed results then stop request                
                if(theData.length < _deferredProvider.getNumberOfResults()){
                    _this.removeLoader();
                    clearInterval(dataRetrieval);
                }                
            });

        }, _intervalBetweenDataRequests);
        
    }; // ./getCalendarDataforMonth()

    /**
     * Enable or disable the calendar
     */
    this.toggleCalendar = function(event) {

        event.preventDefault();

        //get the current setting
        var currentState = Creole.settings.calendarEnabled;



        //update the backend
        DataLoader.post('/settings-userSettings.json', {
            action: 'set-settings',
            calendar: !currentState
        }).done(function(data) {
            //we allow page reloads rather than the default no action            
            Creole.settings.calendarEnabled = data.calendarEnabled;
            // window.location.href = window.location.href;
            $(location).attr('href',window.location.href);
        }).fail(function(error) {
            console.log(error);
        });
    };

	/*
	 * A new calendar widget has been made -- wire up some on-event handling
	 */
    /**
     * When a message is deleted the calendar is updated
     */
    $(document).on('MessageStream:MessageDeleted', function(event, data) {

        _this.addLoader();
        //we have to handle the removal of the event for the calendar
        //remove the element from
        var $source = $(data.element);

        var $scheduledMessage = $source.closest('.data');

        var eventData = $scheduledMessage.data('item');

        var calendarEventsActive = _this.getCalendarEvents();

        //update the event data
        _.find(calendarEventsActive, function(Event, index, list) {
            if (eventData !== undefined && eventData.message.xid === Event.message.xid) {

                //update the calendar with the updated event data
                _calendarInstance.fullCalendar('removeEvents', Event._id);

                //rerender the element
                $scheduledMessage.remove();

                _this.addNoMessages();

                //escape out of loop
                return true;
            }
        });

        _this.removeLoader();

        _this.refreshAjaxed();
    });

    /**
     * Event raised by MessageStream.doSaveDraft upon changes
     * element - the original 
     */
    $(document).on('MessageStream:DraftSaved', function(event, data) {

        _this.addLoader();

        var $source = $(data.element);
        var $scheduledMessage = $source.closest('.scheduled-message');
        var eventData = $scheduledMessage.data('item');
        var calendarEventsActive = _calendarInstance.fullCalendar('clientEvents');

        //create a calendar event object from the new data and replace
        var eventObject = _this.prepareCalendarEvent(data.message);
        var calDate = _calendarMessageEditor.attr('data-date');

        //get events for month
        var listOfEvents;

        //Is this is a new message being created
        if ($('.create-message').length > 0) {
            //remove the message and add it to the calendar
            $('.create-message').fadeOut(function() {
                $(this).remove();
            });

            _calendarInstance.fullCalendar('renderEvent', eventObject, true);

            listOfEvents = _this.getCalendarEventsByDate(calDate);

            _this.populateMessageEditor(listOfEvents);

            _this.refreshAjaxed();

            //remove the 'no message' message
            _this.removeNoMessages();

            _this.removeLoader();

            return;

        }

        //update the event data
        _.find(calendarEventsActive, function(Event, index, list) {
            if (eventData !== undefined && eventData.message.xid === Event.message.xid) {

                //extend the calendar object event data
                Event = _.extend(Event, eventObject);

                //update the calendar with the updated event data
                _calendarInstance.fullCalendar('updateEvent', Event);

                //rerender the element
                // var rendered = $(templates.CalendarWidget_Message(Event));
                listOfEvents = _this.getCalendarEventsByDate(calDate);

                //rerender the calendar editor area
                _this.populateMessageEditor(listOfEvents);

                //escape out of loop
                return true;
            }
        });

        _this.addNoMessages();

        _this.removeLoader();

        _this.refreshAjaxed();

    }); // ./ on(DraftSaved)

    /**
     * Event raised by MessageStream.doSend, Header.header_initUpdates
     * Allows for the handling of incoming messages that were created.
     * @param {Object{}} data Format is and object with the properties {element, message}
     * 
     */
    $(document).on('MessageStream:NewMessage Header:NewMessage', function(event, data) {

        var message = data.message;
        var $element = $(data.element);

        //was this a newly created message from the create new message button?
        if ($element && $element.closest('.create-message').length > 0) {
            //remove the create message area
            $element.closest('.create-message').fadeOut(function() {
                $(this).remove();
            });
        }

        //ignore empty messages
        if (message === undefined) {
            return;
        }

        //only continue if calendar is created and is visible
        if (_calendarInstance === undefined || _calendarInstance.is(':hidden')) {
            return;
        }

        //indicate to the user that a new message is being processed
        _this.addLoader();

        //does the event already exist
        //discovered by using the xid of the message
        var findExisting = _this.findEvent(message);

        //the placeholder for the message if converted into an event object for the
        //calendar plugin to render
        var eventObject;

        //undefined then we create a new entry
        if (findExisting === undefined) {

            //create a calendar event object from the new data
            eventObject = _this.prepareCalendarEvent(message);

            //add the new calendar item to the calendar
            _calendarInstance.fullCalendar('renderEvent', eventObject, true);

            //what happens if the new object is scheduled for the 
            //active day in the message editor?
            //TODO: Handle this @JH
            //rerender same month
            var listOfEvents = _this.getCalendarEventsByDate(_calendarMessageEditor.attr('data-date'));

            _this.populateMessageEditor(listOfEvents);

            _this.refreshAjaxed();

            //removes the loading indicator
            _this.removeLoader();

            return;
        }

        //if the message objects are identical then
        //there has been no change, prevent further processing
        if (_.isEqual(findExisting.message, message)) {
            _this.removeLoader();
            return;
        }

        //create a calendar event object from the new data and replace
        eventObject = _this.prepareCalendarEvent(message);

        //update the existing calendar event object with the changes
        findExisting = _.extend(findExisting, eventObject);

        //visually update the calendar
        _calendarInstance.fullCalendar('updateEvent', findExisting);

        //rerender the updated data with the template
        var rendered = $(templates.CalendarWidget_Message(findExisting));

        //replace the currently existing scheduled message 
        var $scheduledMessage = $('.scheduled-message[data-xid="' + message.xid + '"]');
        $scheduledMessage.replaceWith(rendered);

        //update the data against the message
        //allows for change checks and other comparisons
        $scheduledMessage.data('item', eventObject);

        //remove the loading indicator
        _this.removeLoader();

        _this.refreshAjaxed();

    }); // ./ on(NewMessage)

    /**
     * Toggles the calendar view for a user as opposed to a workspace
     * Workspace setting is in settings-workspaceSettings    
     */
    $(document).on('click', '.toggleCalendarFeature', _this.toggleCalendar);

    /**
     * Add a new message to a date on the calendar message editor
     */
    $(document).on('click', '.add-scheduled-message', _this.addScheduledMessage);


    /**
     * Allows a clicked dropdown box in the calendar message editor to be changed based on selected item
     */
    $(document).on('click', '.create-message .auto-select ul li a, .scheduled-message .auto-select ul li a', _this.selectPuppet);

    /**
     * Retrieve the data based on the next and previous button when clicked
     */
    $(document).on('click', 'button.fc-prev-button,button.fc-next-button', _this.getCalendarDataforMonth);

    /**
     * Rather than have the calendar widget have the functionality embedded we 
     * control it here. The calendar widget is simply responsible for providing
     * the relevant objects to act against
     */
    $(document).on('Calendar:eventClick Calendar:dayClick', function onCalendarDayClick(event, date, view) {

        //highlights the clicked days cell
        var calendarDate = _this.getSelectedDate(date);
        _this.highlightClicked(calendarDate);

        //if the date matches then ignore the users click
        if (_calendarMessageEditor.attr('data-date') === calendarDate && _calendarMessageEditor.is(':hidden') === false) {
            return;
        }

        //if we want to display the new message button we have to retrieve the 
        //parsed date string from the users click
        //expects calenderDate to be in the format  yyyy-mm-dd
        var safeDate = calendarDate.split('-');
        var safeYear = safeDate[0];
        var safeDay = safeDate[2];
        var safeMonth = safeDate[1];
        var selectedDate = new Date(safeYear, safeMonth - 1, safeDay);
        var currentDate = new Date();

        //set the editor widgets current date for messages
        _this.setEditorDate(calendarDate);

        // Change the title, with a little visual pop
        $('.calendar-tab-title', _calendarMessageEditor).hide().text(selectedDate.format('dd MM yy') + ' Messages').fadeIn();

        //display the associated list of messages for that day
        if (_calendarMessageEditor.is(':hidden')) {
            _calendarMessageEditor.show();
        }

        //do not allow the user to add a message if the date is before the current day
        //if the user attempts to set a scheduled message before the current time they will
        //see a notification error
        if (Date.sameDay(selectedDate, currentDate) || selectedDate > currentDate) {
            _calendarMessageEditor.find('.add-scheduled-message').removeClass('hidden');
            _calendarMessageEditor.find('.add-scheduled-message').attr('title', 'Cannot add messages on a day that has passed');
        } else {
            _calendarMessageEditor.find('.add-scheduled-message').addClass('hidden');
        }

        //clear the message area
        var $messagesArea = _calendarMessageEditor.find('.messages');
        $messagesArea.empty();

        //get all events for a particular day
        var listOfEvents = _this.getCalendarEventsByDate(calendarDate);

        //there are no messages for this specific day
        //notify the user
        if (listOfEvents === undefined) {
            _this.addNoMessages();
            return;
        }

        //when populating the message editor we want to do so in date order for the user
        listOfEvents = _this.sortByTime(listOfEvents);

        //render message on scheduled messages pane
        _this.populateMessageEditor(listOfEvents);

        _this.refreshAjaxed();
    }); // on dayClick

    /**
     * handles the delete button functionality for now on calendar message editor view
     * Should be standardised
     *
     */
    $(document).on('CalendarWidget:DeleteNewMessage', function(event, element) {
        var $element = $(element);
        $element.closest('.data').remove();
    });
    
}; // ./CalendarWidget()


/**
 * Sorts a list by date, must have a date key within list to be sorted
 * @param  {Object[]} list An array of events
 * @return {Object[]}      The list of objects containing the date parameter sorted by date desc
 */
CalendarWidget.prototype.sortByDate = function sortByDate(list) {
    return _.sortBy(list, 'date');
};

/**
 * Sorts a list by time i.e. 12:00 < 13:00
 * @param  {Object[]} list A list of objects, each object contains the parameter date
 * @return {[type]}      [description]
 */
CalendarWidget.prototype.sortByTime = function sortByTime(list) {
    return _.sortBy(list, 'time');
};

/**
 * Groups a list by date
 * Produces an object where key is the date and value is the object
 * events within this date
 * @param  {Object[]} list A list of objects, each object contains the parameter date
 * @return {Object}      [description]
 */
CalendarWidget.prototype.groupByDate = function groupByDate(list) {
    return _.groupBy(list, 'date');
};

/**
 * @return {Date}
 */
CalendarWidget.prototype.getEditorDate = function() {
    return this.editorDate;
};

/**
 * Get a string formatted date and time 2015-04-17 12:00
 * @param  {Date} date   The date object
 * @param  {String} format The format to be rendered
 * @return {String}
 */
CalendarWidget.prototype.getDatetime = function getDatetime(date) {
    return date.format('yy-mm-dd hh:ii');
};

/**
 * Get a string formatted date 2015-04-17
 * @param  {Date} date A date object
 * @return {String}
 */
CalendarWidget.prototype.getCalendarFormattedDate = function getCalendarFormattedDate(date) {
    return date.format('yy-mm-dd');
};

/**
 * Get a string formatted time - 12:00
 * @param  {Date} date A date object
 * @return {String}
 */
CalendarWidget.prototype.getTime = function getTime(date) {
    return date.format('hh:ii');
};

/**
 * Get a string formatted time with am/pm - 12:00 pm
 * @param  {Date} date A date object
 * @return {String}
 */
CalendarWidget.prototype.getUserFriendlyTime = function getUserFriendlyTime(date) {
    return this.getTime(date) + ' ' + (date.getHours() >= 12 ? 'pm' : 'am');
};

/**
 * Get date formatted as a string from a custom format
 * @param  {Date} date   A date object
 * @param  {String} format The format string e.g. yy-mm-dd hh:ii OR yy/mm/dd e.t.c
 * @return {String}
 */
CalendarWidget.prototype.getCustomDate = function getCustomDate(date, format) {
    return format !== undefined ? date.format(format) : date.format('yy-mm-dd hh:ii');
};