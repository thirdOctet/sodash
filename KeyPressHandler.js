/**
 * @author Jonathan Hussey <jonathan@winterwell.com>
 *
 * Hopefully an elegant way of handling key presses across multiple view elements in the dom
 * A widget will usually have key presses attached and this approach is an attempt
 * to decouple functionality to a specific class
 *
 * To use this do the following:
 * - For the object in question, when initialised, register the keypresses you are concerned with
 * e.g
 * ------------------------------------------------------------
 * var object = new Object() //any object
 * var keys = [3,4,1,67,78,2]; //such as enter, down, up, page down e.t.c
 * var kph = KeyPressHandler.getInstance();
 * kph.addHandler(keys, object)
 * ------------------------------------------------------------
 *
 * Now all keypresses the object is concerned with will be registered against the key press handler
 * To make this really useful the object must implement the onKeyDown method
 *
 * ------------------------------------------------------------
 * var onKeyDown = function(event){};
 *------------------------------------------------------------
 *
 *
 * It is better if you prepare an object concerned with the object in question ** see MessageStream **
 * This object will act as the environment variables each function cares about
 * This is because there are cases where the function should early terminate based on the environment
 *
 * Now in order to prevent every key press triggering the onkeydown listener, as was with the previous implementation,
 * we are only going to execute the function pertaining to the key e.g. enter, up e.t.c
 *
 * For this to be of any meaning we use a map contained within KeyPressMediator which contains
 * the key:value pairing for the keyCode and the function name
 *
 * If you want your object, who cares about certain key presses, to take an action based on a key press then it must
 * implement the function name associated with the keyCode in keyPressMediator. In a sense the Mediator is acting as
 * an interface, demanding that you must implement the function calls for the key maps in order to take action based on a
 * key press.
 *
 *
 * Singleton _instance of the KeyPressHandler
 * To retrieve a reference to the handler call KeyPressHandler.get_instance()
 * @return {KeyPressHandler{}}          The handler for kepresses within the interface
 */
var KeyPressHandler = (function($, _) {

    var _instance;

    // var _keyPressMediator = new KeyPressMediator();

    /**
     * Stores the elements/objects registered against the key pressed
     * @type {Object}
     */
    var _registeredListeners = _registeredListeners || {};

    /**
     * Predetermined key mappings merged with jquery ui mappings
     * on init()
     * @type {Object}
     */
    var _keys = {
        ZERO: 48,
        MINUS: 189,
        EQUALS: 187,
        NUMPAD_ZERO: 96,
        NUMPAD_MINUS: 109,
        NUMPAD_PLUS: 107,
        TAB: 9,
        UP: 38,
        Q: 81,
        W: 87,
        E: 69,
        R: 82,
        T: 84,
        Y: 89,
        U: 85,
        I: 73,
        O: 79,
        P: 80
    };

    /**
     * Register an object against the list of keys
     * @param  {Numer[]} keyList    A list of keys to store against an object
     * @param  {Object} object
     */
    var _registerListeners = function(keyList, object) {
        for (var i = keyList.length - 1; i >= 0; i--) {
            var key = keyList[i];
            if (_hasKey(key)) {
                _registeredListeners[key].push(object);
            } else {
                _prepareListener(key);
                _registeredListeners[key].push(object);
            }
        }

    };

    /**
     * Check if the object contains the key
     * http://jsperf.com/in-vs-hasownproperty-vs-has
     * @param  {Number}  key The keyCode of the key pressed
     * @return {Boolean}     Verification of whether the key is in the registered listener object
     */
    var _hasKey = function(key) {
        return _registeredListeners[key] !== undefined;
    };

    /**
     * Sets up the keypresses to the default value
     * @param  {[type]} key [description]
     */
    var _prepareListener = function(key) {
        _registeredListeners[key] = [];
    };

    /**
     * For the key press only execute functionality based around the key pressed
     * only for the elements concerned
     * This should essentially follow the command pattern where objects/elements implement the
     * function but define the functionality within the element
     * @param  {[type]} event [description]
     */
    var _triggerListeners = function(event) {

        //get the key pressed and retrieve the list of objects against the key
        var keyPressed = _getKeyPressed(event),
            listeners = _getListeners(keyPressed);

        //fire the events concerned with the listener
        _.each(listeners, function(listener, index, listenerList) {

            //this follows the command pattern all listeners must implement the keyPress function
            listener.onKeyDown(event);
        });
    };

    /**
     * For the key pressed get the list of objects registered against it
     * @param  {Number} key The key pressed
     * @return {Object[]}     The list of objects registered against the keypress
     */
    var _getListeners = function(key) {
        return _hasKey(key) === true ? _registeredListeners[key] : [];
    };

    /**
     * Cross browser key press determinant
     * @param  {event} event
     * @return {Number}       The keycode of the keypressed
     */
    var _getKeyPressed = function(event) {
        return event.which || event.keyCode || event.charCode;
    };

    /**
     * Create a map of keys with their associated array of objects
     */
    var _prepareListeners = function() {
        _.each(_keys, function(keyCode, key, list) {

            //string version of _keys object for key
            _prepareListener(keyCode);
        });
    };

   /**
     * Expects an object of key mappings
     * @param {[type]} keyObject [description]
     */
    var _addKeys = function(keyObject) {
        _keys = _.extend(_keys, keyObject);
    };

    /**
     * Publically available methods
     */
    var init = function() {

        //preparation

        assert($.ui.keyCode !== undefined, "KeyPressHandler:init - jquery ui key bindings missing");

        _addKeys($.ui.keyCode);

        _prepareListeners();

        return {
            addListener: function(key, object) {
                _registerListeners(key, object);
            },
            getKeyPressed: function(event) {
                return _getKeyPressed(event);
            }
        };
    };

    /**
     * Register the function against the keydown event
     */
    $(document).on('keydown', _triggerListeners);

    /**
     * Here have your singleton object
     */
    return {
        getInstance: function() {
            if (!_instance) {
                _instance = init();
            }
            return _instance;
        }
    };
})(jQuery, _);
