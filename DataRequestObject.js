/**
 * An object which allows for the storage of requests for data
 * It consists of the url, the deferred and the result of the deferred
 *
 * @author Jonathan Hussey <jonathan@winterwell.com>
 * @requires SJTest
 * @requires jQuery
 */
var DataRequestObject = (function() {

    /**
     * A useful object for interacting with data requests
     */
    function DataRequestObject(props) {

        /**
         * The url of the request
         * @type {String}
         */
        this.url;

        /**
         * This will always be a jquery deferred once a request is made
         * @type {Deferred}
         */
        this.request;

        /**
         * This will normally be the result of the data request
         * @type {Object|Array}
         */
        this.data;

        /**
         * Initialisation function
         * @param  {Object} props
         * @return {DataRequestObject}
         */
        this.init = function(props) {
            if (!props.url) return this;
            this.setUrl(props.url)
                .setRequestType(props.type)
                .makeRequest()
                .requestHandler();

            return this;
        };

        /**
         * Initialisation function
         */
        this.init(props);

    }

    /**
     * Set the request type e.g. DataLoader or json, defaults to json request
     * @param {String} type The request for data
     * @return {String}
     */
    DataRequestObject.prototype.setRequestType = function(type) {
        this.type = type ? type : 'json';
        return this;
    };

    /**
     * Get the request type
     * @param {String} type The request for data
     * @return {String}
     */
    DataRequestObject.prototype.getRequestType = function() {
        return this.type;
    };

    /**
     * Make a request for the data based on the url
     * @param {Deferred} deferred The request for data
     * @return {DataRequestObject}
     */
    DataRequestObject.prototype.setUrl = function(url) {
        assert(url, "DataRequestObject.js - setUrl: url is not set for this DataRequestObject");
        this.url = url;
        return this;
    };

    /**
     * Make a request for the data based on the url
     * @param {Deferred} deferred The request for data
     * @return {DataRequestObject}
     */
    DataRequestObject.prototype.setRequest = function(deferred) {
        assert(deferred, "DataRequestObject.js - setRequest: url is not set for this DataRequestObject");
        this.request = deferred;
        return this;
    };

    /**
     * Set the data from the json request
     * @param {Object|Array} data The data from the deferred request
     * @return {DataRequestObject}
     */
    DataRequestObject.prototype.setData = function(data) {
        assert(data, "DataRequestObject.js - setData: data is undefined for this DataRequestObject");
        this.data = data;
        return this;
    };

    /**
     * The data returned from the original request
     */
    DataRequestObject.prototype.getData = function() {
        return this.data;
    };

    /**
     * The request url
     * @return {String}
     */
    DataRequestObject.prototype.getUrl = function() {
        return this.url;
    };

    /**
     * A Jquery Deferred
     * @return {Deferred}
     */
    DataRequestObject.prototype.getDeferred = function() {
        return this.request;
    };


    /**
     * Make a request for the data based on the url
     * @return {DataRequestObject}
     */
    DataRequestObject.prototype.makeRequest = function() {
        assert(this.url, "DataRequestObject.js - makeRequest: url is not set for this DataRequestObject");
        this.setRequest(this.getDataSource(this.url));
        return this;
    };

    /**
     * Allows for the custom retrieval of dashboard widgets
     * Used in cases DataLoader.get fails
     * @param  {String} jsonUrl
     * @return {Deferred}
     */
    DataRequestObject.prototype.getDataSource = function(jsonUrl) {
        assert(jsonUrl, "DataRequestObject.js - getDataSource: jsonUrl is undefined");
        if(this.type && this.type === 'DataLoader'){
            return DataLoader.get(this.url);
        } else {
            return $.getJSON(jsonUrl);
        }

    };

    /**
     * When the deferred is complete set the data returned against the object
     */
    DataRequestObject.prototype.requestHandler = function() {
        this.request
            .done($.proxy(function(data) {
                this.setData(data);
            }, this))
            .fail(function(error) {
                console.error(error);
            });
    };



    return DataRequestObject;
}());
