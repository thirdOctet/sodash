/**
 * file: outbox.js
 * Added by OutboxServlet.java
 * @author Jonathan Hussey <jonathan@winterwell.com>
 *
 * 
 */

/** Cheap object -- can just be remade */
var OutBox = function() {};

/**
 * Get the data for this time period.
 * @param start {!Date}
 * @param end {!Date}
 */
OutBox.prototype.getData = function getData(start, end) {
    // TODO we might want to add caching here -- but caching is error-prone and not essential, so NOT for the alpha release
    assertMatch(start, Date, end, Date);
    var url = setUrlType(window.location, '.json');
    return DataLoader.get(url, {
        start: start.getTime(),
        end: end.getTime(),
        numres: 200 //perhaps to get as many as possible (not all are returned)
    });
};

(function($) {

    var outbox = new OutBox();

    //the calendar widget for the scheduled area
    //note: setting this here will allow other events and the templates to access the variable
    window.calendarWidget = new CalendarWidget();

    //get calendar templates
    var calendarTemplates = loadTemplates('/static/code/widgets/CalendarWidgetTemplates.html');
    var streamRowTemplates = loadTemplates('/static/code/widgets/StreamRowTemplates.html');
    var miscTemplates = loadTemplates('static/code/widgets/Misc.html');

    //this will check if the workspace setting is enabled
    //as opposed to the url

    //The calendar is limited to the broadcast page for now
    //Waiting for streamtabs to provide us with the current stream will take too long
    if (window.location.href.indexOf('status=ALL_BAR_TRASH&misc=threadroots&uni=true') === -1) {
        return;
    }

    //get the current month
    var month = moment().month();
    var year = moment().year();

    //set up the date range 
    var startDate = new Date(year, month, 1);
    var endDate = new Date(year, (month + 1), 1);

    //get the data based on the current range
    var scheduledData = outbox.getData(startDate, endDate);

    //we will need to know the team members so we can populate the message list with trueuser data
    var teamRequest = DataLoader.get('/creole/team.json', {
        detail: 'lite'
    });

    //placeholder for calendar templates
    var $calendar, $calendarMessageEditor;

    //to store scheduled messages
    var scheduledMessages;

    //store calendar events when the first set is retrieved
    var calendarEvents;

    //render templates
    // FIXME refactor this, so we have a function which can take in fresh data and update the calendar
    $.when(calendarTemplates, streamRowTemplates, miscTemplates, teamRequest, scheduledData).done(function(ct, srt, misc, teamData, data) {

        //set the team so that we can refer to it later for ease of access
        var team = teamData[0].groups[Creole.project].members || {};

        //lets store a xid->object mapping of the team
        var teamMappedToXID = {};

        _.each(team, function(Member, key, list) {
            teamMappedToXID[Member.xid] = Member;

            //what about members who xids against their name?
        });

        //Where would be a better place to store this if others needed it?
        //and what would be a better format?
        //others will not know how the format is created
        //xid->Person
        Creole.workspace.team = teamMappedToXID;

        //if we are on the broadcast page is the calendar enabled?
        if (Creole.settings.calendarEnabled === undefined || Creole.settings.calendarEnabled === false) {
            return;
        }

        //set up the data
        scheduledMessages = data[0];
        assert(_.isArray(scheduledMessages) === true, 'Outbox.js - : data received is not an array');

        //we don't want to do anything if the data is empty
        if (scheduledMessages.length === 0) {

            //TODO: perhaps signal to the user that no messages exist
            return;
        }

        //html templates
        $calendar = $(templates.CalendarWidget_Wrapper());

        //get the area in which the messages will be displayed
        $calendarMessageEditor = $(templates.CalendarWidget_MessageList());

        //prepare the calendar events for the calendar plugin
        calendarEvents = calendarWidget.prepareCalendarEvents(scheduledMessages);

        //we want to populate when the mainbody is available
        //appears to attempt to do this before page is rendered
        var timeout = setInterval(function(event) {

            //is the mainbody available?
            if ($('#mainbody').length > 0) {

                //prepare the calendar
                $('#mainbody').append($calendar);

                //initialise the calendar
                //createCalendar sets the initialised calendar against the calendar object
                calendarWidget.createCalendar($calendar, {

                    //provide the calendar with the converted event objects based on the message
                    events: calendarEvents,

                    //wire up the click event against the calendar columns
                    dayClick: function(date, event, view) {
                        $(document).trigger('Calendar:dayClick', [date, view, event]);
                    },
                    eventClick: function(date, event, view) {
                        $(document).trigger('Calendar:eventClick', [date, view, event]);
                    },

                    //append the calendar to the date based on the calendar contents
                    eventRender: function(event, element) {
                        $(element).append([event.contents]);
                    },

                    eventLimitClick: function(event, event2, event3) {
                        // console.log("a message");
                        // event.dayEl.click();
                        $(document).trigger('Calendar:eventClick', [event.date])
                    },
                    eventLimit: 3
                        // views: {
                        //     agenda: {
                        //         eventLimit: 4
                        //     }
                        // }
                });

                //sets the calendar widgets editor
                calendarWidget.setCalendarEditor($calendarMessageEditor);

                //editor for selected days to display editable messages within a sidebar
                $calendar.append($calendarMessageEditor);

                //remove the loading indicator
                $('#mainbody').find('.loading').remove();

                //stop checking if the #mainbody div is available
                clearTimeout(timeout);
            }
        }, 200);

    });



    /**
     * Rather than have the calendar widget have the functionality embedded we 
     * control it here. The calendar widget is simply responsible for providing
     * the relevant objects to act against
     */
    $(document).on('Calendar:eventClick Calendar:dayClick', function onCalendarDayClick(event, date, view) {


        //highlights the days cell
        var calendarDate = calendarWidget.getSelectedDate(date);
        calendarWidget.highlightClicked(calendarDate);

        //if we want to display the new message button we have to retrieve the 
        //parsed date string from the users click
        var safeDate = calendarDate.split('-');
        var safeYear = safeDate[0];
        var safeDay = safeDate[2];
        var safeMonth = safeDate[1];
        var selectedDate = new Date(safeYear, safeMonth - 1, safeDay);
        var currentDate = new Date();

        //if the date matches then ignore the users click
        if ($calendarMessageEditor.attr('data-date') === calendarDate && $calendarMessageEditor.is(':hidden') === false) {
            return;
        }

        //get all events for a particular day
        var listOfEvents = calendarWidget.getCalendarEventsByDate(calendarDate);

        //set the editor widgets current date for messages
        calendarWidget.setEditorDate(calendarDate);

        // Change the title, with a little visual pop
        $('.calendar-tab-title', $calendarMessageEditor).hide().text(selectedDate.format('dd MM yy') + ' Messages').fadeIn();

        //display the associated list of messages for that day
        if ($calendarMessageEditor.is(':hidden')) {
            $calendarMessageEditor.show();
        }

        //clear the message area
        var $messagesArea = $calendarMessageEditor.find('.messages');
        $messagesArea.empty();

        //do not allow the user to add a message if the date is before the current day
        //if the user attempts to set a scheduled message before the current time they will
        //see a notification error
        if (Date.sameDay(selectedDate, currentDate) || selectedDate > currentDate) {
            $calendarMessageEditor.find('.add-scheduled-message').removeClass('hidden');
            $calendarMessageEditor.find('.add-scheduled-message').attr('title', 'Cannot add messages on a day that has passed');
        } else {
            $calendarMessageEditor.find('.add-scheduled-message').addClass('hidden');
        }

        //there are no messages for this specific day
        //notify the user
        if (listOfEvents === undefined) {
            calendarWidget.addNoMessages();
            return;
        }

        //when populating the message editor we want to do so in date order for the user
        listOfEvents = calendarWidget.sortByTime(listOfEvents);

        //render message on scheduled messages pane
        calendarWidget.populateMessageEditor(listOfEvents);

        calendarWidget.refreshAjaxed();
    });

    /**
     * Event raised by MessageStream.doSend, Header.header_initUpdates
     * Allows for the handling of incoming messages that were created.
     */
    $(document).on('MessageStream:NewMessage Header:NewMessage', function(event, data) {

        var message = data.message[0];
        var $element = data.element !== undefined ? $(data.element) : undefined;

        //was this a newly created message from the create new message button?
        if ($element && $element.closest('.create-message').length > 0) {
            //remove the create message area
            var $createMessage = $element.closest('.create-message');

            $createMessage.fadeOut(function() {
                $(this).remove();
            });
        }

        //ignore empty messages
        if (message === undefined) {
            return;
        }

        //only continue if calendar is created and is visible
        if ($calendar === undefined || $calendar.is(':hidden')) {
            return;
        }

        //indicate to the user that a new message is being processed
        calendarWidget.addLoader();

        //does the event alread exist
        //discovered by using the xid of the message
        var findExisting = calendarWidget.findEvent(message);

        //the placeholder for the message if converted into an event object for the
        //calendar plugin to render
        var eventObject;

        //undefined then we create a new entry
        if (findExisting === undefined) {

            //create a calendar event object from the new data
            eventObject = calendarWidget.prepareCalendarEvent(message);

            //add the new calendar item to the calendar
            $calendar.fullCalendar('renderEvent', eventObject, true);

            //what happens if the new object is scheduled for the 
            //active day in the message editor?
            //TODO: Handle this @JH

            //removes the loading indicator
            calendarWidget.removeLoader();

            return;
        }

        //if the message objects are identical then
        //there has been no change, prevent further processing
        if (_.isEqual(findExisting.message, message)) {
            calendarWidget.removeLoader();
            return;
        }

        //create a calendar event object from the new data and replace
        eventObject = calendarWidget.prepareCalendarEvent(message);

        //update the existing calendar event object with the changes
        findExisting = _.extend(findExisting, eventObject);

        //visually update the calendar
        $calendar.fullCalendar('updateEvent', findExisting);

        //rerender the updated data with the template
        var rendered = $(templates.CalendarWidget_Message(findExisting));

        //replace the currently existing scheduled message 
        var $scheduledMessage = $('.scheduled-message[data-xid="' + message.xid + '"]');
        $scheduledMessage.replaceWith(rendered);

        //update the data against the message
        //allows for change checks and other comparisons
        $scheduledMessage.data('item', eventObject);

        //remove the loading indicator
        calendarWidget.removeLoader();

        calendarWidget.refreshAjaxed();

    });

    /**
     * Event raised by MessageStream.doSaveDraft upon changes
     * element - the original 
     */
    $(document).on('MessageStream:DraftSaved', function(event, data) {

        calendarWidget.addLoader();

        var $source = $(data.element);

        var $scheduledMessage = $source.closest('.scheduled-message');

        var eventData = $scheduledMessage.data('item');

        var calendarEventsActive = $calendar.fullCalendar('clientEvents');

        //create a calendar event object from the new data and replace
        var eventObject = calendarWidget.prepareCalendarEvent(data.updatedData);

        //Is this is a new message being created
        if ($('.create-message').length > 0) {
            //remove the message and add it to the calendar
            $('.create-message').fadeOut(function() {
                $(this).remove();
            });

            $calendar.fullCalendar('renderEvent', eventObject, true);

            //rerender same month
            var listOfEvents = calendarWidget.getCalendarEventsByDate($calendarMessageEditor.attr('data-date'));

            calendarWidget.populateMessageEditor(listOfEvents);

            calendarWidget.refreshAjaxed();

            //remove the 'no message' message
            calendarWidget.removeNoMessages();

            calendarWidget.removeLoader();

            return;

        }

        //update the event data
        _.find(calendarEventsActive, function(Event, index, list) {
            if (eventData !== undefined && eventData.message.xid === Event.message.xid) {

                //extend the calendar object event data
                Event = _.extend(Event, eventObject);

                //update the calendar with the updated event data
                $calendar.fullCalendar('updateEvent', Event);

                //rerender the element
                var rendered = $(templates.CalendarWidget_Message(Event));

                //if the event has been moved to a new date then we remove it from the scheduled messages editor
                if ($calendarMessageEditor.attr('data-date') !== eventObject.date) {

                    //remove the scheduled message from this editor
                    $scheduledMessage.remove();

                } else {
                    //update the contents of the scheduled message area
                    $scheduledMessage.replaceWith(rendered);

                    //update the data against the message
                    rendered.data('item', eventObject);
                }

                //escape out of loop
                return true;
            }
        });

        calendarWidget.addNoMessages();

        calendarWidget.removeLoader();

        calendarWidget.refreshAjaxed();

    });

    /**
     * When a message is deleted 
     * @param  {[type]} event   [description]
     * @param  {[type]} element [description]
     * @param  {[type]} data)   [description]
     * @return {[type]}         [description]
     */
    $(document).on('MessageStream:MessageDeleted', function(event, element, data) {

        calendarWidget.addLoader();
        //we have to handle the removal of the event for the calendar
        //remove the element from 
        var $source = $(element);

        var $scheduledMessage = $source.closest('.data');

        var eventData = $scheduledMessage.data('item');

        var calendarEventsActive = calendarWidget.getCalendarEvents();

        //update the event data
        _.find(calendarEventsActive, function(Event, index, list) {
            if (eventData !== undefined && eventData.message.xid === Event.message.xid) {

                //update the calendar with the updated event data
                $calendar.fullCalendar('removeEvents', Event._id);

                //rerender the element
                $scheduledMessage.remove();

                calendarWidget.addNoMessages();

                //escape out of loop
                return true;
            }
        });

        calendarWidget.removeLoader();

        calendarWidget.refreshAjaxed();
    });


    /**
     * Toggles the calendar view for a user as opposed to a workspace
     * Workspace setting is in settings-workspaceSettings    
     */
    $(document).on('click', '.toggleCalendarFeature', function(event) {

        //get the current setting
        var currentState = Creole.settings.calendarEnabled;

        //update the backend
        DataLoader.post('/settings-userSettings.json', {
            action: 'set-settings',
            calendar: !currentState
        }).done(function(data) {
            //we allow page reloads rather than the default no action
            event.preventDefault();
            Creole.settings.calendarEnabled = data.calendarEnabled;
            window.location.href = window.location.href;
        }).fail(function(error) {
            console.log(error);
        });
    });

    $(document).on('QueryBuilder:ready', function(event) {
        $('#mainbody').append(templates.Misc_loading());
        $.when(calendarTemplates).done(function() {
            $('.WizardLinks').prepend($.parseHTML(templates.CalendarWidget_ToggleCalendarFeature()));
        });
    });


    /**
     * Retrieve the data based on the next and previous button
     * @param  {[type]} event) {                           var date [description]
     * @return {[type]}        [description]
     */
    $(document).on('click', 'button.fc-prev-button,button.fc-next-button', function(event) {

        //signal that the calendar is loading data
        calendarWidget.addLoader();

        //Trigger polling on selected month
        //a moment object
        var date = $calendar.fullCalendar('getDate');

        var month = date.month();
        var year = date.year();

        //allows us to then get next time period but changes the
        //moment object to the next month
        date.add(1, 'months');

        //date is always going to be current date plus 1
        //to get all month
        var nextMonth = date.month() + 1;
        var nextYear = date.year();

        var startDate, endDate;

        startDate = new Date(year, month, 1); //this month
        endDate = new Date(nextYear, nextMonth, 1); //next month

        //get the data
        var messages = outbox.getData(startDate, endDate);

        //of the work when new messages are received
        $.when(messages).done(function(data) {
            //populate the calendar
            //this is used to only process updated or changed elements
            var notInCalendar = [];

            _.each(data, function(Message, key, list) {
                //preferably build the list before rendering
                //only process the data that has changed
                var findExisting = calendarWidget.findEvent(Message);

                var eventObject;

                //this is a new scheduled message
                if (findExisting === undefined) {
                    //prepare the object
                    eventObject = calendarWidget.prepareCalendarEvent(Message);
                    notInCalendar.push(eventObject);
                } else {

                    //this happends much rarely than expected
                    //do not update the calendar if the data is the same
                    //expensive but less expensive than rerendering the calendar plugin
                    //and locking the users machine
                    if (_.isEqual(findExisting.message, Message)) {
                        return;
                    }

                    //if it does exist then lets update and add
                    //update the existing calendar event object with the changes
                    findExisting = _.extend(findExisting, eventObject);
                    $calendar.fullCalendar('updateEvent', findExisting);
                }
            });

            calendarWidget.removeLoader();

            //this will allow us to render the calendar without causing the browser 
            //to crash due to memory errors
            $calendar.fullCalendar('addEventSource', notInCalendar);

        });
    });

    $(document).on('click', '.add-scheduled-message', function(event) {
        //open the message creator
        if ($('.create-message').length > 0) return;
        // The date for the selected cell to pass into the form and hence the Scheduler widget
        var sdate = $calendarMessageEditor.attr('data-date');
        var thisDay = null;
        if (sdate) {
            var d = new Date(sdate);
            d.setHours(9); // Default to 9am. How should we handle time-of-day??
            thisDay = d.getTime();
        }
        var form = templates.CalendarWidget_CreateMessage({
            thefn: '$(document).trigger(\'CalendarWidget:DeleteNewMessage\',this);',
            pubDate: thisDay
        });
        var $msgs = $calendarMessageEditor.find('.messages');
        assert($msgs.length === 1, $msgs);
        $msgs.prepend(form);

        $msgs.find('.add-attachments').sodashuploader();

        calendarWidget.refreshAjaxed();
    });

    $(document).on('click', '.create-message .auto-select ul li a, .scheduled-message .auto-select ul li a', function(event) {

        event.preventDefault();

        //the setup
        var $this = $(this),
            $autoSelect = $this.closest('.auto-select'),
            $input = $autoSelect.find('.input-group input'),
            $serviceIcon = $autoSelect.find('.input-group .service');

        //we might need to update the service icon based on the users selection
        var icons = templates.fn.StreamRow_GetReplyAsIconMap();

        var $form = $this.closest('form');

        //lets get the value and start changing things
        var xid = $this.attr('data-xid');

        //is populated once only
        //cached against Creole by function call
        var puppets = templates.fn.StreamRow_MapAllPuppets();
        var puppet = _.find(puppets, function(Puppet, key, list) {
            return Puppet.xid === xid;
        });

        $form.find('input[name="service"]').val(puppet.service);
        var $textarea = $form.find('textarea');

        _.each(icons, function(iconName, service, list) {
            $serviceIcon.removeClass('icomoon-' + iconName);
        });

        //set the service icon to the new puppets unless is not a service
        puppet.service ? $serviceIcon.addClass('icomoon-' + icons[puppet.service]) : $serviceIcon.addClass('icomoon-bullhorn');

        $textarea.attr('data-service', puppet.service);
        $textarea.textCounter('update', {
            service: puppet.service
        });

        //set the xid of the input element
        $input.attr('data-xid', puppet.xid);

        //set the text
        $input.val($.trim(puppet.name));
    });

    /**
     * handles the delete button functionality for now
     * Should be standardised
     * @param  {[type]} element  The html element that was clicked
     * @return {[type]}          [description]
     */
    $(document).on('CalendarWidget:DeleteNewMessage', function(event, element) {
        var $element = $(element);
        $element.closest('.data').remove();
    });

})(jQuery);
