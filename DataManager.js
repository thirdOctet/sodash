/**
 * TODO: @JH Refactor
 * A method by which to handle data requests throughout the entire application
 * - The purpose of this is to store requests to data so that other items throughout
 *   the application can check first before making a request
 * - The cost is minimal for this check
 * - Some parts of the application make json requests separate to CreoleLoader
 * - We want to be able to implement this type of functionality across the board for all json requests
 * - Multiple data managers can be created if needed for different types of data management
 * - This means each object can have its own data manager
 *
 * Benefits:
 * - Requests that have already been sent can now piggyback on the completion of the same request
 * - We can now test for when the data is retrieved
 * - This leads to perhaps a smarter way to listen for a specific type of completed tasks
 * --   Can tie into events system
 * - Requests to data are asynchronous
 * - We have separated html and javascript on some template files
 * - A set of actions can be performed before rendering data e.g. Grid layouts on dashboard
 *
 * Challenges:
 * - convention agreement
 * ---- May lead to duplicate requests stored due to a typo or rebellion against the convention
 * - When do developers decide when to check if the data is already stored?
 * - Familiarity with javascript concepts
 *
 */
var DataManager = function() {

    /**
     * A Place to access all data requests made
     */
    var _dataCentre = _dataCentre || {};

    /**
     * There are different types of urls to pull data from
     * lets seprate them from the context
     */
    var _urlList = _urlList || {};

    /**
     * Lets immediately extend this object with common default url strings
     */
    $.extend(true,_urlList, {
        stream: '/stream.json',
        dashboard: '/dashboard/charts.json',
        probes: '/creole/probes.json',
        trends: {
            project: '/msg-projectTrends.json',
            global: '/msg-globalTrends.json'
        },
        status: {
            twitter: '/msg-twitterStatus.json'
        }
    });

    /**
     * Curse you scope in external libraries
     */
    var $this = this;

    /**
     * Get the stored data objects
     * @return {_dataCentre} The data centre of the DataManager object
     */
    this.getDataCentre = function() {
        return _dataCentre;
    };

};

/**
 * Get the unique id of the data request
 * This is represented as a combination of url and params
 * How else will you uniquely identify your data request?
 *
 * This can be overwritten and used for custom unique id generation when datamanagers
 * are created against other objects
 * @param  {Object} data The data source of an object. This should have .url and .params properties.
 * @return {String}      The encoded uri string request
 */
DataManager.prototype.getUniqueID = function(data) {

    //we are checking if the url already contains the '?', if so then just return the url
    return data.url.indexOf('?') > -1 ? data.url : data.url + '?' + Url.getQueryString(data.params);
}

/**
 * Extend the dataCentre object with data requests
 * Accessible by including the name of the object being accessed
 * Create a new object of type Datamanager and add a custom addData method
 * with the parent datamanger being callable
 * @param  {String} url         The url to request data from
 * @param  {Object} params      The additional information to retrieve - optional
 * @param  {Object} source      The original data object in which the id was retrieved
 */
DataManager.prototype.addData = function(source,url, params) {
    //preconditions
    assert(_.isUndefined(url) === false, 'url must be set, ensure also it is a string');

    //set the unique id
    $.extend(true,source, {
        uniqueid: this.getUniqueID(source.data)
    });

    var uniqueid = source.uniqueid;

    // var url = source.data.url;

    // url += _.isUndefined(params) === false ? '&' + $.param(params) : '';

    $.extend(true,source, {
        url: url
    });

    //get the data centre
    var dataCentre = this.getDataCentre();

    //extend the dataCentre with the unique id
    //THIS IS WHY THIS IS A PROTOTYPE
    //you can imlplement your own way if you intend to implement this library
    dataCentre[uniqueid] = {};

    var params = _.isUndefined(source.data.params) === true ? {} : params;

    //lets create a deferred
    var dataRequest = DataLoader.get(url, source.data.params).done(function(rData) {

        //what happens if we fail?

        //when it is completed lets store the data against the dataCentre object
        $.extend(true,dataCentre[uniqueid], {
            data: rData
        });
    });

    //build the innerbody id
    var innerid = source.template.html.id + '-' + source.template.type;

    //if the user wants to create multiple widgets then we will have to ensure that they are unique
    //we do not want to render different versions of the data to the same div
    if($('#'+innerid).length>0){
        innerid = innerid + $('#'+innerid).length+1;
    }

    //lets also dynamically build the inner id of the html elements
    $.extend(true, source.template.html, {
        bodyid: innerid
    });

    //we store the original source data
    if (_.isUndefined(source) === false) {

        $.extend(true,dataCentre[uniqueid], {
            main: source
        });
    }

    //set the template base for the widget being added
    $.extend(true, dataCentre[uniqueid], {
            renderedHtml: templates.Dashboard_Generic(dataCentre[uniqueid].main)
    });

    // //if a user disables a tagset then we should not make a request for the data
    // if(_.isUndefined(source.data.sentiment) === false){
    //     var tagExists = _.find(Creole.tagSets, function(TagSet, key, list){
    //         return TagSet.name.indexOf(source.data.sentiment) > -1;
    //     });

    //     if(_.isUndefined(tagExists) === false){

    //     }
    // }




    //let the users retrieve the deferred if necessary
    //store some other parameters against the captured object
    $.extend(true,dataCentre[uniqueid], {
        deferred: dataRequest
    });
};


/**
 * Will return the data object from the DataManager
 * @param  {String} uniqueid The string or unique name that you originally stored the data against
 * @return {_dataCenter{}}   The object in the dataCentre, returns the deferred stored against the object and the data if available
 */
DataManager.prototype.getData = function(uniqueid) {
    var dc = this.getDataCentre();
    return _.isUndefined(dc[uniqueid]) === false ? dc[uniqueid] : {};
};

/**
 * You may wish to get the deferred of the request for processing
 * Even if inherit the data manager this functionality is valid
 * @param  {String} uniqueid The known id of the object stored
 * @return {Deferred{}}      The deferred of the data request
 */
DataManager.prototype.getDeferred = function(uniqueid) {
    //if the deferred does not exist then create it

    var dataObject = this.getData(uniqueid);
    return dataObject.deferred;
};

DataManager.prototype.buildDeferred = function(uniqueid) {

    var deferred;

    var dataObject = this.getData(uniqueid);

    //if the deferred does not exist then create it
    if(dataObject.hasOwnProperty('deferred') === false){
        deferred = dataObject.deferred;
    } else {
        //build the deferred
        // deferred = DataLoader.get(dataObject)
    }

    return deferred;
};
