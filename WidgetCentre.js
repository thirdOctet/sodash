/**
 * @author Jonathan Hussey <jonathan@winterwell.com>
 * @requires jquery
 * @requires JSON
 * @requires DataLoader
 * @requires Underscore
 * 
 * 
 */
var DashboardWidget = (function() {

    /**
     * The base container of a widget
     */
    function Widget() {}

    /**
     * A dashboard widget is a container for displaying information, usually in a grid on a dashboard page
     * Widgets contain numerous properties that allow for real time functionality, custom templates,
     * data sources, positioning within a grid structure
     *
     * A dashboard widget should be initialised on a dashboard page
     * @param {Object} widgetSource A json formated string of properties
     */
    function DashboardWidget(widgetSource) {

        /**
         * The blueprint for data requests.
         * Depends on DataLoader to retrieve the data
         * @type {Object}
         */
        this.dataSource = {
            url: undefined,
            params: {}
        };

        /**
         * A widget can be of many types, a chart(pie|timeseries|more to come), table, list, plain text e.t.c
         * Its is useful to set this so as to indicate to other logic within system what should be done e.g. What type of template to render as -> Dashboard_Generic_Table
         * 
         * @type {String}
         */
        this.type = undefined;

        /**
         * A widget can exist as default|custom (usually streams)|sogrow|reports|
         * 
         * @type {String}
         */
        this.widgetType = 'default';

        /**
         * A DashboardWidget will normally be part of a grid
         * The values are closely tied to a plugin - Gridster         
         * @type {Object}
         */
        this.grid = {
            sizex: 6,
            sizey: 10,
            row: undefined,
            col: undefined
        };

        /**
         * The template name the widget will use,
         * this will override the default template mechanism.
         * 
         * @type {String}
         */
        this.customTemplate = undefined;

        /**
         * Stores a reference to the html template or more specifically the container 
         * that the widget contents will be rendered to
         * @type {String}
         */
        this.renderedTemplate = undefined;

        /**
         * A jquery deferred stored on a DataLoader request
         * @type {Deferred}
         */
        this.deferred = undefined;

        /**
         * The data for this widget that was retrieved by DataLoader
         * in its raw format
         * @type {Object}
         */
        this.data = undefined;

        /**
         * By default all dashboard widgets are editable unless changed during initialisation
         * @type {Boolean}
         */
        this.settings = true;

        /**
         * The html object contains the parent id and the content id of
         * a widget. 
         * The id normally functions as the top most container of the html
         * The contentid will normally contain the results of a template rendering 
         * for a table, chart e.t.c
         * @type {Object}
         */
        this.html = {
            id: undefined,
            contentid: undefined
        };

        /**
         * A widget will normally be represented as an html block
         * The html will contain a title and an icon as a visual indicator
         * for the type of widget 
         * @type {Object}
         */
        this.header = {
            title: undefined,
            icon: undefined
        };

        /**
         * Enable or disable realtime data retrieval for a widget
         * The realtime object contains properties such as lastRequest, refreshRate and isRealTime.
         * @type {Realtime}
         */
        this.realtime = new RealTime();

        /**
         * A dashboard that is saved will use this property of the widget to rerender the widget
         * to the dashboard
         * @type {String}
         */
        this.saved = undefined;

        if (widgetSource) {

            //we want to convert any legacy styles if necessary
            //this will become obselete when dashboards are saved after the v18b release
            widgetSource = this.convertIfLegacy(widgetSource);

            //properties that will always be saved in a widget
            this.setDataSource(widgetSource.dataSource)
                .setHeader(widgetSource.header)
                .setHtml(widgetSource.html)
                //properties that may not be set but are safely handled
                //these will default to baseline properties otherwise
                .setGrid(widgetSource.grid)
                .setRealTimeProperties(widgetSource.realtime)
                .setWidgetType(widgetSource.widgetType)
                .setSettings(widgetSource.settings);

        }


    }; //DashboardWidget

    /**
     * Set the DashboardWidget to be of type widget
     * @type {Widget}
     */
    DashboardWidget.prototype = new Widget();
    DashboardWidget.prototype.constructor = DashboardWidget;

    /**
     * Export the widget in a format that can be reinstated
     * @return {String} The widget configuration 
     */
    DashboardWidget.prototype.stringify = function() {
        return JSON.stringify(this.export());
    };

    /**     
     * When editing this please ensure that you modify 
     * Dashboard.js - widgetSourceToWidget 
     * What this function exports and widgetSourcetoWidget converts
     * are interrelated
     * @return {Object} 
     */
    DashboardWidget.prototype.export = function() {

        var stringifyable = {};

        $.extend(true, stringifyable, {
            dataSource: this.filterDataSource(),
            type: this.getType(),
            header: this.getHeader(),
            html: this.getHtml(),
            grid: this.getGrid(),
            realtime: this.realtime.get(),
            customTemplate: this.getCustomTemplate(),
            widgetType: this.getWidgetType(),
            settings: this.isEditable(),
            saved: this.isSaved()
        });

        //if a chart is set and the chart type is set
        //we export this property
        if (this.getChartType()) {
            $.extend(true, stringifyable, {
                chart: {
                    type: this.getChartType()
                }
            });
        }

        return stringifyable;
    };

    /**
     * Filters unwanted exportable items from the datasource e.g. as
     * @return {Object}
     */
    DashboardWidget.prototype.filterDataSource = function(){
        //add more items to the array as deemed fit
        var blacklist = ['as'];

        var dataSource = this.getDataSource();

        //assumes that items should be deleted from the params object
        //TODO: recursive search preferred
        _.each(blacklist, function(exclude){            
            dataSource.params && dataSource.params[exclude] ? delete dataSource.params[exclude] : '';
        });

        return dataSource;
    };

    /**
     * Used to compare itself to another dashboard widget based on data request, type and if set, chart type
     * @return {String} The stringified object comparator properties
     */
    DashboardWidget.prototype.getComparator = function() {
        var comparator = {
            dataSource: this.getDataSource(),
            type: this.getType()
        };
        if (this.getChartType()) _.extend(comparator, {
            chart: {
                type: this.getChartType()
            }
        });
        return JSON.stringify(comparator);
    };

    /**
     * Is used to reset the chart properties of a widget which has retrieved the latest data     
     * @return {DashboardWidget} Chainable function
     */
    DashboardWidget.prototype.resetChart = function() {
        this.chart.options = {};
        return this;
    };

    /** 
     * Check if a realtime request can be done
     * @param  {Dashboardwidget} widget
     * @return {Boolean}
     */
    DashboardWidget.prototype.canRealTime = function() {
        return (new Date().getTime() - this.getLastRequest()) > this.getRefreshRate() ? true : false;
    };

    /**
     * Is a widget editable
     * Used to control whether the settings icon is displayed within the widget container
     * This may or may not display depending on teh user privileges
     * @return {Boolean}
     */
    DashboardWidget.prototype.isEditable = function() {
        return this.settings;
    };

    /**
     * Set whether the widget is editable or not
     * @param {Boolean} editState
     * @return {DashboardWidget} Chainable function
     */
    DashboardWidget.prototype.setSettings = function(editState) {
        this.settings = editState && editState === true ? true : false;
        return this;
    };

    /**
     * Set the saved property of the widget to true
     * This is used to render a saved widget to the dashboard
     * @return {DashboardWidget} Chainable function
     */
    DashboardWidget.prototype.setSaved = function() {
        this.saved = true;
        return this;
    };

    /**
     * Verifies if a dashboard widget is saved
     * @return {Boolean} [description]
     */
    DashboardWidget.prototype.isSaved = function() {
        return this.saved;
    };

    /**
     * Get the DashboardWidget's template container
     * @return {String}
     */
    DashboardWidget.prototype.getRenderedTemplate = function() {
        return this.renderedTemplate;
    };

    /**
     * This will depend on a process to render the Dashboardwidget's properties
     * to a template, either custom or default
     * @param {String} html The rendered html template retrieved 
     *                      from Dashboard_Generic_*type or a custom template
     */
    DashboardWidget.prototype.setRenderedTemplate = function(html) {
        this.renderedTemplate = html;
        return this;
    };

    /**
     * Sets the DashboardWidget width within a grid for rendering to a grid
     * @param {Number} sizex The width of a widget within teh grid
     * @return {DashboardWidget} Chainable function
     */
    DashboardWidget.prototype.setSizeX = function(sizex) {
        this.grid.sizex = sizex;
        return this;
    };

    /**
     * Sets the DashboardWidget height within a grid for rendering to a grid
     * @param {Number} sizey The height of a widget within the grid
     * @return {DashboardWidget} Chainable function
     */
    DashboardWidget.prototype.setSizeY = function(sizey) {
        this.grid.sizey = sizey;
        return this;
    };

    /**
     * Get the grid width of the Dashboardwidget
     * @return {Number}
     */
    DashboardWidget.prototype.getSizeX = function() {
        return this.grid.sizex;
    };

    /**
     * Get the grid height of the DashboardWidget
     * @return {Number}
     */
    DashboardWidget.prototype.getSizeY = function() {
        return this.grid.sizey;
    };

    /**
     * Get the DashboardWidget horizontal position within the grid i.e how many columns across
     * @return {Number}
     */
    DashboardWidget.prototype.getCol = function() {
        return this.grid.col;
    };

    /**
     * Get the DashboardWidget vertical position within the grid i.e. how many rows down
     * @return {Number} [description]
     */
    DashboardWidget.prototype.getRow = function() {
        return this.grid.row;
    };

    /**
     * Set the DashboardWidget position within the grid i.e. how many columns across
     * @param {Number} col
     * @return {DashboardWidget} Chainable function
     */
    DashboardWidget.prototype.setCol = function(col) {
        this.grid.col = col;
        return this;
    };

    /**
     * Set the DashboardWiddget position within the grid i.e. how many rows down
     * @param {Number} row 
     * @return {DashboardWidget} Chainable function
     */
    DashboardWidget.prototype.setRow = function(row) {
        this.grid.row = row;
        return this;
    };

    /**
     * A widget is normally set to a default widget unless specified e.g. custom widget, report widget e.t.c.
     * @param {String} type
     * @return {DashboardWidget} Chainable function
     */
    DashboardWidget.prototype.setWidgetType = function(type) {

        this.widgetType = type ? type : 'default';

        return this;
    };

    /**
     * Get the widget type
     * @return {String}
     */
    DashboardWidget.prototype.getWidgetType = function() {
        return this.widgetType;
    };

    /**
     * Creates a url string based on the properties within the
     * dataSource object
     * @return {String}
     */
    DashboardWidget.prototype.generateUrl = function() {
        return !this.hasParams() || this.urlContains('?') ? this.getUrl() : this.getUrl() + '?' + $.param(this.getParams());
    };

    /**
     * Checks if the dataSource has parameters when requesting data
     * @return {Boolean}
     */
    DashboardWidget.prototype.hasParams = function() {
        return !_.isEmpty(this.getParams());
    };

    /**
     * Checks if a property exists within the params object of the data source
     * @param  {String} property The property to search for
     * @return {Boolean}
     */
    DashboardWidget.prototype.propertyInParams = function(property) {
        return this.getParams()[property] ? true : false;
    };

    /**
     * Check if the widget url contains a character or word
     * @param  {String} char The character to search for in the url
     * @return {Boolean}
     */
    DashboardWidget.prototype.urlContains = function(char) {
        return this.dataSource.url.indexOf(char) > -1;
    };

    /**
     * Sets the datasource of the Dashboardwidget
     * @param {Object} source The properties to set the Dashboardwidget
     * @return {DashboardWidget} Chainable function
     */
    DashboardWidget.prototype.setDataSource = function(source) {
        $.extend(true, this.dataSource, source);
        return this;
    };

    /**
     * Get the data source of a DashboardWidget
     * @return {Object} returns an object containing {url: '', params : {}}
     */
    DashboardWidget.prototype.getDataSource = function() {
        return this.dataSource;
    };

    /**
     * Get the real time status of the widget
     * @return {Boolean}
     */
    DashboardWidget.prototype.isRealTime = function() {
        return this.realtime.isRealTime();
    };

    /**
     * Sets the properties of the real time object if an object of
     * properties is passed
     * @param {Object} prop
     * @return {DashboardWidget} Chainable function
     */
    DashboardWidget.prototype.setRealTimeProperties = function(prop) {

        if (!prop) return this;

        if (prop.isRealTime) this.realtime.setRealTime(prop.isRealTime);
        if (prop.lastRequest) this.realtime.setLastRequest(prop.lastRequest);
        if (prop.refreshRate) this.realtime.setRefreshRate(prop.refreshRate);

        return this;
    };

    /**
     * Enable or disable realtime functionality for a widget
     * Disabled by default
     * @param {Boolean} bool
     * @return {DashboardWidget} Chainable function
     */
    DashboardWidget.prototype.setRealTime = function(bool) {
        this.realtime.setRealTime(bool);
        return this;
    };

    /**
     * Set the refresh rate for a real time widget
     * The default is set to 5 minutes
     * @param {Widget} milliseconds The number of milliseconds
     * @return {DashboardWidget} Chainable function
     */
    DashboardWidget.prototype.setRefreshRate = function(milliseconds) {
        this.realtime.setRefreshRate(milliseconds);
        return this;
    };

    /**
     * Set the last request for data
     * @param {Number} timestamp
     * @return {DashboardWidget} Chainable function
     */
    DashboardWidget.prototype.setLastRequest = function(timestamp) {
        this.realtime.setLastRequest(timestamp);
        return this;
    };

    /**
     * Get the last request made by the DashboardWidget
     * @return {Number} The timestamp from the last request
     */
    DashboardWidget.prototype.getLastRequest = function() {
        return this.realtime.getLastRequest();
    };

    /**
     * Get the interval rate of a DashboardWidget when making requests
     * @return {Number} The time in milliseconds (usually 5 minutes)
     */
    DashboardWidget.prototype.getRefreshRate = function() {
        return this.realtime.getRefreshRate();
    };

    /**
     * Set the deferred request for this widget
     * @param {Deferred} deferred The deferred created upon requesting data
     * @return {DashboardWidget} Chainable function
     */
    DashboardWidget.prototype.setDeferred = function(deferred) {
        this.deferred = deferred;
        return this;
    };

    /**
     * Uses the data source to request data from DataLoader
     * Handles data retrieved using setData method
     * @return {DashboardWidget} Chainable function
     */
    DashboardWidget.prototype.requestData = function() {

        //A hack for dealing with report-stat requests which use format stat/user@service
        if(this.getUrl().indexOf('/report-stat') > -1){
            var labels = this.getParams().labels;
            assert(Creole.user, "DashboardWidget.js - requestData: A report-stat request was made but Creole.user is not set");
            var user = Creole.user.xid;
            this.setParams({
                labels: labels+user
            });
        }

        //when the data is retrieved we set the data for the widget
        var request = DataLoader.get(this.getUrl(), this.getParams()).done($.proxy(this.setData, this));

        //we also set the deferred request
        this.setDeferred(request);

        return this;
    };

    /**
     * This will auto set the time span of a DashboardWidget for data requests
     * @param {Object} timespan Saved object parameters or new parameters for the next data request
     * @return {DashboardWidget} Chainable function
     */
    DashboardWidget.prototype.setDataTimeSpan = function(timeSpan) {
        if (timeSpan) {
            this.setParams(timeSpan);
        } else {
            this.setParams({
                start: '1 week ago',
                end: 'now'
            });
        }
        return this;
    };

    /**
     * Set the data for the dashboard once retrieved
     * @param {Object} data The data in any format
     * @return {DashboardWidget} Chainable function
     */
    DashboardWidget.prototype.setData = function(data) {
        this.data = data;
        return this;
    };

    /**
     * DashboardWidgets will normally have a deferred as a result of a data request
     * This can be used and is updated on new requests
     * @return {jQuery Deferred}
     */
    DashboardWidget.prototype.getDeferred = function() {
        return this.deferred;
    };

    /**
     * Get the raw data from the result of a data request
     * This will normally be from a request to streams for breakdown/timeseries data or dashboardservlet responses,
     * could possibly include results from reports requests
     * @return {Object} Data in any format provided by original request 
     */
    DashboardWidget.prototype.getData = function() {
        return this.data;
    };
    /**
     * A DashboardWidget can be of many types e.g. table, list, chart, text, number e.t.c
     * This property is used when rendering the type to the right template.
     * Ensure this is set!!
     * @return {DashboardWidget} Chainable function
     */
    DashboardWidget.prototype.setType = function(type) {
        this.type = type;
        if (type === 'chart') {
            $.extend(true, this.chart, {});
        }
        return this;
    };
    /**
     * Set the grid properties of a DashboardWidget in order to allow it to
     * be rendered to a grid. This is useful when saved grid properties need to be
     * set against the DashboardWidget
     * @return {DashboardWidget} Chainable function
     */
    DashboardWidget.prototype.setGrid = function(config) {
        $.extend(true, this.grid, config);
        return this;
    };
    /**
     * Set the url of the data request
     * @param {String} url The url, in this case in the form '/stream.json?q=some+query&tagset=someTagset' e.t.c
     * @return {DashboardWidget} Chainable function
     */
    DashboardWidget.prototype.setUrl = function(url) {
        this.dataSource.url = url;
        return this;
    };
    /**
     * Set the start time of the data request
     * This can be in many forms e.g. '1 week ago', '6 hours ago', '01/01/2015', '01/12/2014', '01/01/2015 15:00 GMT'
     * @param {String} start
     * @return {DashboardWidget} Chainable function
     */
    DashboardWidget.prototype.setStart = function(start) {
        $.extend(true, this.dataSource.params, {
            start: start
        });
        return this;
    };

    /**
     * Set the end time of the data request
     * @param {String} end The end time - for the moment is usually set to 'now'
     * @return {DashboardWidget} Chainable function
     */
    DashboardWidget.prototype.setEnd = function(end) {
        $.extend(true, this.dataSource.params, {
            end: end
        });
        return this;
    };
    /**
     * Set additional the parameters of the data request
     * @return {DashboardWidget} Chainable function
     */
    DashboardWidget.prototype.setParams = function(params) {
        if (params) {
            $.extend(true, this.dataSource.params, params);
        }
        return this;

    };
    /**
     * Sets the chart options of a DashboardWidget of type chart
     * A Pie|Line chart have different ways of being rendered
     * @param {Object} options   The properties of the chart to be rendered
     * @return {DashboardWidget} Chainable function
     */
    DashboardWidget.prototype.setChart = function(options) {
        if (!this.chart) {
            this.chart = {};
        }

        $.extend(true, this.chart, options || {}); 
        
        return this;
    };

    /**
     * Set the type of request to be made i.e. get timeseries output or breakdown output from StreamServlet
     * @param {String} breakdownOrTimeseries
     * @return {DashboardWidget} Chainable function
     */
    DashboardWidget.prototype.setOutputType = function(breakdownOrTimeseries) {
        this.setParams({
            output: breakdownOrTimeseries
        });
        return this;
    };

    /**
     * Get the chart configuration properties 
     * chart.options will usually contain the type, series data, configuration parameters for the type of chart being rendered e.t.c
     * @return {Object} The chart properties
     */
    DashboardWidget.prototype.getChartOptions = function() {
        return this.chart.options;
    };

    /**
     * Set the chart type of a DashboardWidget
     * @return {String}
     * @return {DashboardWidget} Chainable function
     */
    DashboardWidget.prototype.setChartType = function(type) {
        $.extend(true, this.chart, {
            type: type
        });
        return this;
    };

    /**
     * Get the chart type of a dashboard widget
     * @return {String}
     */
    DashboardWidget.prototype.getChartType = function() {
        if (this.chart) return this.chart.type;
        return undefined;
    };


    /**
     * Set the template name for widgets that use a custom template
     * This usually be used to call templates[customTemplate](data)
     * @return {String}
     */
    DashboardWidget.prototype.setCustomTemplate = function(Template) {
        this.customTemplate = Template;
        return this;
    };
    /**
     * Set the html properties of a DashboardWidget
     * this could be {id: 'id', contentid: 'optional'}
     * @return {DashboardWidget} Chainable function
     */
    DashboardWidget.prototype.setHtml = function(config) {
        $.extend(true, this.html, config);
        return this;
    };

    /**
     * Get the Object containing the id and content id if set
     * @return {Object} {id:'',contentid:''}
     */
    DashboardWidget.prototype.getHtml = function() {
        return this.html;
    };
    /**
     * Set the id of the DashboardWidget, the id is usually used as the id of the parent container of an 
     * html element on the dashboard
     * This is unique and could be the id of a stream, custom text e.t.c
     * @param {String} name The unique id of the Dashboardwidget
     * @return {DashboardWidget} Chainable function
     */
    DashboardWidget.prototype.setID = function(name) {
        this.html.id = name;
        return this;
    };
    /**
     * Set the content id of the DashboardWidget. This is usually used in the
     * inside of the html of a DashboardWidget when rendering tables, charts or other elements
     * @param {String} name The id
     * @return {DashboardWidget} Chainable function
     */
    DashboardWidget.prototype.setContentID = function(name) {
        this.html.contentid = name;
        return this;
    };

    /**
     * Generate a unique content id based on the 
     * parent id and timestamp 
     * @return {String} 
     */
    DashboardWidget.prototype.generateContentID = function() {
        this.setContentID([this.getID(), new Date().getTime()].join('-'));
        return this;
    };

    /**
     * Set the header of the DashboardWidget
     * Usually consists of {title: '', 'icon':'an icomoon icon, optional'}
     * @param {Object} header 
     */
    DashboardWidget.prototype.setHeader = function(header) {
        $.extend(true, this.header, header);
        return this;
    };
    /**
     * Get the chart object of the DashboardWidget
     * This is usually a configuration that is rendered by Highcharts
     * @return {Object}
     */
    DashboardWidget.prototype.getChart = function() {
        if (!this.chart) {
            this.chart = undefined;
        }
        return this.chart;
    };
    /**
     * Get the custom template name
     * @return {String}
     */
    DashboardWidget.prototype.getCustomTemplate = function() {
        return this.customTemplate;
    };
    /**
     * Get the grid object
     * @return {Object}
     */
    DashboardWidget.prototype.getGrid = function() {
        return this.grid;
    };
    /**
     * Get the url of the DashboardWidget
     * @return {String}
     */
    DashboardWidget.prototype.getUrl = function() {
        return this.dataSource.url;
    };
    /**
     * Get the start property of the DashboardWidget from the datasource
     * @return {String}
     */
    DashboardWidget.prototype.getStart = function() {
        return this.dataSource.params.start;
    };
    /**
     * Get the end property of a dashboard widget from the datasource
     * @return {String}
     */
    DashboardWidget.prototype.getEnd = function() {
        return this.dataSource.params.end;
    };
    /**
     * Get the params object from the datasource
     * @return {Object}
     */
    DashboardWidget.prototype.getParams = function() {
        return this.dataSource.params;
    };
    /**
     * Get the DashboardWidget type which may be a table, list, chart e.t.c
     * @return {String}
     */
    DashboardWidget.prototype.getType = function() {
        return this.type;
    };
    /**
     * Get the header object which contains the properties icon and title
     * @return {Object}
     */
    DashboardWidget.prototype.getHeader = function() {
        return this.header;
    };

    /**
     * Get the title of the DashboardWidget
     * @return {String}
     */
    DashboardWidget.prototype.getTitle = function() {
        return this.header.title;
    };

    /**
     * Set the title of a DashboardWidget
     * @param {String} title The title to be displayed to users
     * @return {DashboardWidget} Chainable function
     */
    DashboardWidget.prototype.setTitle = function(title) {
        this.header.title = title;
        return this;
    };

    /**
     * Get the icomoon name of the icon to be rendered to the the header
     * of a DashboardWidget
     * @return {String}
     */
    DashboardWidget.prototype.getIcon = function() {
        return this.header.icon;
    };

    /**
     * Get the content id of the DashboardWidget
     * @return {String}
     */
    DashboardWidget.prototype.getContentID = function() {
        return this.html.contentid;
    };
    /**
     * Get the id of the DashboardWidget
     * @return {String}
     */
    DashboardWidget.prototype.getID = function() {
        return this.html.id;
    };


    /**
     * This is the function which expects the object source to contain
     * the properties required to create a dashboard widget.
     *
     * The widget is the widget source or the source object that a new widget
     * will be derived from. This currently is used to parse several sources i.e. from saved widgets, newly added widgets from form properties in a stream
     * and changes to dashboard widgets in the settings area of a dashboard widget
     *
     * This function will parse default dashboard widgets from DashboardDefaultWidgets.json. It will
     * also parse new custom widgets added using the dashboard wizard.
     * 
     * @param {Object} widgetSource
     * @param {Dashboardwidget} dbw
     */
    DashboardWidget.prototype.setWidgetOptionalProperties = function(widgetSource) {


        /*********************************************************************************************************
         * SETTING THE TYPE OF THE WIDGET ************************************************************************
         *********************************************************************************************************/

        //It is here where we use the type of the widget to call upon the template when rendering the widget data
        //i.e. a widget of type table will call on the 'Dashboard_Widget_Table' template.                

        //is it a list, chart, text, report e.t.c
        if (widgetSource.type) this.setType(widgetSource.type);

        //use a predefined custom template from DashboardTemplates.html for this widget
        if (widgetSource.customTemplate) this.setCustomTemplate(widgetSource.customTemplate);

        /*********************************************************************************************************
         * SETTING THE CHART OF THE WIDGET ***********************************************************************
         *********************************************************************************************************/

         if(widgetSource.chart) this.setChart(widgetSource.chart);

        //initialise the chart property on the dashboard widget        
        if (!widgetSource.chart && widgetSource.type === 'chart') this.setChart();

        //if this is not set, then it will render a chart as a time series
        if (widgetSource.chart) this.setChartType(widgetSource.chart.type || "");

        /*********************************************************************************************************
         * SETTING THE DATASOURCE OF THE WIDGET ******************************************************************
         *********************************************************************************************************/

        //start and end may or may not be applied to a request for data
        //for all chart types the data defaults to 1 week ago unless the property is set in dataSource
        if (widgetSource.dataSource && widgetSource.dataSource.params && !widgetSource.dataSource.params.start && widgetSource.type === 'chart') this.setDataTimeSpan();

        //set the url
        if (widgetSource.url) this.setUrl(widgetSource.url);

        //set the start time of the widget                
        if (widgetSource.start) this.setStart(widgetSource.start);

        //set the start time of the widget                
        if (widgetSource.end) this.setEnd(widgetSource.end);

        //if there is a tagset then set it (usually for streams)
        if (widgetSource.tagset) this.setParams({
            tagset: widgetSource.tagset
        });

        //custom widgets normally have a data format for rendering
        if (widgetSource.output && this.getWidgetType() !== 'default') this.setParams({
            output: widgetSource.output
        });

        /*********************************************************************************************************
         * SETTING THE HTML OF THE WIDGET ************************************************************************
         *********************************************************************************************************/

        //set the id to identify the widget
        if (widgetSource.id) this.setID(widgetSource.id);

        //if the content id has not been set then generate one for rendering
        //data to the content container 
        if (widgetSource.html && !widgetSource.html.contentid) this.generateContentID();

        //ensure that a contentid is created or rendering to the widget
        if (!this.getContentID()) this.generateContentID();

        /*********************************************************************************************************
         * SETTING THE HEADER OF THE WIDGET **********************************************************************
         *********************************************************************************************************/

        //all widgets should have a title
        if (widgetSource.title) this.setTitle(widgetSource.title);

        /*********************************************************************************************************
         * SETTING THE REALTIME PROPERTY OF THE WIDGET ***********************************************************
         *********************************************************************************************************/

        //the value of the checkbox is tied to Dashboard_Widget_Settings_RealTime
        //checkbox handling when widget settings are saved
        if (_.isObject(widgetSource.realtime) === true) {
            this.setRealTimeProperties(widgetSource.realtime);
            if (!widgetSource.realtime.isRealTime) this.setRealTime(false);
        } else {
            var realtime = widgetSource.realtime && (widgetSource.realtime === 'true' || widgetSource.realtime === true) ? true : false;
            this.setRealTime(realtime);
        }

        //for any widget which becomes realtime we set the last request, does not affect processing of
        //non real time widgets, this means that the last time we made a data request was the time in which the
        //dashboard object was initialised
        this.setLastRequest(new Date().getTime());

        /*********************************************************************************************************
         * SETTING THE GRID OPTIONS OF THE WIDGET ****************************************************************
         *********************************************************************************************************/

        if (widgetSource.grid) this.setGrid(widgetSource.grid);

        /*********************************************************************************************************
         * SETTING THE EDITABILITY OF THE WIDGET *****************************************************************
         *********************************************************************************************************/
        //anything that is not default or a report then allow it to be editable
        //TODO - default should always be unchanged an uneditable                 
        // if(['default', 'report'].indexOf(this.getWidgetType()) === -1) this.setSettings(true);               

        //this should overwrite any widget settings if it is a default widget
        // if (this.getWidgetType() === 'default') this.setSettings(false);
        if (widgetSource.settings === false || widgetSource.settings === 'false') {
            this.setSettings(false);
        } else {
            this.setSettings(true);
        }

        /*********************************************************************************************************
         * CONDITIONAL SETTINGS **********************************************************************************
         *********************************************************************************************************/       
        //was the dashboard saved?
        if(widgetSource.saved) this.setSaved();

        return this;
    };

    /**
     * convert a source from legacy configurations to current configuration safely
     * @param  {[type]} widgetSource [description]
     * @return {[type]}              [description]
     */
    DashboardWidget.prototype.convertIfLegacy = function(widgetSource) {
        //a legacy object will contain the property object.template
        //If this exists then we can do the conversion
        if (!widgetSource.template) return widgetSource;

        var newWidgetSource = {};

        $.extend(true, newWidgetSource, {
            html: {
                id: widgetSource.id
            },
            grid: {
                col: widgetSource.col,
                row: widgetSource.row,
                sizex: widgetSource.size_x,
                sizey: widgetSource.size_y
            },
            header: widgetSource.template.html.header,
            type: widgetSource.template.type,
            widgetType: widgetSource.template.widgettype
        });

        //chart handling
        if (widgetSource.template.chart) {
            $.extend(true, newWidgetSource, {
                chart: {
                    type: widgetSource.template.chart.type
                }
            });
        }

        var params = {};

        //if a widgetsource contains multiple parameters then we want to handle this separately
        if (widgetSource.url.indexOf('?') > -1) {

            //url parsing procedure
            //we are only concerned with the output, start, end and tagset if set
            //assumes Url is available                
            // widgetSource.url = widgetSource.url.replace('.json','');

            var queryObj = Url.parseQuery(widgetSource.url);

            if (queryObj.tagset) params.tagset = queryObj.tagset;

            if (queryObj.start) params.start = queryObj.start;

            if (queryObj.end) params.end = queryObj.end;

            if (queryObj.output) params.output = queryObj.output;

            if (queryObj.name) params.name = queryObj.name;

            //remove the tagset, start, end and output from the url as we now handle this separately
            delete queryObj.start;
            delete queryObj.end;
            delete queryObj.tagset;
            delete queryObj.output;
            delete queryObj.name;

            $.extend(true, newWidgetSource, {
                dataSource: {
                    url: decodeURIComponent($.param(queryObj)),
                    params: params
                }
            });
        } else {
            $.extend(true, newWidgetSource, {
                dataSource: {
                    url: widgetSource.url,
                    params: params
                }
            });
        }

        return newWidgetSource;
    };

    /**
     * This retrieves the template container and binds the widget to the container
     * @param {DashboardWidget} widget
     */
    DashboardWidget.prototype.setGenericWidgetContainer = function(renderedTemplate) {
        
        //set the html against the widget
        this.setRenderedTemplate($(renderedTemplate));
        
        return this;
    };

    /**
     * A RealTime object is a configuration which allows an object
     * to retrieve data based on time intervals in relation to the last request
     * The RealTime object is a configuration container by which other objects can
     * utilise to make repeated requests.
     * The default interval is set to 5 minutes
     * The reason for this is that the back end caches requests for 5 minutes!
     */
    function RealTime() {
        this.realtime = {
            isRealTime: false,
            refreshRate: 5 * 60 * 1000,
            lastRequest: undefined
        };
    }

    /**
     * Get the properties object which contains the realtime configuration
     * @return {Object}
     */
    RealTime.prototype.get = function() {
        return this.realtime;
    };
    /**
     * Check if this is a RealTime object
     * @return {Boolean}
     */
    RealTime.prototype.isRealTime = function() {
        return this.realtime.isRealTime;
    };
    /**
     * Get the refresh rate of the realtime object
     * @return {Number} In milliseconds 
     */
    RealTime.prototype.getRefreshRate = function() {
        return this.realtime.refreshRate;
    };
    /**
     * Get the last request for data 
     * @return {Number} timestamp of the last request, usually from new Date().getTime();
     */
    RealTime.prototype.getLastRequest = function() {
        return this.realtime.lastRequest;
    };
    /**
     * Set if this object is realtime or not
     * @return {RealTime} Chainable function
     */
    RealTime.prototype.setRealTime = function(bool) {
        this.realtime.isRealTime = bool;
        return this;
    };
    /**
     * Set the refresh rate in milliseconds i.e. the interval between data requests
     * @return {RealTime} Chainable function
     */
    RealTime.prototype.setRefreshRate = function(milliseconds) {
        this.realtime.refreshRate = milliseconds;
        return this;
    };
    /**
     * Set the time at which the most recent request was made
     * @return {RealTime} Chainable function
     */
    RealTime.prototype.setLastRequest = function(timestamp) {
        this.realtime.lastRequest = timestamp;
        return this;
    };

    return DashboardWidget;

})();
